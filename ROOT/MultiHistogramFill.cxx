/**
 * HAPPy (Higgs Analysis Plotting in Python)
 * A tool to facilitate the creation of publication quality ROOT plots.
 * Copyright �� 2017 Christian Grefe
 */
#include <TChain.h>
#include <TTree.h>
#include <TTreeFormula.h>
#include <TTreeFormulaManager.h>
#include <TStopwatch.h>
#include <TH1.h>
#include <TH2.h>
#include <TObject.h>
#include <TProfile.h>
#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <set>
#include <TLorentzVector.h>
#include <TFile.h>
#include <typeinfo>

// #include <xAODBase/IParticle.h>
// #include <xAODRootAccess/Init.h>
// #include <xAODRootAccess/TEvent.h>
// #include <xAODRootAccess/TStore.h>
// #include <xAODEventInfo/EventInfo.h>
// #include <xAODEventInfo/EventAuxInfo.h>
// #include <xAODBase/IParticleContainer.h>
// #include <xAODEgamma/Electron.h>
// #include <xAODMuon/Muon.h>

using std::cerr;
using std::cout;
using std::endl;
using std::map;
using std::string;
using std::vector;
using std::set;

/**
 * Class to fill multiple histograms in a single loop.
 * Histograms to be filled are defined using strings as for TTree::Draw via
 * the MultiHistogramFill::add method. TH1, TH2, TProfile and derived classes
 * are supported. Internally all requested expressions are converted into TTreeFormula
 * objects which can then processed in a single loop over the corresponding TTree.
 * Every expression is evaluated exactly once per event and then used for all
 * histograms which use the same expression. In addition, It is possible to set a global
 * weight expression which applies to all histograms. This expression is evaluated
 * first and if it evaluates to 0 the event will be skipped. This can make the loop even
 * more efficient.
 *
 * Make sure to define a TTree or TChain using the MultiHistogramFill::setTree
 * method first, then process the event loop by calling MultiHistogramFill::fill.
 * If you want to re-use the same object for another loop with different histograms
 * you can remove all previously registered histograms with MultiHistogramFill::clear.
*/

class MultiHistogramFill {
public:
  /**
   * Default constructor.
   */
  MultiHistogramFill();

  /**
   * Default constructor setting the tree.
   * @param tree   tree used for the event loop
   */
  MultiHistogramFill(TTree* tree);

  // Destructor
  ~MultiHistogramFill();

  
  /**
   * Returns the tree used for the event loop.
   * @return tree   used for the event loop
   */
  TTree* getTree() {
    return m_tree;
  }

  /**
   * Define the tree used for the event loop.
   * @param tree   tree used for the event loop
   */
  void setTree(TTree* tree) {
    m_tree = tree;
  }

  /**
   * Returns the global weight expression multiplied to all histograms.
   * @return global weight expression
   */
  const string& getGlobalWeightExpression() const {
    return m_globalWeightExpression;
  }

  /**
   * Set a global weight expression.
   * This expression will be multiplied to all event weights.
   * If it evaluates to 0 the event will be skipped before
   * computing any other expression
   * @param weightExpression std::string
   */
  void setGlobalWeightExpression(const string& weightExpression) {
    m_globalWeightExpression = weightExpression;
  }

  /**
   * Add definition of a 1D histogram.
   * @param histogram  object that will be filled in the loop
   * @param commandX   expression used to calculate the x value
   * @param weightExpression  expression used to calculate the event weight
   */
  void add(TH1* histogram, const string& commandX=string(), const string& weightExpression=string()) {
    m_histograms1D.push_back(new HistMeta1D(histogram, commandX, weightExpression));
  }

  /**
   * Add definition of a 2D histogram.
   * @param histogram  object that will be filled in the loop
   * @param commandX   expression used to calculate the x value
   * @param commandY   expression used to calculate the y value
   * @param weightExpression  expression used to calculate the event weight
   */
  void add(TH2* histogram, const string& commandX=string(), const string& commandY=string(), const string& weightExpression=string()) {
    m_histograms2D.push_back(new HistMeta2D(histogram, commandX, commandY, weightExpression));
  }

  /**
   * Add definition of a profile histogram.
   * @param histogram  object that will be filled in the loop
   * @param commandX   expression used to calculate the x value
   * @param commandY   expression used to calculate the y value
   * @param weightExpression  expression used to calculate the event weight
   */
  void add(TProfile* histogram, const string& commandX=string(), const string& commandY=string(), const string& weightExpression=string()) {
    m_profiles.push_back(new ProfileMeta(histogram, commandX, commandY, weightExpression));
  }

  /**
   * Remove all registered histogram definitions.
   */
  void clear() {
    for (auto histMeta : m_histograms1D) {
      delete histMeta;
    }
    m_histograms1D.clear();

    for (auto histMeta : m_histograms2D) {
      delete histMeta;
    }
    m_histograms2D.clear();

    for (auto histMeta : m_profiles) {
      delete histMeta;
    }
    // m_histograms2D.clear();
    m_profiles.clear();
  }

  /**
   * Main method to loop over the tree and fill all histograms.
   * @param nEntries   number of tree entries to process (default: all entries)
   */
  bool fill(const Long64_t& nEntries=-1);
  bool create_tree(const Long64_t& nEntries=-1, char* num_char=new char, Double_t extra_weight=1.0);
  bool fill_sym(const Long64_t& nEntries=-1, const int& anti_tau_ff=0);
  bool fill_mcRatio_mutau_signal(const Long64_t& nEntries=-1, const int& truthMatch=0, const int& WithEffCorr=0);
  bool fill_mcRatio_eltau_signal(const Long64_t& nEntries=-1, const int& truthMatch=0);

  // helper class to store information on 1D histograms
  struct HistMeta1D {
    TH1* hist;
    string commandX;
    string weightExpression;
    double* valueX;
    double* valueWeight;

    // default constructor
    HistMeta1D(TH1* hist, const string& commandX, const string& weightExpression) :
      hist(hist), commandX(commandX), weightExpression(weightExpression) {}

    // fill the histogram with the current values
    //gal:added effWeight variable and to the weights
    void fill(double weight=1., double effWeight=1.) {
      if (*valueWeight != 0.) {
        hist->Fill(*valueX, *valueWeight * weight / effWeight);     // with efficiency correction
        // hist->Fill(*valueX, *valueWeight * weight);              // without efficiency correction
        // cout << hist->GetName() << endl;
        // cout<<"sf in fill 1D: "<< *valueX << ", " << *valueWeight << ", " << weight << ", " << effWeight << endl;
        // cout<<"sf in fill 1D: "<<*valueWeight * weight<<endl;
        // hist->Fill(*valueX, *valueWeight * weight);    
      }
    }
  };

  // helper class to store information on 2D histograms
  struct HistMeta2D {
    TH2* hist;
    string commandX;
    string commandY;
    string weightExpression;
    double* valueX;
    double* valueY;
    double* valueWeight;

    // default constructor
    HistMeta2D(TH2* hist, const string& commandX, const string& commandY, const string& weightExpression) :
      hist(hist), commandX(commandX), commandY(commandY), weightExpression(weightExpression) {}

    // fill the histogram with the current values
    // void fill(double weight=1.) {
    void fill(double weight=1., double effWeight=1.) {
      if (*valueWeight != 0.) {
        // hist->Fill(*valueX, *valueY, *valueWeight * weight);
        hist->Fill(*valueX, *valueY, *valueWeight * weight / effWeight);     // with efficiency correction
        // cout<<"sf in fill 2D: "<< *valueX << ", "<< *valueY << ", " << *valueWeight << ", " << weight << ", " << effWeight << endl;
        // cout<<"sf in fill 2D: "<<*valueWeight * weight<<endl;
      }
    }
  };

  // helper class to store information on TProfiles
  struct ProfileMeta {
    TProfile* hist;
    string commandX;
    string commandY;
    string weightExpression;
    double* valueX;
    double* valueY;
    double* valueWeight;

    // default constructor
    ProfileMeta(TProfile* hist, const string& commandX, const string& commandY, const string& weightExpression) :
      hist(hist), commandX(commandX), commandY(commandY), weightExpression(weightExpression) {}

    // fill the histogram with the current values
    void fill(double weight=1.) {
      if (*valueWeight != 0.) {
        hist->Fill(*valueX, *valueY, *valueWeight * weight);
      }
    }
  };

protected:
  string m_globalWeightExpression;
  vector<HistMeta1D*> m_histograms1D;
  vector<HistMeta2D*> m_histograms2D;
  vector<ProfileMeta*> m_profiles;
  map<string, TTreeFormula*> m_stringToFormula;
  map<TTreeFormula*, double*> m_formulaToValue;
  TTree* m_tree;
  //gal:added parameters and load eff maps:
  UInt_t tau_1=0;
  UInt_t prongness;
  TLorentzVector  *tau_1_p4=0;
  TBranch         *b_tau_1_p4=0;   //!
  double effWeight=1.0;
  double effWeight_out=1.0;
  float FFQCD=1.0, FFW=1.0, RQCD=1.0, FF_total=1.0;
  double eff_ratio=1.0;
  double eff_ratio_tmmc=1.0;
  TFile *effmapfile = new TFile ("///afs/cern.ch/work/k/kchu/lfv_lh/symm_lephad/maindir/trials/lepeffOff_TOPQ1_allnoZll.root");
  TFile *effmapfile_lephad = new TFile ("///afs/cern.ch/work/k/kchu/lfv_lh/symm_lephad/maindir/trials/lepeffOff_TOPQ1_lephad_iptight_absetafix_allnoZll.root");//tautag map - currently without reco
  TH2F *el_effmap = (TH2F*)effmapfile->Get((TString)"el/pre/full/leptag/pteta_iso_ON_clus");
  TH2F *el_effmap_lephad_iso_id = (TH2F*)effmapfile_lephad->Get((TString)"el/pre/full/tautag_abseta/pteta_iso_ON_reco");
  TH2F *el_effmap_lephad_reco = (TH2F*)effmapfile_lephad->Get((TString)"el/pre/full/lep1tag_abseta/pteta_reco_ON_clus");
  TH2F *mu_effmap = (TH2F*)effmapfile->Get((TString)"mu/pre/full/leptag/pteta_iso_ON_reco");
  TFile *tmmc_effcorrfile = new TFile ("///afs/cern.ch/work/k/kchu/lfv_lh/symm_lephad/boom/plots/TM_SR_1p3p/Total_vbf_0_TM_SR.root");
  TH2F *tmmc_effcorrmap = (TH2F*)tmmc_effcorrfile->Get((TString)"emuratio_Total_vbf_0_TM_SR_lepton_pt_lepton_eta");
  TFile *FF_mapfile = new TFile ("///afs/cern.ch/work/k/kchu/lfv_lh/symm_lephad/maindir/trials/FF_tight_V05_all.root");
  TFile *R_mapfile = new TFile ("///afs/cern.ch/work/k/kchu/lfv_lh/symm_lephad/maindir/trials/R_tight_V05_all.root");
  TH1F *FF_QCDmap_nonVBF_e1p = (TH1F*)FF_mapfile->Get((TString)"etau_QCD_nonVBF_1Prong");
  TH1F *FF_QCDmap_nonVBF_e3p = (TH1F*)FF_mapfile->Get((TString)"etau_QCD_nonVBF_3Prong");
  TH1F *FF_QCDmap_nonVBF_mu1p = (TH1F*)FF_mapfile->Get((TString)"mutau_QCD_nonVBF_1Prong");
  TH1F *FF_QCDmap_nonVBF_mu3p = (TH1F*)FF_mapfile->Get((TString)"mutau_QCD_nonVBF_3Prong");
  TH1F *FF_Wmap_nonVBF_e1p = (TH1F*)FF_mapfile->Get((TString)"etau_W_nonVBF_1Prong");
  TH1F *FF_Wmap_nonVBF_e3p = (TH1F*)FF_mapfile->Get((TString)"etau_W_nonVBF_3Prong");
  TH1F *FF_Wmap_nonVBF_mu1p = (TH1F*)FF_mapfile->Get((TString)"mutau_W_nonVBF_1Prong");
  TH1F *FF_Wmap_nonVBF_mu3p = (TH1F*)FF_mapfile->Get((TString)"mutau_W_nonVBF_3Prong");
  TH1F *R_QCDmap_etau = (TH1F*)R_mapfile->Get((TString)"etau_QCD_nonVBF");
  TH1F *R_QCDmap_mutau = (TH1F*)R_mapfile->Get((TString)"mutau_QCD_nonVBF");
  int flag_tau=0;
  int flag_pt=0;
  int flag_count=0;
  int tau_0_matched_pdgId=0;
  int tau_1_matched_pdgId=0;
  // int tau_1_matched_classifierParticleOrigin=0;
  // int tau_1_matched_classifierParticleType=0;
  double lep_pt;
  double lep_eta;
  double tau_pt;
  bool effCheck=true;
  //and for trigger efficiencies:
  double trigWeight_data=1.0;
  double trigWeight_data_el=1.0;
  double trigWeight_data_mu=1.0;
  double trigWeight_mc=1.0;
  double sf_el_reco=1.0;
  double sf_el_id=1.0;
  double sf_el_iso=1.0;
  float eff_mc_mu_iso=1.0;
  float eff_mc_mu_id=1.0;
  float sf_mu_iso=1.0;
  float sf_mu_id=1.0;
  ULong64_t eventNum=0;
  ULong64_t eventNumber=0;
  UInt_t runNum;
  ULong64_t hist_counter=0;
  //for creating trees:
  double t_sf_total=-1;
  double t_eff_ratio=-1;
  double t_isVBF=-1;
  double t_NN=-1;
  double t_coll=-1;
  UInt_t t_tau_1=-1;
  double t_tau_eta=-1;
  UInt_t tau_1_matched=0;
// int tau_1_matched_classifierParticleOrigin=0;
// int tau_1_matched_classifierParticleType=0;
int tau_1_matched_isHad=0;
TLorentzVector *tau_1_matched_leptonic_tau_invis_p4;
TLorentzVector *tau_1_matched_leptonic_tau_p4;
TLorentzVector *tau_1_matched_leptonic_tau_vis_p4;
int tau_1_matched_mother_pdgId=0;
int tau_1_matched_mother_status=0;
TLorentzVector *tau_1_matched_p4;
// int tau_1_matched_pdgId=0;
Float_t tau_1_matched_q=0;
int tau_1_matched_status=0;
int tau_1_muonAuthor=0;
int tau_1_muonType=0;
UInt_t tau_1_muon_trig_HLT_mu20_iloose_L1MU15=0;
UInt_t tau_1_muon_trig_HLT_mu26_ivarmedium=0;
UInt_t tau_1_muon_trig_HLT_mu50=0;
UInt_t tau_1_muon_trig_trigger_matched=0;
int tau_1_origin=0;
// string tau_1_p4=0;
Float_t tau_1_q=0;
Float_t tau_1_trk_d0=1.0;
Float_t tau_1_trk_d0_sig=1.0;
Float_t tau_1_trk_pt=1.0;
Float_t tau_1_trk_pt_error=1.0;
Float_t tau_1_trk_pvx_z0=1.0;
Float_t tau_1_trk_pvx_z0_sig=1.0;
Float_t tau_1_trk_pvx_z0_sintheta=1.0;
Int_t tau_1_trk_vx=1;
// TVector3 *tau_1_trk_vx_v=1.0;
Float_t tau_1_trk_z0=1.0;
Float_t tau_1_trk_z0_sig=1.0;
// double tau_1_trk_z0_sinthe=1.0;

  // helper method for initialization
  void init() {}

  // helper method to facilitate creation of TTreeFormulae
  TTreeFormula* addTTreeFormula(const string& expression) {
    auto entry = m_stringToFormula.find(expression);
    if (entry == m_stringToFormula.end()) {
      TTreeFormula* formula = 0;
      if (expression.empty()) {
        formula = new TTreeFormula("1", "1", m_tree);
      } else {
        formula = new TTreeFormula(expression.c_str(), expression.c_str(), m_tree);
      }
      if (not formula->GetTree()) {
        cerr << "[ERROR] MultiHistogramFill::addTTreeFormula() - unable to compile TTreeFormula: \"" << expression << "\"" << endl;
        return 0;
      }
      formula->SetQuickLoad(true);
      m_stringToFormula[expression] = formula;
      m_formulaToValue[formula] = new double(0.);
      return formula;
    }
    return entry->second;
  }
};

MultiHistogramFill::MultiHistogramFill() {
  init();
}

MultiHistogramFill::MultiHistogramFill(TTree* tree) :
    m_tree(tree) {
  init();
}

MultiHistogramFill::~MultiHistogramFill() {
  this->clear();
}

bool MultiHistogramFill::fill(const Long64_t& nEntries) {
  if (not m_tree) {
    cerr << "[ERROR] MultiHistogramFill::fill() - missing input tree" << endl;
    return false;
  }

  TTreeFormulaManager* formulaManager = new TTreeFormulaManager();

  // create TTreeFormulas for all expressions used in 1D histograms
  for (auto histMeta : m_histograms1D) {
    formulaManager->Add(this->addTTreeFormula(histMeta->commandX));
    formulaManager->Add(this->addTTreeFormula(histMeta->weightExpression));
    // store the pointers to the values to fill them directly
    histMeta->valueX = m_formulaToValue[m_stringToFormula[histMeta->commandX]];
    histMeta->valueWeight = m_formulaToValue[m_stringToFormula[histMeta->weightExpression]];
  }

  // create TTreeFormulas for all expressions used in 2D histograms
  for (auto histMeta : m_histograms2D) {
    formulaManager->Add(this->addTTreeFormula(histMeta->commandX));
    formulaManager->Add(this->addTTreeFormula(histMeta->commandY));
    formulaManager->Add(this->addTTreeFormula(histMeta->weightExpression));
    // store the pointers to the values to fill them directly
    histMeta->valueX = m_formulaToValue[m_stringToFormula[histMeta->commandX]];
    histMeta->valueY = m_formulaToValue[m_stringToFormula[histMeta->commandY]];
    histMeta->valueWeight = m_formulaToValue[m_stringToFormula[histMeta->weightExpression]];
  }

  // create TTreeFormulas for all expressions used in TProfiles
  for (auto histMeta : m_profiles) {
    formulaManager->Add(this->addTTreeFormula(histMeta->commandX));
    formulaManager->Add(this->addTTreeFormula(histMeta->commandY));
    formulaManager->Add(this->addTTreeFormula(histMeta->weightExpression));
    // store the pointers to the values to fill them directly
    histMeta->valueX = m_formulaToValue[m_stringToFormula[histMeta->commandX]];
    histMeta->valueY = m_formulaToValue[m_stringToFormula[histMeta->commandY]];
    histMeta->valueWeight = m_formulaToValue[m_stringToFormula[histMeta->weightExpression]];
  }

  TTreeFormula* globalWeightFormula = 0;
  if (not m_globalWeightExpression.empty()) {
    globalWeightFormula = addTTreeFormula(m_globalWeightExpression);
    formulaManager->Add(globalWeightFormula);
  }

  // initialize the TTreeFormulaManager
  formulaManager->Sync();
  m_tree->SetNotify(formulaManager);

  // define some general variables
  Long64_t nEvents = m_tree->GetEntries();
  if (nEntries > 0 and nEntries < nEvents) {
    nEvents = nEntries;
  }
  int treeNumber = -1;
  double treeWeight = 1.;
  double globalWeight = 1.;
  
  m_tree->SetBranchStatus( "*", 0 );
  for (auto entry : m_formulaToValue) {
    for (int iLeaf = 0; iLeaf < entry.first->GetNcodes(); iLeaf++) {
      m_tree->SetBranchStatus( entry.first->GetLeaf(iLeaf)->GetName(), 1 );
    }
  }

  // main loop over the tree
  for (Long64_t iEvent = 0; iEvent < nEvents; iEvent++) {
    // update tree weight if necessary
    if (treeNumber != m_tree->GetTreeNumber()) {
      treeNumber = m_tree->GetTreeNumber();
      treeWeight = m_tree->GetWeight();
    }
    // take care of multiple trees in TChains
    m_tree->LoadTree(m_tree->GetEntryNumber(iEvent));

    //gal: add reading parameters for use and get the eff weights:
    m_tree->GetEntry(m_tree->GetEntryNumber(iEvent));
  
    globalWeight = treeWeight;
    // evaluate the common weight expression
    if (globalWeightFormula) {
      globalWeight *= globalWeightFormula->EvalInstance();
    }

    // no need to continue if weight is 0
    if (globalWeight == 0.) {
      continue;
    }

    // evaluate all TTreeFormulas
    for (auto entry : m_formulaToValue) {
      if (entry.first == globalWeightFormula) {
        continue;
      }
      *(entry.second) = entry.first->EvalInstance();
    }

    // fill all 1D histograms
    for (auto histMeta : m_histograms1D) {
      histMeta->fill(globalWeight, 1.0);
    }

    // fill all 2D histograms
    for (auto histMeta : m_histograms2D) {
      histMeta->fill(globalWeight, 1.0);
    }

    // fill all TProfiles
    for (auto histMeta : m_profiles) {
      histMeta->fill(globalWeight);
    }
  }
  // re-enable all branches
  m_tree->SetBranchStatus( "*", 1 );
  for (auto entry : m_formulaToValue) {
    formulaManager->Remove( entry.first );
    delete entry.first;
    delete entry.second;
  }
  m_formulaToValue.clear();
  m_stringToFormula.clear();
  return true;
}
//gal:Use this code to clone a tree with select entried:
bool MultiHistogramFill::create_tree(const Long64_t& nEntries, char* num_char, Double_t extra_weight) {
  if (not m_tree) {
    cerr << "[ERROR] MultiHistogramFill::fill() - missing input tree" << endl;
    return false;
  }

  TTreeFormulaManager* formulaManager = new TTreeFormulaManager();

  // create TTreeFormulas for all expressions used in 1D histograms
  for (auto histMeta : m_histograms1D) {
    cout << "!!!MultiHistogramFill::create_tree::1D!!!" << endl;
    formulaManager->Add(this->addTTreeFormula(histMeta->commandX));
    formulaManager->Add(this->addTTreeFormula(histMeta->weightExpression));
    // store the pointers to the values to fill them directly
    histMeta->valueX = m_formulaToValue[m_stringToFormula[histMeta->commandX]];
    histMeta->valueWeight = m_formulaToValue[m_stringToFormula[histMeta->weightExpression]];
  }
  
    for (auto histMeta : m_histograms2D) {
    cout << "!!!MultiHistogramFill::create_tree::2D!!!" << endl;
    formulaManager->Add(this->addTTreeFormula(histMeta->commandX));
    formulaManager->Add(this->addTTreeFormula(histMeta->commandY));
    formulaManager->Add(this->addTTreeFormula(histMeta->weightExpression));
    // store the pointers to the values to fill them directly
    histMeta->valueX = m_formulaToValue[m_stringToFormula[histMeta->commandX]];
    histMeta->valueY = m_formulaToValue[m_stringToFormula[histMeta->commandY]];
    histMeta->valueWeight = m_formulaToValue[m_stringToFormula[histMeta->weightExpression]];
  }
  
  TTreeFormula* globalWeightFormula = 0;
  if (not m_globalWeightExpression.empty()) {
    globalWeightFormula = addTTreeFormula(m_globalWeightExpression);
    formulaManager->Add(globalWeightFormula);
  }
  // initialize the TTreeFormulaManager
  formulaManager->Sync();
  m_tree->SetNotify(formulaManager);
  // define some general variables
  int treeNumber = -1;
  double treeWeight = 1.;
  double globalWeight = 1.;


  char file_name[200]={"///eos/user/k/kchu/symm_lephad/small_trees/"};
  strcat(file_name,num_char); 
  strcat(file_name,".root");
  

  //define new tree
  TFile newfile(file_name,"recreate");
  m_tree->SetBranchStatus( "*", 1 );
  // auto friendtree=m_tree->GetFriend("NOMINAL");

  auto newtree = m_tree->CloneTree(0);
  newtree->Branch("sf_total",&t_sf_total);
  newtree->SetAutoSave(0);
  // newtree->Branch("eff_ratio",&t_eff_ratio);
  // m_tree->SetBranchAddress("eff_ratio",&eff_ratio);

  // main loop over the tree
  Long64_t nEvents = m_tree->GetEntries();
  // Long64_t nEvents = m_tree->GetEntriesFast();
  if (nEntries > 0 and nEntries < nEvents) {
    nEvents = nEntries;
  }

  for (Long64_t iEvent = 0; iEvent < nEvents; iEvent++) {
    // update tree weight if necessary
    if (treeNumber != m_tree->GetTreeNumber()) {
      treeNumber = m_tree->GetTreeNumber();
      treeWeight = m_tree->GetWeight();
    }
    // take care of multiple trees in TChains
    m_tree->LoadTree(m_tree->GetEntryNumber(iEvent));
    m_tree->GetEntry(m_tree->GetEntryNumber(iEvent));

    // friendtree->LoadTree(friendtree->GetEntryNumber(iEvent));
    // friendtree->GetEntry(friendtree->GetEntryNumber(iEvent));
    
    globalWeight = treeWeight;
    // t_eff_ratio=eff_ratio;
    // evaluate the common weight expression
    if (globalWeightFormula) {
      globalWeight *= globalWeightFormula->EvalInstance();
    }

    if (globalWeight == 0.) {
      cout<<"$$$$$$$$$$$$$$$ global weight was 0 $$$$$$$$$$$$$$$$$$$$$$"<<endl;
      continue;
    }

    // evaluate all TTreeFormulas
    for (auto entry : m_formulaToValue) {
      if (entry.first == globalWeightFormula) {
        continue;
      }
      *(entry.second) = entry.first->EvalInstance();
    }

    
    for (auto histMeta : m_histograms1D) {
      //hist_counter+=1;
      //cout<<"debug: valueWeight: "<<*(histMeta->valueWeight)<<endl;
      if ((*(histMeta->valueWeight) * globalWeight) != 0){
      t_sf_total= *(histMeta->valueWeight) * globalWeight * extra_weight;
      // if(extra_weight<0){cout<< "### sumOfWeights is negative!!! "<<iEvent <<endl; }
      // if(*(histMeta->valueWeight)<0){cout<< "### Some SF is negative!!! " <<iEvent<<endl; }  
      // if(globalWeight<0){cout<< "### globalWeight is negative!!! " <<iEvent<<endl; }             
      cout<<"t_sf_total 1D: "<< t_sf_total<<endl;
      newtree->Fill();
      }
    }

    for (auto histMeta : m_histograms2D) {
      //hist_counter+=1;
      //cout<<"debug: valueWeight: "<<*(histMeta->valueWeight)<<endl;
      if ((*(histMeta->valueWeight) * globalWeight) != 0){
      t_sf_total= *(histMeta->valueWeight) * globalWeight * extra_weight;
      // if(extra_weight<0){cout<< "### sumOfWeights is negative!!! "<<iEvent <<endl; }
      // if(*(histMeta->valueWeight)<0){cout<< "### Some SF is negative!!! " <<iEvent<<endl; }  
      // if(globalWeight<0){cout<< "### globalWeight is negative!!! " <<iEvent<<endl; }             
      cout<<"t_sf_total 2D: "<< t_sf_total<<endl;
      newtree->Fill();
      }
    }

  }
  // clean up TTreeFormulae
  for (auto entry : m_formulaToValue) {
    formulaManager->Remove( entry.first );
    delete entry.first;
    delete entry.second;
  }
  m_formulaToValue.clear();
  m_stringToFormula.clear();

  cout<<"writing to file: " << num_char << endl;

  newfile.Write("",TObject::kWriteDelete);
  newfile.Close();


  return true;
}

bool MultiHistogramFill::fill_sym(const Long64_t& nEntries, const int& anti_tau_ff) {
  if (not m_tree) {
    cerr << "[ERROR] MultiHistogramFill::fill() - missing input tree" << endl;
    return false;
  }

  TTreeFormulaManager* formulaManager = new TTreeFormulaManager();

  // create TTreeFormulas for all expressions used in 1D histograms
  for (auto histMeta : m_histograms1D) {
    formulaManager->Add(this->addTTreeFormula(histMeta->commandX));
    formulaManager->Add(this->addTTreeFormula(histMeta->weightExpression));
    // store the pointers to the values to fill them directly
    histMeta->valueX = m_formulaToValue[m_stringToFormula[histMeta->commandX]];
    histMeta->valueWeight = m_formulaToValue[m_stringToFormula[histMeta->weightExpression]];
  }

  TTreeFormula* globalWeightFormula = 0;
  if (not m_globalWeightExpression.empty()) {
    globalWeightFormula = addTTreeFormula(m_globalWeightExpression);
    formulaManager->Add(globalWeightFormula);
  }

  // initialize the TTreeFormulaManager
  formulaManager->Sync();
  m_tree->SetNotify(formulaManager);

  // define some general variables
  Long64_t nEvents = m_tree->GetEntries();
  if (nEntries > 0 and nEntries < nEvents) {
    nEvents = nEntries;
  }
  int treeNumber = -1;
  double treeWeight = 1.;
  double globalWeight = 1.;
  effWeight=1.0;
  eventNum=0;
  eventNumber=1;

  m_tree->SetBranchStatus( "*", 0 );
  for (auto entry : m_formulaToValue) {
    for (int iLeaf = 0; iLeaf < entry.first->GetNcodes(); iLeaf++) {
      m_tree->SetBranchStatus( entry.first->GetLeaf(iLeaf)->GetName(), 1 );
    }
  }
  //gal: enable our needed branches (might be irrelevand, might be done automatically by SetBranchAddress):
  m_tree->SetBranchStatus( "tau_1", 1 );
  m_tree->SetBranchStatus( "tau_1_p4", 1 );
  m_tree->SetBranchStatus("trigWeight_mc",1);
  m_tree->SetBranchStatus("trigWeight_data",1);
  m_tree->SetBranchStatus("eventNum",1);
  m_tree->SetBranchStatus("event_number",1);
  //m_tree->SetBranchStatus("NOMINAL_pileup_random_run_number",1);
  m_tree->SetBranchStatus("eff_mc_mu_iso",1);
  m_tree->SetBranchStatus("eff_mc_mu_id",1);
  m_tree->SetBranchStatus("sf_mu_iso",1);
  m_tree->SetBranchStatus("sf_mu_id",1);
  m_tree->SetBranchStatus("sf_el_reco",1);
  m_tree->SetBranchStatus("sf_el_id",1);
  m_tree->SetBranchStatus("sf_el_iso",1);
  m_tree->SetBranchStatus("tau_0_n_charged_tracks",1);
  // m_tree->SetBranchStatus( "tau_0_matched_pdgId", 1 );
  // m_tree->SetBranchStatus( "tau_1_matched_pdgId", 1 );
  tau_1_p4 = 0;
  //m_tree->SetBranchAddress("NOMINAL_pileup_random_run_number",&runNum);
  m_tree->SetBranchAddress("tau_1_p4", &tau_1_p4, &b_tau_1_p4);
  m_tree->SetBranchAddress("tau_1", &tau_1);
  m_tree->SetBranchAddress("trigWeight_mc",&trigWeight_mc);
  m_tree->SetBranchAddress("trigWeight_data",&trigWeight_data);
  m_tree->SetBranchAddress("eventNum",&eventNum);
  m_tree->SetBranchAddress("event_number",&eventNumber);
  m_tree->SetBranchAddress("eff_mc_mu_iso",&eff_mc_mu_iso);
  m_tree->SetBranchAddress("eff_mc_mu_id",&eff_mc_mu_id);
  m_tree->SetBranchAddress("sf_mu_iso",&sf_mu_iso);
  m_tree->SetBranchAddress("sf_mu_id",&sf_mu_id);
  m_tree->SetBranchAddress("sf_el_reco",&sf_el_reco);
  m_tree->SetBranchAddress("sf_el_id",&sf_el_id);
  m_tree->SetBranchAddress("sf_el_iso",&sf_el_iso);
  m_tree->SetBranchAddress("tau_0_n_charged_tracks",&prongness);
  // m_tree->SetBranchAddress("tau_0_matched_pdgId",&tau_pdgId);
  // m_tree->SetBranchAddress("tau_1_matched_pdgId",&tau_1_pdgId);
  // main loop over the tree
  //int cut = 0;
  //bool passlepqualitycuts;
  //bool cut_lep_eta;
  for (Long64_t iEvent = 0; iEvent < nEvents; iEvent++) {
    // update tree weight if necessary
    if (treeNumber != m_tree->GetTreeNumber()) {
      treeNumber = m_tree->GetTreeNumber();
      treeWeight = m_tree->GetWeight();
    }
    // take care of multiple trees in TChains
    m_tree->LoadTree(m_tree->GetEntryNumber(iEvent));

   
    m_tree->GetEntry(m_tree->GetEntryNumber(iEvent));
    
   lep_pt=(tau_1_p4->Pt())*1000; //in MeV
   lep_eta=tau_1_p4->Eta();

    if (lep_pt>=1000000) lep_pt=999995;

    //if( fabs(tau_1_p4->Eta())<2.4 && !(fabs(tau_1_p4->Eta())>1.37 && fabs(tau_1_p4->Eta())<1.52 && fabs(tau_1_p4->Eta())<0.1) ){cut_lep_eta = true;}

    //if(tau_1_p4->Pt()>30 && cut_lep_eta ){passlepqualitycuts = true;}

    //if( passlepqualitycuts ) cut += 1;
    //cut += 1;

    if(eventNum!=eventNumber) cout << "Failed Match! \n" << "eventNum: " <<eventNum<< "\n" << "eventNumber: " <<eventNumber<< "\n" << "$ \n";

    if(tau_1==1) {//effWeight = mu_effmap->GetBinContent(mu_effmap->FindBin(lep_pt,lep_eta));
    //use atlas muon eff:
    effWeight=eff_mc_mu_id*eff_mc_mu_iso; 
    effWeight=effWeight*sf_mu_id*sf_mu_iso;
    }
    else if(tau_1==2) {
    //effWeight = el_effmap->GetBinContent(el_effmap->FindBin(lep_pt,lep_eta));
    effWeight = el_effmap_lephad_iso_id->GetBinContent(el_effmap_lephad_iso_id->FindBin(lep_pt,lep_eta));
    effWeight = effWeight * el_effmap_lephad_reco->GetBinContent(el_effmap_lephad_reco->FindBin(lep_pt,lep_eta));
    effWeight=effWeight*sf_el_id*sf_el_iso*sf_el_reco; 
    }
    else { cout << "$$$$$$$$$$Error!!!!!!!!!! tau_1 is not 1 or 2 for Event No. " << iEvent << "\n";
    continue; }
      
    //add Trig efficiencies:
    effWeight=effWeight*trigWeight_data;
  
    //if (iEvent%10000==0) cout<<" tau_1: "<<tau_1<<" mu id: "<<eff_mc_mu_id<<" mu iso: " << eff_mc_mu_iso << " trigger: "<<trigWeight_mc <<" total: "<<effWeight<<endl;

    globalWeight = treeWeight;
    // evaluate the common weight expression
    if (globalWeightFormula) {
      globalWeight *= globalWeightFormula->EvalInstance();
    }

    // no need to continue if weight is 0
    if (globalWeight == 0.) {
      continue;
    }

    // evaluate all TTreeFormulas
    for (auto entry : m_formulaToValue) {
      if (entry.first == globalWeightFormula) {
        continue;
      }
      *(entry.second) = entry.first->EvalInstance();
    }

    // fill all 1D histograms
    for (auto histMeta : m_histograms1D) {
      histMeta->fill(globalWeight, effWeight);
    }
  }
  //cout << "cut: " << cut << endl;
  // re-enable all branches
  m_tree->SetBranchStatus( "*", 1 );
  //gal: print counts:
  //cout << "bad taus: " << flag_tau << " bad pt: " << flag_pt << " total events: " << flag_count << "\n";
  // clean up TTreeFormulae
  for (auto entry : m_formulaToValue) {
    formulaManager->Remove( entry.first );
    delete entry.first;
    delete entry.second;
  }
  m_formulaToValue.clear();
  m_stringToFormula.clear();

  return true;
}

bool MultiHistogramFill::fill_mcRatio_mutau_signal(const Long64_t& nEntries, const int& truthMatch, const int& WithEffCorr) {
  if (not m_tree) {
    cerr << "[ERROR] MultiHistogramFill::fill() - missing input tree" << endl;
    return false;
  }

  TTreeFormulaManager* formulaManager = new TTreeFormulaManager();

  // create TTreeFormulas for all expressions used in 1D histograms
  for (auto histMeta : m_histograms1D) {
    formulaManager->Add(this->addTTreeFormula(histMeta->commandX));
    formulaManager->Add(this->addTTreeFormula(histMeta->weightExpression));
    // store the pointers to the values to fill them directly
    histMeta->valueX = m_formulaToValue[m_stringToFormula[histMeta->commandX]];
    histMeta->valueWeight = m_formulaToValue[m_stringToFormula[histMeta->weightExpression]];
  }
  
  // TTreeFormulaManager* formulaManager = new TTreeFormulaManager();

  // create TTreeFormulas for all expressions used in 2D histograms
  for (auto histMeta : m_histograms2D) {
    formulaManager->Add(this->addTTreeFormula(histMeta->commandX));
    formulaManager->Add(this->addTTreeFormula(histMeta->commandY));
    formulaManager->Add(this->addTTreeFormula(histMeta->weightExpression));
    // store the pointers to the values to fill them directly
    histMeta->valueX = m_formulaToValue[m_stringToFormula[histMeta->commandX]];
    histMeta->valueY = m_formulaToValue[m_stringToFormula[histMeta->commandY]];
    histMeta->valueWeight = m_formulaToValue[m_stringToFormula[histMeta->weightExpression]];
  }

  TTreeFormula* globalWeightFormula = 0;
  if (not m_globalWeightExpression.empty()) {
    globalWeightFormula = addTTreeFormula(m_globalWeightExpression);
    formulaManager->Add(globalWeightFormula);
  }

  // initialize the TTreeFormulaManager
  formulaManager->Sync();
  m_tree->SetNotify(formulaManager);

  // define some general variables
  Long64_t nEvents = m_tree->GetEntries();
  if (nEntries > 0 and nEntries < nEvents) {
    nEvents = nEntries;
  }
  int treeNumber = -1;
  double treeWeight = 1.;
  double globalWeight = 1.;
  effWeight=1.0;
  eventNum=0;
  eventNumber=1;
  eff_ratio=0;
  eff_ratio_tmmc=0;
  m_tree->SetBranchStatus( "*", 0 );
  for (auto entry : m_formulaToValue) {
    for (int iLeaf = 0; iLeaf < entry.first->GetNcodes(); iLeaf++) {
      m_tree->SetBranchStatus( entry.first->GetLeaf(iLeaf)->GetName(), 1 );
    }
  }
  //gal: enable our needed branches (might be irrelevand, might be done automatically by SetBranchAddress):
   m_tree->SetBranchStatus( "tau_1", 1 );
   m_tree->SetBranchStatus( "tau_1_p4", 1 );
  // m_tree->SetBranchStatus("trigWeight_mc",1);
  // m_tree->SetBranchStatus("trigWeight_data",1);
   m_tree->SetBranchStatus("eventNum",1);
   m_tree->SetBranchStatus("event_number",1);
  // m_tree->SetBranchStatus("NOMINAL_pileup_random_run_number",1);
   m_tree->SetBranchStatus("eff_mc_mu_iso",1);
   m_tree->SetBranchStatus("eff_mc_mu_id",1);
   m_tree->SetBranchStatus("sf_mu_iso",1);
   m_tree->SetBranchStatus("sf_mu_id",1);
   m_tree->SetBranchStatus("sf_el_reco",1);
   m_tree->SetBranchStatus("sf_el_id",1);
   m_tree->SetBranchStatus("sf_el_iso",1);
   m_tree->SetBranchStatus( "tau_0_matched_pdgId", 1 );
   m_tree->SetBranchStatus( "tau_1_matched_pdgId", 1 );

  m_tree->SetBranchStatus( "eff_ratio", 1 );
  
  // m_tree->SetBranchStatus( "tau_1_matched", 1);

  // m_tree->SetBranchStatus( "tau_1_matched_classifierParticleOrigin", 1);
  // m_tree->SetBranchStatus( "tau_1_matched_classifierParticleType", 1);
  // m_tree->SetBranchStatus( "tau_1_matched_isHad", 1);
  // m_tree->SetBranchStatus( "tau_1_matched_leptonic_tau_invis_p4", 1);
  // m_tree->SetBranchStatus( "tau_1_matched_leptonic_tau_p4", 1);
  // m_tree->SetBranchStatus( "tau_1_matched_leptonic_tau_vis_p4", 1);
  // m_tree->SetBranchStatus( "tau_1_matched_mother_pdgId", 1);
  // m_tree->SetBranchStatus( "tau_1_matched_mother_status", 1);
  // m_tree->SetBranchStatus( "tau_1_matched_p4", 1);

  // m_tree->SetBranchStatus( "tau_1_matched_q", 1);
  // m_tree->SetBranchStatus( "tau_1_matched_status", 1);
  // m_tree->SetBranchStatus( "tau_1_muonAuthor", 1);
  // m_tree->SetBranchStatus( "tau_1_muonType", 1);
  // m_tree->SetBranchStatus( "tau_1_muon_trig_HLT_mu20_iloose_L1MU15", 1);
  // m_tree->SetBranchStatus( "tau_1_muon_trig_HLT_mu26_ivarmedium", 1);
  // m_tree->SetBranchStatus( "tau_1_muon_trig_HLT_mu50", 1);
  // m_tree->SetBranchStatus( "tau_1_muon_trig_trigger_matched", 1);
  // m_tree->SetBranchStatus( "tau_1_origin", 1);

  // m_tree->SetBranchStatus( "tau_1_q", 1);
  // m_tree->SetBranchStatus( "tau_1_trk_d0", 1);
  // m_tree->SetBranchStatus( "tau_1_trk_d0_sig", 1);
  // m_tree->SetBranchStatus( "tau_1_trk_pt", 1);
  // m_tree->SetBranchStatus( "tau_1_trk_pt_error", 1);
  // m_tree->SetBranchStatus( "tau_1_trk_pvx_z0", 1);
  // m_tree->SetBranchStatus( "tau_1_trk_pvx_z0_sig", 1);
  // m_tree->SetBranchStatus( "tau_1_trk_pvx_z0_sintheta", 1);
  // m_tree->SetBranchStatus( "tau_1_trk_vx", 1);
  // // m_tree->SetBranchStatus( "tau_1_trk_vx_v", 1);
  // m_tree->SetBranchStatus( "tau_1_trk_z0", 1);
  // m_tree->SetBranchStatus( "tau_1_trk_z0_sig", 1);
  // // m_tree->SetBranchStatus( "tau_1_trk_z0_sinthe", 1);


  tau_1_p4 = 0;
  // m_tree->SetBranchAddress("NOMINAL_pileup_random_run_number",&runNum);
   m_tree->SetBranchAddress("tau_1_p4", &tau_1_p4, &b_tau_1_p4);
   m_tree->SetBranchAddress("tau_1", &tau_1);
  // m_tree->SetBranchAddress("trigWeight_mc",&trigWeight_mc);
  m_tree->SetBranchAddress("trigWeight_data_mu",&trigWeight_data_mu);
  m_tree->SetBranchAddress("trigWeight_data_el",&trigWeight_data_el);
  m_tree->SetBranchAddress("eventNum",&eventNum);
  m_tree->SetBranchAddress("event_number",&eventNumber);

  m_tree->SetBranchAddress("eff_mc_mu_iso",&eff_mc_mu_iso);
  m_tree->SetBranchAddress("eff_mc_mu_id",&eff_mc_mu_id);
  m_tree->SetBranchAddress("sf_mu_iso",&sf_mu_iso);
  m_tree->SetBranchAddress("sf_mu_id",&sf_mu_id);
  m_tree->SetBranchAddress("eff_ratio",&eff_ratio);
  m_tree->SetBranchAddress("sf_el_reco",&sf_el_reco);
  m_tree->SetBranchAddress("sf_el_id",&sf_el_id);
  m_tree->SetBranchAddress("sf_el_iso",&sf_el_iso);
  // m_tree->SetBranchAddress("tau_0_matched_pdgId",&tau_pdgId);
  // m_tree->SetBranchAddress("tau_1_matched_pdgId",&tau_1_pdgId);
  m_tree->SetBranchAddress("tau_0_matched_pdgId",&tau_0_matched_pdgId);
  m_tree->SetBranchAddress("tau_1_matched_pdgId",&tau_1_matched_pdgId);

  // m_tree->SetBranchAddress("tau_1_matched", &tau_1_matched);

  // m_tree->SetBranchAddress("tau_1_matched_classifierParticleOrigin",&tau_1_matched_classifierParticleOrigin);
  // m_tree->SetBranchAddress("tau_1_matched_classifierParticleType",&tau_1_matched_classifierParticleType);
  // m_tree->SetBranchAddress("tau_1_matched_isHad", &tau_1_matched_isHad);
  // m_tree->SetBranchAddress("tau_1_matched_leptonic_tau_invis_p4", &tau_1_matched_leptonic_tau_invis_p4);
  // m_tree->SetBranchAddress("tau_1_matched_leptonic_tau_p4", &tau_1_matched_leptonic_tau_p4);
  // m_tree->SetBranchAddress("tau_1_matched_leptonic_tau_vis_p4", &tau_1_matched_leptonic_tau_vis_p4);
  // m_tree->SetBranchAddress("tau_1_matched_mother_pdgId", &tau_1_matched_mother_pdgId);
  // m_tree->SetBranchAddress("tau_1_matched_mother_status", &tau_1_matched_mother_status);
  // m_tree->SetBranchAddress("tau_1_matched_p4", &tau_1_matched_p4);

  // m_tree->SetBranchAddress("tau_1_matched_q", &tau_1_matched_q);
  // m_tree->SetBranchAddress("tau_1_matched_status", &tau_1_matched_status);
  // m_tree->SetBranchAddress("tau_1_muonAuthor", &tau_1_muonAuthor);
  // m_tree->SetBranchAddress("tau_1_muonType", &tau_1_muonType);
  // m_tree->SetBranchAddress("tau_1_muon_trig_HLT_mu20_iloose_L1MU15", &tau_1_muon_trig_HLT_mu20_iloose_L1MU15);
  // m_tree->SetBranchAddress("tau_1_muon_trig_HLT_mu26_ivarmedium", &tau_1_muon_trig_HLT_mu26_ivarmedium);
  // m_tree->SetBranchAddress("tau_1_muon_trig_HLT_mu50", &tau_1_muon_trig_HLT_mu50);
  // m_tree->SetBranchAddress("tau_1_muon_trig_trigger_matched", &tau_1_muon_trig_trigger_matched);
  // m_tree->SetBranchAddress("tau_1_origin", &tau_1_origin);

  // m_tree->SetBranchAddress("tau_1_q", &tau_1_q);
  // m_tree->SetBranchAddress("tau_1_trk_d0", &tau_1_trk_d0);
  // m_tree->SetBranchAddress("tau_1_trk_d0_sig", &tau_1_trk_d0_sig);
  // m_tree->SetBranchAddress("tau_1_trk_pt", &tau_1_trk_pt);
  // m_tree->SetBranchAddress("tau_1_trk_pt_error", &tau_1_trk_pt_error);
  // m_tree->SetBranchAddress("tau_1_trk_pvx_z0", &tau_1_trk_pvx_z0);
  // m_tree->SetBranchAddress("tau_1_trk_pvx_z0_sig", &tau_1_trk_pvx_z0_sig);
  // m_tree->SetBranchAddress("tau_1_trk_pvx_z0_sintheta", &tau_1_trk_pvx_z0_sintheta);
  // m_tree->SetBranchAddress("tau_1_trk_vx", &tau_1_trk_vx);
  // // m_tree->SetBranchAddress("tau_1_trk_vx_v", &tau_1_trk_vx_v);
  // m_tree->SetBranchAddress("tau_1_trk_z0", &tau_1_trk_z0);
  // m_tree->SetBranchAddress("tau_1_trk_z0_sig", &tau_1_trk_z0_sig);
  // // m_tree->SetBranchAddress("tau_1_trk_z0_sinthe", &tau_1_trk_z0_sinthe);



  //Change for checking efficiencies:
  //m_tree->SetBranchStatus( "eff_ratio_lephad_iptight_noreco", 1 );
  // m_tree->SetBranchStatus( "eff_ratio_leptag", 1 );
  // m_tree->SetBranchStatus( "eff_ratio_leplep", 1 );
  //Change this back to eff_ratio to have correct efficiency.
  //m_tree->SetBranchAddress("eff_ratio_lephad_iptight_noreco",&eff_ratio);

  

  // main loop over the tree
  for (Long64_t iEvent = 0; iEvent < nEvents; iEvent++) {
    // update tree weight if necessary
    if (treeNumber != m_tree->GetTreeNumber()) {
      treeNumber = m_tree->GetTreeNumber();
      treeWeight = m_tree->GetWeight();
    }
    // take care of multiple trees in TChains
    m_tree->LoadTree(m_tree->GetEntryNumber(iEvent));

   
    m_tree->GetEntry(m_tree->GetEntryNumber(iEvent));
    
    lep_pt=tau_1_p4->Pt(); 
    lep_eta=tau_1_p4->Eta();
    
   // lep_pt=(tau_1_p4->Pt())*1000; //in MeV
   // lep_eta=tau_1_p4->Eta();
   

     //if (lep_pt>=1000000) lep_pt=999995;


  if(eventNum!=eventNumber) cout << "Failed Match! \n" << "eventNum: " <<eventNum<< "\n" << "eventNumber: " <<eventNumber<< "\n" << "$ \n";


      // if(tau_1==1) {
      //   // mutau events

      //   effWeight = 1.0;

      // }
      // else if(tau_1==2){
      //   // etau events

      //   // ******************Role of "effWeight": hist->Fill(*valueX, *valueWeight * weight / effWeight)***********************
      //   if(WithEffCorr==1){
      //       effWeight = 1 / eff_ratio;
      //   }else if(WithEffCorr==2){
      //       eff_ratio_tmmc = tmmc_effcorrmap->GetBinContent(tmmc_effcorrmap->FindBin(lep_pt,lep_eta));
      //       // eff_ratio_tmmc: N_etau / N_mutau
      //       if( abs(eff_ratio_tmmc) < 0.01 ){
      //           // if( lep_pt > 30 && abs(lep_eta) > 0.1 ){
      //           //     // effWeight = 1.0 * 0.01;
      //           //     cout << "lepton_pt: " << lep_pt << "lepton_eta: " << lep_eta << ", Get eff_ratio: " << eff_ratio_tmmc << ", Adjusted to 1.0" << endl;
      //           // }
      //           effWeight = 1.0;
      //       }else{
      //           effWeight = 1.0 * eff_ratio_tmmc;
      //       }
      //   }else{
      //       effWeight = 1.0;
      //   }
        
      //   cout << "tau_1:" << tau_1 << "lepton_pt: " << lep_pt << "lepton_eta: " << lep_eta << ", Get eff_ratio: " << eff_ratio_tmmc << ", Adjusted to 1.0" << endl;

      effWeight = 1.0;

      //   // effWeight = 1 / eff_ratio;
        
      //   //effWeight = 1/ ((eff_mc_mu_id * eff_mc_mu_iso * sf_mu_id * sf_mu_iso * trigWeight_data_mu) / (trigWeight_data_el));
 
      //   // effWeight = 1;
      // }
      // else{cout << "$$$$$$$$$$Error!!!!!!!!!! tau_1 is not 1 or 2 for Event No. " << iEvent << "\n";
      // continue;}

    globalWeight = treeWeight;
    // evaluate the common weight expression
    if (globalWeightFormula) {
      globalWeight *= globalWeightFormula->EvalInstance();
    }

    // no need to continue if weight is 0
    if (globalWeight == 0.) {
      continue;
    }

    // evaluate all TTreeFormulas
    for (auto entry : m_formulaToValue) {
      if (entry.first == globalWeightFormula) {
        continue;
      }
      *(entry.second) = entry.first->EvalInstance();
    }
    
    // cout << "globalWeight: " << globalWeight << ", effWeight: " << effWeight << endl;
    // if(tau_1==2 && (abs(tau_1_matched_pdgId)==0)){
    //   cout << "case with pdgId = 0" << endl;
    // // if(tau_1==2){
    //   cout << "tau_1_matched: " << tau_1_matched << endl;
    //   cout << "tau_1_matched_classifierParticleOrigin: " << tau_1_matched_classifierParticleOrigin << endl;
    //   cout << "tau_1_matched_classifierParticleType: " << tau_1_matched_classifierParticleType << endl;
    //   // cout << "tau_1_matched_isHad: " << tau_1_matched_isHad << endl;
    //   // cout << "tau_1_matched_leptonic_tau_invis_p4: " << tau_1_matched_leptonic_tau_invis_p4 << endl;
    //   // cout << "tau_1_matched_leptonic_tau_p4: " << tau_1_matched_leptonic_tau_p4 << endl;
    //   // cout << "tau_1_matched_leptonic_tau_vis_p4: " << tau_1_matched_leptonic_tau_vis_p4 << endl;
    //   // cout << "tau_1_matched_mother_pdgId: " << tau_1_matched_mother_pdgId << endl;
    //   // cout << "tau_1_matched_mother_status: " << tau_1_matched_mother_status << endl;
    //   // cout << "tau_1_matched_p4: " << tau_1_matched_p4 << endl;
    //   cout << "tau_1_matched_pdgId: " << tau_1_matched_pdgId << endl;
    //   cout << "tau_1_matched_q: " << tau_1_matched_q << endl;
    //   cout << "tau_1_matched_status: " << tau_1_matched_status << endl;
    //   // cout << "tau_1_muonAuthor: " << tau_1_muonAuthor << endl;
    //   // cout << "tau_1_muonType: " << tau_1_muonType << endl;
    //   // cout << "tau_1_muon_trig_HLT_mu20_iloose_L1MU15: " << tau_1_muon_trig_HLT_mu20_iloose_L1MU15 << endl;
    //   // cout << "tau_1_muon_trig_HLT_mu26_ivarmedium: " << tau_1_muon_trig_HLT_mu26_ivarmedium << endl;
    //   // cout << "tau_1_muon_trig_HLT_mu50: " << tau_1_muon_trig_HLT_mu50 << endl;
    //   // cout << "tau_1_muon_trig_trigger_matched: " << tau_1_muon_trig_trigger_matched << endl;
    //   // cout << "tau_1_origin: " << tau_1_origin << endl;
    //   // cout << "tau_1_p4: " << tau_1_p4 << endl;
    //   cout << "tau_1_q: " << tau_1_q << endl;
    //   // cout << "tau_1_trk_d0: " << tau_1_trk_d0 << endl;
    //   // cout << "tau_1_trk_d0_sig: " << tau_1_trk_d0_sig << endl;
    //   // cout << "tau_1_trk_pt: " << tau_1_trk_pt << endl;
    //   // cout << "tau_1_trk_pt_error: " << tau_1_trk_pt_error << endl;
    //   // cout << "tau_1_trk_pvx_z0: " << tau_1_trk_pvx_z0 << endl;
    //   // cout << "tau_1_trk_pvx_z0_sig: " << tau_1_trk_pvx_z0_sig << endl;
    //   // cout << "tau_1_trk_pvx_z0_sintheta: " << tau_1_trk_pvx_z0_sintheta << endl;
    //   // cout << "tau_1_trk_vx: " << tau_1_trk_vx << endl;
    //   // // cout << "tau_1_trk_vx_v: " << tau_1_trk_vx_v << endl;
    //   // cout << "tau_1_trk_z0: " << tau_1_trk_z0 << endl;
    //   // cout << "tau_1_trk_z0_sig: " << tau_1_trk_z0_sig << endl;
    //   // cout << "tau_1_trk_z0_sinthe: " << tau_1_trk_z0_sinthe << endl;
    //   cout << "lep_pt: " << lep_pt << endl; 
    // }

    // fill all 1D histograms
    for (auto histMeta : m_histograms1D) {
      // cout << "!!!fill all 1D histograms!!!" << endl;
      histMeta->fill(globalWeight, effWeight);
    }

    // fill all 2D histograms
    for (auto histMeta : m_histograms2D) {
      // cout << "!!!fill all 2D histograms!!!" << endl;
      histMeta->fill(globalWeight, effWeight);
    }
    
  }
  
  
  // re-enable all branches
  m_tree->SetBranchStatus( "*", 1 );
  //gal: print counts:
  //cout << "bad taus: " << flag_tau << " bad pt: " << flag_pt << " total events: " << flag_count << "\n";
  // clean up TTreeFormulae
  for (auto entry : m_formulaToValue) {
    formulaManager->Remove( entry.first );
    delete entry.first;
    delete entry.second;
  }
  m_formulaToValue.clear();
  m_stringToFormula.clear();

  return true;
}

bool MultiHistogramFill::fill_mcRatio_eltau_signal(const Long64_t& nEntries, const int& truthMatch) {
  //this function is exactly like fill_mcRatio_mutau_signal, only the effweight is switched - 1.0 for etau.
  if (not m_tree) {
    cerr << "[ERROR] MultiHistogramFill::fill() - missing input tree" << endl;
    return false;
  }

  TTreeFormulaManager* formulaManager = new TTreeFormulaManager();

  // create TTreeFormulas for all expressions used in 1D histograms
  for (auto histMeta : m_histograms1D) {
    formulaManager->Add(this->addTTreeFormula(histMeta->commandX));
    formulaManager->Add(this->addTTreeFormula(histMeta->weightExpression));
    // store the pointers to the values to fill them directly
    histMeta->valueX = m_formulaToValue[m_stringToFormula[histMeta->commandX]];
    histMeta->valueWeight = m_formulaToValue[m_stringToFormula[histMeta->weightExpression]];
  }

  TTreeFormula* globalWeightFormula = 0;
  if (not m_globalWeightExpression.empty()) {
    globalWeightFormula = addTTreeFormula(m_globalWeightExpression);
    formulaManager->Add(globalWeightFormula);
  }

  // initialize the TTreeFormulaManager
  formulaManager->Sync();
  m_tree->SetNotify(formulaManager);

  // define some general variables
  Long64_t nEvents = m_tree->GetEntries();
  if (nEntries > 0 and nEntries < nEvents) {
    nEvents = nEntries;
  }
  int treeNumber = -1;
  double treeWeight = 1.;
  double globalWeight = 1.;
  effWeight=1.0;
  eventNum=0;
  eventNumber=1;
  eff_ratio=0;
  m_tree->SetBranchStatus( "*", 0 );
  for (auto entry : m_formulaToValue) {
    for (int iLeaf = 0; iLeaf < entry.first->GetNcodes(); iLeaf++) {
      m_tree->SetBranchStatus( entry.first->GetLeaf(iLeaf)->GetName(), 1 );
    }
  }
  //gal: enable our needed branches (might be irrelevand, might be done automatically by SetBranchAddress):
   m_tree->SetBranchStatus( "tau_1", 1 );
  // m_tree->SetBranchStatus( "tau_1_p4", 1 );
  // m_tree->SetBranchStatus("trigWeight_mc",1);
  // m_tree->SetBranchStatus("trigWeight_data",1);
   m_tree->SetBranchStatus("eventNum",1);
   m_tree->SetBranchStatus("event_number",1);
  // m_tree->SetBranchStatus("NOMINAL_pileup_random_run_number",1);
  // m_tree->SetBranchStatus("eff_mc_mu_iso",1);
  // m_tree->SetBranchStatus("eff_mc_mu_id",1);
  // m_tree->SetBranchStatus("sf_mu_iso",1);
  // m_tree->SetBranchStatus("sf_mu_id",1);
  // m_tree->SetBranchStatus("sf_el_reco",1);
  // m_tree->SetBranchStatus("sf_el_id",1);
  // m_tree->SetBranchStatus("sf_el_iso",1);
  // m_tree->SetBranchStatus( "tau_0_matched_pdgId", 1 );
  // m_tree->SetBranchStatus( "tau_1_matched_pdgId", 1 );

  m_tree->SetBranchStatus( "eff_ratio", 1 );
  
  // tau_1_p4 = 0;
  // m_tree->SetBranchAddress("NOMINAL_pileup_random_run_number",&runNum);
  // m_tree->SetBranchAddress("tau_1_p4", &tau_1_p4, &b_tau_1_p4);
   m_tree->SetBranchAddress("tau_1", &tau_1);
  // m_tree->SetBranchAddress("trigWeight_mc",&trigWeight_mc);
  // m_tree->SetBranchAddress("trigWeight_data",&trigWeight_data);
   m_tree->SetBranchAddress("eventNum",&eventNum);
   m_tree->SetBranchAddress("event_number",&eventNumber);
  // m_tree->SetBranchAddress("eff_mc_mu_iso",&eff_mc_mu_iso);
  // m_tree->SetBranchAddress("eff_mc_mu_id",&eff_mc_mu_id);
  // m_tree->SetBranchAddress("sf_mu_iso",&sf_mu_iso);
  // m_tree->SetBranchAddress("sf_mu_id",&sf_mu_id);
  // m_tree->SetBranchAddress("sf_el_reco",&sf_el_reco);
  // m_tree->SetBranchAddress("sf_el_id",&sf_el_id);
  // m_tree->SetBranchAddress("sf_el_iso",&sf_el_iso);
  // m_tree->SetBranchAddress("tau_0_matched_pdgId",&tau_pdgId);
  // m_tree->SetBranchAddress("tau_1_matched_pdgId",&tau_1_pdgId);

  m_tree->SetBranchAddress("eff_ratio",&eff_ratio);


  // main loop over the tree
  for (Long64_t iEvent = 0; iEvent < nEvents; iEvent++) {
    // update tree weight if necessary
    if (treeNumber != m_tree->GetTreeNumber()) {
      treeNumber = m_tree->GetTreeNumber();
      treeWeight = m_tree->GetWeight();
    }
    // take care of multiple trees in TChains
    m_tree->LoadTree(m_tree->GetEntryNumber(iEvent));

   
    m_tree->GetEntry(m_tree->GetEntryNumber(iEvent));
    
  //  lep_pt=(tau_1_p4->Pt())*1000; //in MeV
  //  lep_eta=tau_1_p4->Eta();
   

    // if (lep_pt>=1000000) lep_pt=999995;


    if(eventNum!=eventNumber) cout << "Failed Match! \n" << "eventNum: " <<eventNum<< "\n" << "eventNumber: " <<eventNumber<< "\n" << "$ \n";


      if(tau_1==1) {

        effWeight = 1 / eff_ratio;
      }
      else if(tau_1==2){

        effWeight = 1.0;

      }
      else{cout << "$$$$$$$$$$Error!!!!!!!!!! tau_1 is not 1 or 2 for Event No. " << iEvent << "\n";
      continue;}

    globalWeight = treeWeight;
    // evaluate the common weight expression
    if (globalWeightFormula) {
      globalWeight *= globalWeightFormula->EvalInstance();
    }

    // no need to continue if weight is 0
    if (globalWeight == 0.) {
      continue;
    }

    // evaluate all TTreeFormulas
    for (auto entry : m_formulaToValue) {
      if (entry.first == globalWeightFormula) {
        continue;
      }
      *(entry.second) = entry.first->EvalInstance();
    }

    // fill all 1D histograms
    for (auto histMeta : m_histograms1D) {
      histMeta->fill(globalWeight, effWeight);
    }

    // fill all 2D histograms
    for (auto histMeta : m_histograms2D) {
      histMeta->fill(globalWeight, effWeight);
    }

    // // fill all TProfiles
    // for (auto histMeta : m_profiles) {
    //   histMeta->fill(globalWeight);
    // }
  }
  // re-enable all branches
  m_tree->SetBranchStatus( "*", 1 );
  //gal: print counts:
  //cout << "bad taus: " << flag_tau << " bad pt: " << flag_pt << " total events: " << flag_count << "\n";
  // clean up TTreeFormulae
  for (auto entry : m_formulaToValue) {
    formulaManager->Remove( entry.first );
    delete entry.first;
    delete entry.second;
  }
  m_formulaToValue.clear();
  m_stringToFormula.clear();

  


  return true;
}

void test() {
  TStopwatch timer;

  TChain* tree = new TChain("NOMINAL");
  tree->Add("/home/cgrefe/atlas/samples/Htautau_hh/v17_skim_tight/mc15/group.phys-higgs.hh.mc15_13TeV.344779.Sh221_PDF30_Ztt_MHPT70_140_h30h20.D3.e5585_s2726_r7772_r7676_p3017.v17.0_hist/*.root");

  string var0("ditau_mmc_mlm_m");
  string var1("ditau_tau0_pt");
  string var2("ditau_tau_1_pt");

  string cut0 = string("selection_lepton_veto && HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo &&") +
      "ditau_tau0_HLT_tau35_medium1_tracktwo && ditau_tau1_HLT_tau25_medium1_tracktwo &&" +
      "ditau_tau0_jet_bdt_medium == 1 && ditau_tau0_jet_bdt_medium == 1 && n_taus_medium == 2 &&" +
      "abs(ditau_tau0_q) == 1 && abs(ditau_tau1_q) == 1 &&" +
      "(ditau_tau0_n_tracks == 1 || ditau_tau0_n_tracks == 3) &&" +
      "(ditau_tau1_n_tracks == 1 || ditau_tau1_n_tracks == 3) &&" +
      "ditau_tau0_pt > 40 && ditau_tau1_pt > 30 &&" +
      "ditau_deta < 1.5 && ditau_dr > 0.8 && ditau_dr < 2.4 &&" +
      "jet_0_pt > 70. && abs(jet_0_eta) < 3.2 &&" +
      "selection_opposite_sign == 1 &&" +
      "0.1 < ditau_coll_approx_x0 && ditau_coll_approx_x0 < 1.4 &&" +
      "0.1 < ditau_coll_approx_x1 && ditau_coll_approx_x1 < 1.4 &&" +
      "ditau_mmc_mlm_m > 0.0";
  string cut1 = cut0 + "&& n_jets < 2 && ditau_vect_sum_pt > 100.";
  string cut2 = cut0 + "&& n_jets >= 2";

  TH1D* h_mmc_boosted = new TH1D("h_mmc_boosted", ";M_{MMC} [GeV];Entries / 2 GeV", 50, 50., 150.);
  TH1D* h_mmc_vbf = (TH1D*) h_mmc_boosted->Clone("h_mmc_vbf");
  TH1D* h_mmc_preselection = (TH1D*) h_mmc_boosted->Clone("h_mmc_preselection");
  TH1D* h_tau0pt_boosted = new TH1D("h_tau0pt_boosted", ";p_{T}(#tau_{0}) [GeV];Entries / 5 GeV", 50, 0., 250.);
  TH1D* h_tau0pt_vbf = (TH1D*) h_tau0pt_boosted->Clone("h_tau0pt_vbf");
  TH1D* h_tau0pt_preselection = (TH1D*) h_tau0pt_boosted->Clone("h_tau0pt_preselection");
  TH2D* h_mmc_vs_tau0pt_boosted = new TH2D("h_mmc_vs_tau0pt_boosted", ";M_{MMC} [GeV];p_{T}(#tau_{0}) [GeV];Entries / 2 GeV", 50, 50., 150., 50, 0., 250.);
  TH2D* h_mmc_vs_tau0pt_vbf = (TH2D*) h_mmc_vs_tau0pt_boosted->Clone("h_mmc_vs_tau0pt_vbf");
  TH2D* h_mmc_vs_tau0pt_preselection = (TH2D*) h_mmc_vs_tau0pt_boosted->Clone("h_mmc_vs_tau0pt_preselection");

  auto histograms1D = {h_mmc_preselection, h_mmc_boosted, h_mmc_vbf, h_tau0pt_preselection, h_tau0pt_boosted, h_tau0pt_vbf};
  auto histograms2D = {h_mmc_vs_tau0pt_preselection, h_mmc_vs_tau0pt_boosted, h_mmc_vs_tau0pt_vbf};

  timer.Start(true);
  tree->Draw((var0+" >> "+h_mmc_preselection->GetName()).c_str(), cut0.c_str(), "goff");
  tree->Draw((var0+" >> "+h_mmc_boosted->GetName()).c_str(), cut1.c_str(), "goff");
  tree->Draw((var0+" >> "+h_mmc_vbf->GetName()).c_str(), cut2.c_str(), "goff");
  tree->Draw((var1+" >> "+h_tau0pt_preselection->GetName()).c_str(), cut0.c_str(), "goff");
  tree->Draw((var1+" >> "+h_tau0pt_boosted->GetName()).c_str(), cut1.c_str(), "goff");
  tree->Draw((var1+" >> "+h_tau0pt_vbf->GetName()).c_str(), cut2.c_str(), "goff");
  tree->Draw((var1+" : "+var0+" >> "+h_mmc_vs_tau0pt_preselection->GetName()).c_str(), cut0.c_str(), "goff");
  tree->Draw((var1+" : "+var0+" >> "+h_mmc_vs_tau0pt_boosted->GetName()).c_str(), cut1.c_str(), "goff");
  tree->Draw((var1+" : "+var0+" >> "+h_mmc_vs_tau0pt_vbf->GetName()).c_str(), cut2.c_str(), "goff");
  timer.Stop();

  cout << "Timer: " << 1000.*timer.CpuTime() << "ms" << endl;
  for (auto hist : histograms1D) {
    cout << hist->GetName() << ": " << hist->GetEntries() << endl;
  }
  for (auto hist : histograms2D) {
    cout << hist->GetName() << ": " << hist->GetEntries() << endl;
  }

  for (auto hist : histograms1D) {
    hist->Reset();
  }
  for (auto hist : histograms2D) {
    hist->Reset();
  }

  string cut0b = "";
  string cut1b = "n_jets < 2 && ditau_vect_sum_pt > 100.";
  string cut2b = "n_jets >= 2";

  MultiHistogramFill multi(tree);
  //multi.setGlobalWeightExpression(cut0);
  //multi.add(h_mmc_preselection, var0, cut0b);
  //multi.add(h_mmc_boosted, var0, cut1b);
  //multi.add(h_mmc_vbf, var0, cut2b);
  //multi.add(h_tau0pt_preselection, var0, cut0b);
  //multi.add(h_tau0pt_boosted, var0, cut1b);
  //multi.add(h_tau0pt_vbf, var0, cut2b);
  //multi.add(h_mmc_vs_tau0pt_preselection, var0, var1, cut0b);
  //multi.add(h_mmc_vs_tau0pt_boosted, var0, var1, cut1b);
  //multi.add(h_mmc_vs_tau0pt_vbf, var0, var1, cut2b);
  multi.add(h_mmc_preselection, var0, cut0);
  multi.add(h_mmc_boosted, var0, cut1);
  multi.add(h_mmc_vbf, var0, cut2);
  multi.add(h_tau0pt_preselection, var0, cut0);
  multi.add(h_tau0pt_boosted, var0, cut1);
  multi.add(h_tau0pt_vbf, var0, cut2);
  multi.add(h_mmc_vs_tau0pt_preselection, var0, var1, cut0);
  multi.add(h_mmc_vs_tau0pt_boosted, var0, var1, cut1);
  multi.add(h_mmc_vs_tau0pt_vbf, var0, var1, cut2);

  timer.Start(true);
  multi.fill();
  timer.Stop();

  cout << "Timer: " << 1000.*timer.CpuTime() << "ms" << endl;
  for (auto hist : histograms1D) {
    cout << hist->GetName() << ": " << hist->GetEntries() << endl;;
  }
  for (auto hist : histograms2D) {
    cout << hist->GetName() << ": " << hist->GetEntries() << endl;
  }
}
