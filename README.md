HAPPy (Higgs Analysis Plotting in Python)
=========================================

This plotting package was created to facilitate the creation of beautiful
ROOT plots. It provides high level classes and interfaces that use plain ROOT
classes underneath to simplify the usual work flow in physics analyses.

While convenient in most cases there are of course exceptions. If you deviate
to far from the expected workflow it might be just easier to make your plots
from plain ROOT. The underlying ROOT classes are always accessible to
encourage using the standard ROOT interface to modify objects whenever it
is convinient.

Most of the python modules come with test cases (i.e. you can simply run
`python plot.py`). These can be used as examples for using the classes
defined in the module. Good starting points are the examples listed below
or exploring the  `plot.py`, `dataset.py` and `dataMcPlot.py` modules
in the `happy` package.

Christian Grefe, Bonn University (christian.grefe@cern.ch)

Getting Started
============

Browse the code repository: [GitLab](https://gitlab.cern.ch/ATauLeptonAnalysiS/HAPPy), and the API documentation: [www.cern.ch/cgrefe/HAPPy](https://cgrefe.web.cern.ch/cgrefe/HAPPy/index.html)

Download the code:
```bash
git clone ssh://git@gitlab.cern.ch:7999/ATauLeptonAnalysiS/HAPPy.git
```
Or fork the project first (click 'Fork') and download your version of the code
```bash
# replace CERN_USER_NAME by your user name
git clone ssh://git@gitlab.cern.ch:7999/CERN_USER_NAME/HAPPy.git
# Watch the main project (adding the remote upstream)
git remote add upstream ssh://git@gitlab.cern.ch:7999/ATauLeptonAnalysiS/HAPPy.git
# fetching the latest changes from main project
git fetch upstream
# alternatively fetch and merge the latest changes to your master branch in one step
git pull upstream master
```

Requirements:
- ROOT with pyROOT bindings (at least v6.14 recommended)
- python 2.7 (at least v2.7.13 recommended)

If the ATLAS software is available these can be loaded via
```bash
setupATLAS
lsetup root
```

Go to the HAPPy directory and set up the environment variables:
```bash
cd ./HAPPy/
source setup.sh
```

Package content
=============
- `ROOT`: C++ code and ROOT macros that are called from the Python code.
- `data`: static data like databases for cross-section values, etc.
- `happy`: main package containing all the Python code of core classes
- `scripts`: standalone Python scripts for specific tasks
- `logos`: logo files used in the documentation
- `doc`: documentation and doxygen configuration used to generate API documentation
- `examples`: standalone Python scripts with examples

Step by step examples
=================
- [Making plots and defining variables](doc/plots.md)
- Datasets
- Putting it all together

License
======
[License](LICENSE.md)
