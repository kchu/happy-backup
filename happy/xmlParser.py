## @package happy.xmlParser
#  @brief Module to allow creation of HAPPy objects from XML definitions.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from xml.etree.ElementTree import ElementTree
from happy.cut import Cut
from happy.dataset import RootDataset, CombinedDataset
from happy.histogramStore import HistogramStore
from happy.systematics import Systematics
from happy.crossSectionDB import crossSectionDB
import logging

logger = logging.getLogger( __name__ )

## List of Cut objects created from XML
cuts = []
## List of Variable objects created from XML
variables = []
## List of Dataset objects created from XML
datasets = []
## List of PhysicsProcess objects created from XML
physicsProcesses = []
## List of Systematics objects created from XML
systematics = []
## List of SystematicsSet objects created from XML
systematicsSets = []
## List of HistogramStore objects created from XML
histogramStores = []
    
elementTree = ElementTree()

## Read all known HAPPy objects from an XML file
#  @param fileName       input XML
def parse( fileName ):
    logger.debug( 'parse(): reading "%s"' % fileName )
    tree = elementTree.parse( fileName )
        
    # read the CrossSectionDB elements
    for element in tree.findall('CrossSectionDB'):
        crossSectionDB.fromXML( element )
        
    # read the HistogramStore elements
    for element in tree.findall('HistogramStore'):
        histogramStores.append( HistogramStore.fromXML( element ) )
        
    # read all Cut elements
    for element in tree.findall('Cut'):
        cuts.append( Cut.fromXML( element ) )
            
    # read all Dataset elements
    for element in tree.findall('RootDataset'):
        datasets.append( RootDataset.fromXML( element ) )
            
    # read all PhysicsProcess elements
    for element in tree.findall('PhysicsProcess'):
        physicsProcesses.append( CombinedDataset.fromXML( element ) )
            
if __name__ == '__main__':
    parse( 'xTauPlots/xTauDatasets.xml' )
