## @package happy.plot
#  @brief Simple plot objects representing a single canvas
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.variable import Variable, VariableBinning, var_Entries, binningFromHistogram
from happy.plotDecorator import TitleDecorator, LegendDecorator
from happy.style import Style
from happy.histogramTools import createOutOfRangeArrows
from happy.stringTools import alphanumericString, appendUUID
import os, math, copy

## Class to represent an arbitrary text object that can be put on a plot.
#  Used to store all attributes that might be passed to a TLatex object.
class TextElement:
    ## Default contructor.
    #  @param text       string of the text to display (can use TLatex)
    #  @param size       float defining font size (optional, default is 0.05)
    #  @param font       int defining the font to use (see https://root.cern.ch/doc/master/classTAttText.html#T5, optional, default is 42)
    #  @param color      int defining the text color (see https://root.cern.ch/doc/master/classTAttText.html#T3, optional, default is 1)
    #  @param italics    bool deciding if the text should be in italic font (optional, default is False)
    #  @param bold       bool deciding if the text should be in boldface font (optional, default is False)
    #  @param alignment  int defining the alignement of the text (see https://root.cern.ch/doc/master/classTAttText.html#T1, optional, default is 11)
    #  @param x          x position of the text in coordinates of the canvas between 0 and 1 (optional, default is 0)
    #  @param y          y position of the text in coordinates of the canvas between 0 and 1 (optional, default is 0)
    def __init__( self, text, size=0.05, font=42, color=1, italics=False, bold=False, alignment=11, x=0., y=0. ):
        ## String of the text to display (can use TLatex)
        self.text = text
        ## Float defining font size
        self.size = size
        ## Int defining the font to use (see https://root.cern.ch/doc/master/classTAttText.html#T5)
        self.font = font
        ## Int defining the text color (see https://root.cern.ch/doc/master/classTAttText.html#T3)
        self.color = color
        ## Bool deciding if the text should be in italic font
        self.italics = italics
        ## Bool deciding if the text should be in boldface font
        self.bold = bold
        ## Int defining the alignement of the text (see https://root.cern.ch/doc/master/classTAttText.html#T1)
        self.alignment = alignment
        ## x position of the text in coordinates of the canvas between 0 and 1
        self.x = x
        ## y position of the text in coordinates of the canvas between 0 and 1
        self.y = y
        
    def __repr__( self ):
        return 'TextElement(%r, %r, %r, %r, %r, %r, %r, %r, %r)' %(self.text, self.size, self.font, self.color,
                                                                   self.italics, self.bold, self.alignment, self.x, self.y)

    ## Creates a copy of the text string including all TLatex attributes
    #  @return  string to be used in TLatex
    def getText( self ):
        result = self.text
        if self.font:
            result = '#font[%d]{%s}' % (self.font, result)
        if self.color is not None:
            result = '#color[%d]{%s}' % (self.color, result)
        if self.italics:
            result = '#it{%s}' % result
        if self.italics:
            result = '#bf{%s}' % result
        return result
    
    ## Creates a TLatex attributes with the text and all attributes applied
    #  @return  TLatex object
    def getTLatex( self ):
        from ROOT import TLatex
        t = TLatex( self.x, self.y, self.getText() )
        t.SetNDC()
        t.SetTextSize( self.size )
        t.SetTextAlign( self.alignment )
        return t

## Class to store all attributes of a legend entry to be shown in a plot.
class LegendElement:
    ## Default contructor.
    #  @param obj       object referenced in the legend entry
    #  @param text      string or TextElement object used for the legend entry
    #  @param option    draw option: 'l' for line, 'p' for marker, 'f' for filled box
    def __init__( self, obj, text='', option='' ):
        self.obj = obj
        self.text = text
        self.option = option

    def __repr__( self ):
        return 'LegendElement(%r, %r, %r)' % (self.obj, self.text, self.option)

    ## Returns the string to be displayed as the legend entry
    #  return string object
    def getText( self ):
        if isinstance( self.text, TextElement ):
            return self.text.getText()
        return self.text

## Generic canvas class.
#  The canvas size is controled by Canvas::sizeX and Canvas::sizeY. The default
#  size is 600x600 as suggested in the ATLAS style guide. In addition, a reference size
#  can be defined using Canvas::referenceSizeX and Canvas::referenceSizeY. If they
#  are set they are used to keeping the label sizes and margins exactly the same size as
#  when using the reference size in that direction while increasing the size of the plot.
class Canvas( object ):
    # list of file types that are produced when calling Canvas::saveAsAll.
    outputTypes = ['C', 'eps', 'pdf', 'png', '.root']
    ## Default constuctor.
    #  @param title       string used as title of the TCanvas
    def __init__( self, title='' ):
        ## Title of the TCanvas
        self.title = title
        ## Size of the canvas in x
        self.sizeX = 600
        ## Size of the canvas in y
        self.sizeY = 600.
        ## All margins, labels, tick marks etc. are scaled to this reference size
        self.referenceSizeX = None
        ## All margins, labels, tick marks etc. are scaled to this reference size
        self.referenceSizeY = None
        ## TCanvas object (available after calling draw)
        self.canvas = None
        ## Keep track of scale factor for all objects in x direction
        self._scaleX = 1.
        ## Keep track of scale factor for all objects in y direction
        self._scaleY = 1.
        ## Keep track of scale factor for text objects in x direcction
        self._scaleTextX = 1.
        ## Keep track of scale factor for text objects in y direcction
        self._scaleTextY = 1.
        ## Access to the color palette if draw option contained 'z' and draw() was called
        self.palette = None

    def __repr__( self ):
        return 'Canvas(%r)' % (self.title)

    ## Draw the canvas and all contained objects.
    def draw( self ):
        from ROOT import TCanvas
        if not self.canvas or isinstance( self.canvas, TCanvas):
            if self.canvas:
                self.canvas.Close()
            self.canvas = TCanvas( alphanumericString( self.title ), self.title, int(self.sizeX), int(self.sizeY) )
            # the canvas size includes the menu bar and other window decorations, need to correct for it
            self.canvas.SetWindowSize(int(self.sizeX + (self.sizeX - self.canvas.GetWw())), int(self.sizeY + (self.sizeY - self.canvas.GetWh())));
            self.canvas.SetCanvasSize(int(self.sizeX), int(self.sizeY))
        else:
            self.canvas.Clear()
        # clean up previous canvas
        self.canvas.cd()
        # determine the scale factors
        sizeX = self.canvas.GetWw() * self.canvas.GetWNDC()
        sizeY = self.canvas.GetWh() * self.canvas.GetHNDC()
        self._scaleX = self.referenceSizeX / float(sizeX) if self.referenceSizeX else 1.
        self._scaleY = self.referenceSizeY / float(sizeY) if self.referenceSizeY else 1.
        # text sizes are determined as the fraction of the shortest side (width or height)
        if self.referenceSizeX or self.referenceSizeY:
            shortestSide = min(sizeX, sizeY)
            refX = self.referenceSizeX if self.referenceSizeX else sizeX
            refY = self.referenceSizeY if self.referenceSizeY else sizeY
            #shortestSideRef = min(refX, refY)
            self._scaleTextX = refY / float(shortestSide)
            self._scaleTextY = refX / float(shortestSide)
        else:
            self._scaleTextX = 1.
            self._scaleTextY = 1.
        # scale margins around the plot
        self.canvas.SetLeftMargin( self.canvas.GetLeftMargin() * self._scaleX )
        self.canvas.SetRightMargin( self.canvas.GetRightMargin() * self._scaleX )
        self.canvas.SetTopMargin( self.canvas.GetTopMargin() * self._scaleY )
        self.canvas.SetBottomMargin( self.canvas.GetBottomMargin() * self._scaleY )

    ## Save the canvas under the given path.
    #  The file type is determined automatically. If no file ending is given
    #  it is stored as pdf format.
    #  @param path    full path of output file (optional, default uses the title as file name)
    def saveAs( self, path='' ):
        fileName, extension = os.path.splitext( path )
        if not extension:
            extension = '.pdf'
        if not fileName:
            fileName = self.title
        self.canvas.SaveAs( fileName + extension )

    ## Save the canvas under the given path in all relevant file types.
    #  The canvas is stored as eps, pdf, png, ROOT file and ROOT macro.
    #  The list of file types produced is controlled by Canvas.outputTypes.
    #  The file type is ignored if an extension is given in the path.
    #  @param path     full path of output file (optional, default is current directory)
    def saveAsAll( self, path='' ):
        fileName = os.path.splitext( path )[0]
        for fileType in self.outputTypes:
            self.saveAs( fileName + '.' + fileType )
    
    ## Save the canvas in the given directory path as a pdf.
    #  The file name is determined automatically from the plot title.
    #  @param path    directory to store the output file (optional, default is current directory)
    def saveIn( self, path='' ):
        self.saveAs( os.path.join( path, self.title ) )
        

## Simple plot with a single canvas.
#  Takes care of the styling and drawing of all objects. Supports drawing of TH1, TH2,
#  TProfile, TGraph and all derived objects as well as arbitrary text elements. Also 
#  takes care of creating a legend and titles using LegendDecorator and TitleDecorator
#  objects, respectively.
class Plot( Canvas ):
    ## TitleDecorator object used as default for all plots
    defaultTitleDecorator = TitleDecorator()
    ## LegendDecorator object used as default for all plots
    defaultLegendDecorator = LegendDecorator()
    ## Default constuctor.
    #  @param title       string used as title of the TCanvas
    #  @param variableX   Variable object defining the x axis
    #  @param variableY   Variable object defining the y axis (optional, default uses "Entries")
    #  @param variableZ   Variable object defining the z axis, only shown for 2D objects with a z-axis (optional, default uses "Entries")
    def __init__( self, title='', variableX=None, variableY=var_Entries, variableZ=var_Entries ):
        Canvas.__init__( self, title )
        ## TitleDecorator object used for this plot (copied from BasicPlot::defaultTitleDecorator)
        self.titleDecorator = copy.copy( self.defaultTitleDecorator )
        ## LegendDecorator object used for this plot (copied from BasicPlot::defaultLegendDecorator)
        self.legendDecorator = copy.deepcopy( self.defaultLegendDecorator )
        ## List of additional PlotDecorator objects that are executed in BasicPlot::draw()
        self.decorators = []
        ## List of strings displayed on the plot using the TitleDecorator object
        self.titles = []
        ## List of TextElement objects
        self.textElements = []
        ## List of LegendEntry objects (reset and filled automatically in draw)
        self.legendElements = []
        ## List of all objects that are drawn
        self._objects = []
        ## List of objects created by PlotDecorator objects (reset and filled automatically in draw)
        self._decoratorObjects = []
        ## Decide if a legend should be drawn
        self.drawLegend = True
        ## Decide if titles should be added
        self.drawTitle = True
        ## Line color used for stacked histograms if no fill style is defined
        self.stackedOutlineColor = 922
        ## Line width used for stacked histograms if no fill style is defined
        self.stackedOutlineWidth = 1
        self.normalizeByBinWidth = False      # decide if individual bins should be drawn normalized by their respective bin width
        ## Decide if the histograms should be normalised to 1, i.e. divided by their integral
        self.normalized = False
        ## Margin in the plot above all histograms, graphs, etc. to allow space for legend or titles
        self.topMargin = 0.2
        ## Margin in the plot below all histograms, graphs, etc. to allow space for legend or titles
        self.bottomMargin = 0.05
        ## Decide if the plot should be drawn with logarithmic x-axis
        self.logX = False
        ## Decide if the plot should be drawn with logarithmic y-axis
        self.logY = False
        ## Decide if the plot should be drawn with logarithmic z-axis
        self.logZ = False
        self.showBinWidthY = True             # decide if the y-axis label contains '/ <binWidthX> [<unitX>]' 
        self.combineStatsAndSyst = True       # decide if systematic and statistical errors should be drawn combined or seperately
        ## Variable used for the x axis
        self.variableX = variableX
        ## Variable used for the y axis
        self.variableY = variableY
        ## Variable used for the z axis
        self.variableZ = variableZ
        ## List of 1D histograms
        self.histograms = []
        ## List of 2D histograms
        self.histograms2D = []
        ## THStack object, includes all 1D histograms with stacked set to True
        self._histogramStack = None
        ## List of stacked histograms
        self.stackedHistograms = []
        ## Dictionary of name to histogram object
        self.histogramsByName = {}
        ## Dictionary of all objects to their draw options
        self.optionsDict = {}
        ## List of graphs
        self.graphs = []
        ## Dictionary of name to graph object
        self.graphsByName = {}
        ## Dictionary of fit function to the object which should be fitted
        self.fitFunctions = {}
        ## Dictionary of fit function to fit result
        self.fitResults = {}
        ## Dictionary of fit function to its fit options
        self.fitOptions = {}
        ## List of fit functions
        self.functions = []
        ## Dictionary of name to fit function
        self.functionsByName = {}
        ## Keep track of minimum value in any drawn object, used to set y axis range
        self._minimum = float('inf')
        ## Keep track of minimum value in any drawn object, used to set y axis range
        self._maximum = float('-inf')
        ## Keep track of the object that was drawn first, which is used to define the axes
        self._firstDrawnObject = None
        ## Keep track of the object that was used to draw the axes in a previous call to draw
        self._previousFirstDrawnObject = None
        ## Map of TH1 or TGraph objects to Style objects, deciding if out-of-range arrows should be drawn
        self._drawOutOfRangeArrows = {}
        
    def __repr__( self ):
        return 'Plot(%r, %r, %r, %r)' % (self.title, self.variableX, self.variableY, self.variableZ)
    
    ## Adds an object to the list of objects to be drawn.
    #  The object needs to have a Draw method.
    #  @param obj      ROOT object that should be drawn.
    def add( self, obj ):
        self._objects.append( obj )
    
    ## Remove an object from this plot.
    #  Removes the object from all internal containers.
    #  @param obj      object to be removed
    def remove( self, obj ):
        if not obj:
            return
        # remove the object from all containers
        for container in [self.histograms, self.histograms2D, self.graphs, self.functions, self._objects]:
            while obj in container:
                container.remove( obj )
        for dictionary in [self.fitFunctions, self.fitResults, self.fitOptions, self.optionsDict]:
            if dictionary.has_key( obj ):
                del dictionary[ obj ]
        for legendElement in self.legendElements:
            if obj == legendElement.obj:
                self.legendElements.remove( legendElement )
    
    ## Adds a TH1 or TH2 to the list of objects to be drawn.
    #  The histogram will not appear in the legend if the title is empty.
    #  There is no need to pass "SAME" in the draw option, this is handled automatically.
    #  @param histogram    TH1 or TH2 object
    #  @param drawOption   option used to draw the object (see https://root.cern/doc/master/classTHistPainter.html#HP01, optional, default is "HIST")
    #  @param stacked      set if histogram should be stacked with other stacked histograms (optional, default is False)
    #  @param copy         make a copy of the histogram instead of using the object itself (optional, default is False)
    #  @return the histogram object (copied version in case of copy=True)
    def addHistogram( self, histogram, drawOption='HIST', stacked=False, copy=False ):
        if not histogram:
            return
        name = histogram.GetName()
        if copy:
            # generate a copy with a unique name
            histogram = histogram.Clone( appendUUID( name ) )
        self.histogramsByName[ name ] = histogram
        self.optionsDict[ histogram ] = drawOption
        from ROOT import TH2
        if isinstance( histogram, TH2 ):
            self.histograms2D.append( histogram )
        else:
            if stacked:
                self.stackedHistograms.append( histogram )
            self.histograms.append( histogram )
            # add a corresponding LegendElement
            title = histogram.GetTitle()
            if title:
                if histogram in self.stackedHistograms:
                    legendOption = 'f'
                elif drawOption in ['HIST']:
                    legendOption = 'l'
                    if histogram.GetFillStyle() and histogram.GetFillColor():
                        legendOption = 'f'
                elif any( x in drawOption for x in ['E0', 'E1']):
                    legendOption = 'pel'  
                elif any( x in drawOption for x in ['E2', 'E3', 'E4', 'E5']):
                    legendOption = 'f'
                else:
                    legendOption = 'p'
                self.legendElements.append( LegendElement( histogram, title, legendOption ) )
        self.add( histogram )
        return histogram
    
    ## Adds a TGraph to the list of objects to be drawn.
    #  The graph will not appear in the legend if the title is empty.
    #  There is no need to pass "SAME" in the draw option, this is handled automatically.
    #  @param graph        TGraph object
    #  @param drawOption   option used to draw the object (see https://root.cern/doc/master/classTGraphPainter.html#GP01, optional, default is "P")
    #  @param copy         make a copy of the histogram instead of using the object itself (optional, default is False)
    #  @return the graph object (copied version in case of copy=True)
    def addGraph( self, graph, drawOption='P', copy=False ):
        if not graph:
            return
        name = graph.GetName()
        if copy:
            graph = graph.Clone( appendUUID( name ) )
        self.graphsByName[ name ] = graph
        self.optionsDict[ graph ] = drawOption
        self.graphs.append( graph )
        self.add( graph )
        # add a corresponding legend element
        title = graph.GetTitle()
        if title:
            legendOption = ''
            if 'X' not in drawOption or any( x in drawOption for x in ['L', 'C']):
                legendOption += 'L'
            if any( x in drawOption for x in ['2', '3', '4', '5']):
                legendOption += 'F'
            if any( x in drawOption for x in ['P']):
                legendOption += 'P'
            self.legendElements.append( LegendElement( graph, title, legendOption ) )
        return graph
    
    ## Adds a TF1 object which is linked to the given object. When BasicPlot::draw() is called
    #  obj.Fit( function ) will be called. Make sure obj is in the list of drawn objects or call
    #  obj.Draw() yourself in order to allow the fit to succeed.
    #  @param obj          object which should be fitted
    #  @param function     TF1 object used in the fit
    #  @param drawOption   option used to draw the object (optional, default is no option)
    #  @param fitOption    option passed to obj.Fit (see https://root.cern.ch/doc/master/classTH1.html#a7e7d34c91d5ebab4fc9bba3ca47dabdd, optional, default is "QN0")
    #  @param xMin         lower limit of the fit range (optional, default is the lower limit of the x variable)
    #  @param xMax         upper limit of the fit range (optional, default is the upper limit of the x variable)
    #  @param legendTitle  title used in the plot legend. If empty no entry willl be made for the fit function (optional, default is an empty string)
    #  @param copy         make a copy of the fit function instead of using the object itself (optional, default is False)
    #  @return TF1 object (useful if copy=True to get the copied object)
    def addFitFunction( self, obj, function, drawOption='', fitOption='QN0', xMin=None, xMax=None, legendTitle='', copy=False ):
        name = function.GetName()
        if copy:
            function = function.Clone( appendUUID( name ) )
        if not self.fitFunctions.has_key( obj ):
            self.fitFunctions[ obj ] = []
        function = self.addFunction( function, drawOption, legendTitle, copy)
        self.fitFunctions[ obj ].append( function )
        self.fitOptions[ function ] = fitOption
        if function.GetXmin() == 0. and function.GetXmax() == 1.:
            if not xMin:
                xMin = self.variableX.binning.low
            if not xMax:
                xMax = self.variableX.binning.high
            function.SetRange( xMin, xMax )
        return function
    
    ## Adds a TF1 object to the list of objects to be drawn.
    #  The function will not appear in the legend if no legend title is set.
    #  There is no need to pass "SAME" in the draw option, this is handled automatically.
    #  @param function     TF1 object
    #  @param drawOption   option used to draw the object (see https://root.cern/doc/master/classTGraphPainter.html#GP01, optional, default is "P")
    #  @param legendTitle  title to be used for the legend entry
    #  @param copy         make a copy of the histogram instead of using the object itself (optional, default is False)
    #  @return the TF1 object (copied version in case of copy=True)
    def addFunction( self, function, drawOption='', legendTitle='', copy=False ):
        name = function.GetName()
        if copy:
            function = function.Clone( appendUUID( name ) )
        self.functionsByName[ name ] = function
        self.optionsDict[ function ] = drawOption
        self.functions.append( function )
        self.add( function )
        # add a corresponding LegendEntry
        if legendTitle:
            legendOption = 'L'
            self.legendElements.append( LegendElement( function, legendTitle, legendOption ) )
        return function
    
    ## Add arrows inidcating points of the given object that would fall outside of the y-axis range.
    #  @param obj      TH1 or TGraph object that should be checked
    #  @param style    Style object used to define the arrow style
    def addOutOfRangeArrows( self, obj, style=Style( 2, lineWidth=2, fillStyle=1001 ) ):
        self._drawOutOfRangeArrows[ obj ] = style
    
    def draw( self ):
        Canvas.draw( self )
        self.__initAction__()
        self.__mainAction__()
        self.__drawObjects__()
        self.__finalizeAction__()
        self.__drawDecorations__()
    
    def __mainAction__( self ):
        # Normalizes all bins in all histograms, i.e. divides their entries by the bin width.
        # Only works for objects that have the same number of bins or points as the number of
        # bins defined in the x variable.
        if self.variableX and self.variableX.binning.normaliseByBinWidth and self.normalizeByBinWidth:
            # check which histograms and graphs match the number of bins on the x axis
            nBins = self.variableX.binning.nBins
            histograms = [h for h in self.histograms if h.GetNbinsX() == nBins]
            graphs = [g for g in self.graphs if g.GetN() == nBins]
            # need double pointer to retrive x and y from TGraph
            from ROOT import Double
            x, y = (Double(0),Double(0))
            referenceHist = self.variableX.createHistogram('binning')
            for iBin in range( 1, nBins+1 ):
                binWidth = referenceHist.GetBinWidth( iBin )
                if binWidth > 0.:
                    for histogram in histograms:
                        histogram.SetBinContent( iBin, histogram.GetBinContent( iBin ) / binWidth )
                        histogram.SetBinError( iBin, histogram.GetBinError( iBin ) / binWidth )
                    for graph in graphs:
                        # point index is shifted by -1 since TGraph has no overflow bin
                        graph.GetPoint( iBin-1, x, y )
                        ex1 = graph.GetErrorXlow( iBin-1 )
                        ex2 = graph.GetErrorXhigh( iBin-1 )
                        ey1 = graph.GetErrorYlow( iBin-1 )
                        ey2 = graph.GetErrorYhigh( iBin-1 )
                        graph.SetPoint( iBin-1, x, y/binWidth )
                        graph.SetPointError( iBin-1, ex1, ex2, ey1/binWidth, ey2/binWidth )
        # Normalizes all histograms, i.e. set their integral to 1
        if self.normalized:
            for histogram in self.histograms + self.histograms2D:
                if not histogram.GetSumw2N():
                    histogram.Sumw2()
                integral = histogram.Integral()
                if integral > 0. and not integral == 1.:
                    histogram.Scale( 1. / integral )
        # Perform a fit to all objects for which a fit function is defined
        for obj, fitFunctions in self.fitFunctions.items():
            for function in fitFunctions:
                self.fitResults[ function ] = obj.Fit( function, self.fitOptions[ function ] )
    
    # Normalizes all bins in all histograms, i.e. divides their entries by the bin width.
    # Only works for objects that have the same number of bins or points as the number of
    # bins defined in the 
    def __normalizeByBinWidth__( self ):
        from ROOT import Double, TH1
        if not isinstance( self._firstDrawnObject, TH1 ):
            return
        if not self.variableX.binning.normaliseByBinWidth:
            return
        # check which histograms and graphs match the number of bins on the x axis
        nBins = self._firstDrawnObject.GetNbinsX()
        histograms = [h for h in self.histograms if h.GetNbinsX() == nBins]
        graphs = [g for g in self.graphs if g.GetN() == nBins]
        # require double pointer to retrive x and y from TGraph
        x, y = (Double(0),Double(0))
        for iBin in range( 1, nBins+1 ):
            binWidth = self._firstDrawnObject.GetBinWidth( iBin )
            if binWidth > 0.:
                for histogram in histograms:
                    histogram.SetBinContent( iBin, histogram.GetBinContent( iBin ) / binWidth )
                    histogram.SetBinError( iBin, histogram.GetBinError( iBin ) / binWidth )
                for graph in graphs:
                    # point index is shifted by -1 since TGraph has no overflow bin
                    graph.GetPoint( iBin-1, x, y )
                    ex1 = graph.GetErrorXlow( iBin-1 )
                    ex2 = graph.GetErrorXhigh( iBin-1 )
                    ey1 = graph.GetErrorYlow( iBin-1 )
                    ey2 = graph.GetErrorYhigh( iBin-1 )
                    graph.SetPoint( iBin-1, x, y/binWidth )
                    graph.SetPointError( iBin-1, ex1, ex2, ey1/binWidth, ey2/binWidth )

    # Normalizes all histograms, i.e. set their integral to 1
    def __normalizeObjects__( self ):
        for histogram in self.histograms + self.histograms2D:
            if not histogram.GetSumw2N():
                histogram.Sumw2()
            integral = histogram.Integral()
            if integral > 0. and not integral == 1.:
                histogram.Scale( 1. / integral )

    # Perform a fit to all objects for which a fit function is defined
    def __fitObjects__( self ):
        for obj, fitFunctions in self.fitFunctions.items():
            for function in fitFunctions:
                self.fitResults[ function ] = obj.Fit( function, self.fitOptions[ function ] )

    # Draw all objects including histograms, graphs and functions and any other object registered
    def __drawObjects__( self ):
        # first draw the 2D histograms
        for histogram in self.histograms2D:
            self.__drawHistogram2D__( histogram )
        # then draw the histogram stack
        self.__drawHistogramStack__()
        # now loop over objects and draw them
        for obj in self._objects:
            if obj in self.histograms2D:
                continue
            elif obj in self.histograms:
                self.__drawHistogram__( obj )
            elif obj in self.graphs:
                self.__drawGraph__( obj )
            elif obj in self.functions:
                self.__drawFunction__( obj )
            else:
                obj.Draw()
        # redraw axis to ensure tick marks are not covered
        if self._firstDrawnObject:
            self._firstDrawnObject.Draw( 'SAMEAXIS' )

    # Takes care of drawing a 2D histogram object.
    # Also takes care of calculating axis ranges, etc.
    def __drawHistogram2D__( self, hist ):
        drawOption = self.optionsDict[ hist ]
        if not self._firstDrawnObject:
            self._firstDrawnObject = hist
            if 'z' in drawOption.lower():
                self.canvas.SetRightMargin( 0.2 )
        else:
            drawOption += 'SAME'
        hist.Draw( drawOption )
        # make room for z color palette if necessary
        if 'z' in drawOption.lower() and hist is self._firstDrawnObject:
            self.canvas.Update()
            self.palette = hist.GetListOfFunctions().FindObject( 'palette' )
            self.palette.SetX1NDC( 0.8 )
            self.palette.SetX2NDC( 0.86 )
            self._firstDrawnObject.GetZaxis().SetTitleOffset( 1.4 )

    # Takes care of drawing the histogram stack
    # Also takes care of calculating axis ranges, etc.
    def __drawHistogramStack__( self ):
        if self.stackedHistograms:
            from ROOT import THStack, SetOwnership
            self._histogramStack = THStack()
        for hist in reversed(self.stackedHistograms):
            fillStyle = hist.GetFillStyle()
            fillColor = hist.GetFillColor()
            lineColor = hist.GetLineColor()
            # set the line color as fill color if a fill style is defined
            if fillStyle and lineColor and not fillColor:
                hist.SetFillColor( lineColor )
                hist.SetLineColor( self.stackedOutlineColor )
                hist.SetLineWidth( self.stackedOutlineWidth )
            elif fillColor and not fillStyle:
                hist.SetFillStyle( 1001 )
                hist.SetLineColor( self.stackedOutlineColor )
                hist.SetLineWidth( self.stackedOutlineWidth )
            self._histogramStack.Add( hist, self.optionsDict[hist] )
            if not self._firstDrawnObject:
                # create an empty histogram to handle the drawing of the axis
                self._firstDrawnObject = hist.Clone( appendUUID( hist.GetName() ) )
                # let ROOT take care of this cleanup since we don't guarantee to keep this
                SetOwnership( self._firstDrawnObject, False )
                self._firstDrawnObject.Reset()
                self._firstDrawnObject.Draw()
        if self.stackedHistograms:
            # update plot min/max
            minBin = self.stackedHistograms[0].GetMinimumBin()
            histMin = self.stackedHistograms[0].GetBinContent(minBin)
            if self._minimum > histMin:
                self._minimum = histMin
            if self._maximum < self._histogramStack.GetMaximum():
                self._maximum = self._histogramStack.GetMaximum()
            # actual draw operation
            self._histogramStack.Draw( 'SAME' )
    
    # Takes care of drawing a histogram.
    # Also takes care of calculating axis ranges, etc.
    def __drawHistogram__( self, hist ):
        drawOption = self.optionsDict[ hist ].upper()
        # don't draw stacked histograms (done by drawHistogramStack)
        if hist not in self.stackedHistograms:
            # update plotting range
            minBin = hist.GetMinimumBin()
            histMin = hist.GetBinContent(minBin)
            maxBin = hist.GetMaximumBin()
            histMax = hist.GetBinContent(maxBin)
            if 'e' in drawOption.lower():
                histMin -= hist.GetBinErrorLow(minBin)
                histMax += hist.GetBinErrorUp(maxBin)
            if histMin < self._minimum:
                self._minimum = histMin
            if histMax > self._maximum:
                self._maximum = histMax
            if self._firstDrawnObject:
                drawOption += 'SAME'
            else:
                self._firstDrawnObject = hist
            hist.Draw( drawOption )
    
    # Takes care of drawing a graph
    # Also takes care of calculating axis ranges, etc.
    def __drawGraph__( self, graph ):
        drawOption = self.optionsDict[ graph ].upper()
        if not self._firstDrawnObject:
            self._firstDrawnObject = self.variableX.createHistogram()
            self._firstDrawnObject.Draw()
        if graph.GetN() > 0:
            from ROOT import Double
            xMin, xMax, yMin, yMax = (Double(), Double(), Double(), Double())
            graph.ComputeRange( xMin, yMin, xMax, yMax )
            if self._minimum > yMin:
                self._minimum = yMin
            if self._maximum < yMax:
                self._maximum = yMax
            graph.Draw( drawOption )

    # draws all function objects
    def __drawFunction__( self, function ):
        drawOption = self.optionsDict[ function ]
        if self._firstDrawnObject:
            drawOption += 'SAME'
        else:
            self._firstDrawnObject = self.variableX.createHistogram()
            self._firstDrawnObject.Draw()
        function.Draw( drawOption )
        
    # initialization hook, takes care of modifying any member variables for later use
    def __initAction__( self ):
        # keep track of the previous first drawn object
        self._previousFirstDrawnObject = self._firstDrawnObject
        # reset some values in case it was drawn before
        self._firstDrawnObject = None
        self._minimum = float('inf')
        self._maximum = float('-inf')
        del self._decoratorObjects[:]
        
    # draws all objects created by PlotDecorator objects as well as text elements
    def __drawDecorations__( self ):
        decorators = self.decorators
        if self.drawTitle and self.titleDecorator:
            decorators.append( self.titleDecorator )
        if self.drawLegend and self.legendDecorator:
            decorators.append( self.legendDecorator )
        for decorator in decorators:
            decorator.decorate( self )
        for textElement in self.textElements:
            self._decoratorObjects.append( textElement.getTLatex() )
        for obj, style in self._drawOutOfRangeArrows.iteritems():
            if obj not in self._objects:
                continue
            self._decoratorObjects += createOutOfRangeArrows( obj, self._minimum, self._maximum, style=style )
        for obj in self._decoratorObjects:
            obj.Draw()
    
    # finalization hook, takes care setting axis ranges and setting log axes if requested
    # pre-set ranges override automatic range
    def __finalizeAction__( self ):
        # treat axis labels
        if not self._firstDrawnObject:
            return
        xAxis = self._firstDrawnObject.GetXaxis()
        yAxis = self._firstDrawnObject.GetYaxis()
        try:
            zAxis = self._firstDrawnObject.GetZaxis()
        except:
            zAxis = None
        if xAxis:
            # no exponents on x-axis
            xAxis.SetNoExponent( True )
            if self.variableX:
                self.variableX.applyToAxis( xAxis )
        if yAxis:
            if self.variableY:
                self.variableY.applyToAxis( yAxis )
            from ROOT import TH2
            if xAxis and xAxis.GetNbins() > 1 and self.showBinWidthY and not isinstance( self._firstDrawnObject, TH2 ):
                yTitle = yAxis.GetTitle() + '#kern[-0.5]{ }/'
                if isinstance(binningFromHistogram(self._firstDrawnObject), VariableBinning):
                    if not self.normalizeByBinWidth:
                        yTitle += '#kern[-0.5]{ }Bin'
                    elif self.variableX and self.variableX.unit:
                        yTitle += '#kern[-0.5]{ }%s' % self.variableX.unit
                    else:
                        yTitle += '#kern[-0.5]{ }1'
                else:
                    yTitle += '#kern[-0.5]{ }%.3g' % ((xAxis.GetXmax() - xAxis.GetXmin())/xAxis.GetNbins())
                    if self.variableX and self.variableX.unit:
                        yTitle += '#kern[-0.5]{ }%s' % self.variableX.unit
                yAxis.SetTitle( yTitle )
        if zAxis and self.variableZ:
            self.variableZ.applyToAxis( zAxis )
        
        # update the minimum and maximum if it is defined in the variable binning
        if self.variableY:
            if self.variableY.binning.low is not None:
                if self.variableY.binning.low != 0 or not self.logY:
                    self._minimum = self.variableY.binning.low
            if self.variableY.binning.high is not None:
                self._maximum = self.variableY.binning.high
        
        delta = self._maximum - self._minimum
        if self.logY:
            # make sure we deal with positive values for a log plot
            if self._maximum <= 0.:
                self._maximum = 1.
            if self._minimum <= 0.:
                self._minimum = 0.001 * self._maximum
            # if we have a flat distribution just make up some range for plot
            if self._minimum == self._maximum:
                self._minimum *= 0.1
                self._maximum *= 10.
            # calculate the fraction of the histogram on the pad in log units
            if self.variableY.binning.high is None and self._maximum > 0.:
                delta = math.log10(self._maximum) - math.log10(self._minimum)
                self._maximum = 10**(math.log10(self._maximum) + delta*self.topMargin)
            # calculate the fraction of the histogram on the pad in log units
            if self.variableY.binning.low is None and self._minimum > 0.:
                delta = math.log10(self._maximum) - math.log10(self._minimum)
                self._minimum = 10**(math.log10(self._minimum) - delta*self.bottomMargin)
        else:
            if self.variableY.binning.low is None and self._minimum != 0.:
                # if minimum is close to 0, set minimum to 0 instead
                if abs(self._minimum) < 0.05*delta or (self._minimum > 0 and self._minimum < delta * self.bottomMargin):
                    self._minimum = -0.
                else:
                    # 0 supressed plot, just leave margin to axis
                    self._minimum -= delta * self.bottomMargin
            if self.variableY.binning.high is None:
                self._maximum += delta * self.topMargin

        from ROOT import TH2
        if isinstance( self._firstDrawnObject, TH2 ):
            # For 2D histograms we just care about the minimum and the maximum, no margins required
            if self.variableZ:
                # we need to treat all 2D histograms separately, they don't use the axis of the first 2D histogram!
                for hist in self.histograms2D:
                    if self.variableZ.binning.low is not None:
                        hist.SetMinimum( self.variableZ.binning.low )
                    if self.variableZ.binning.high is not None:
                        hist.firstDrawnObject.SetMaximum( self.variableZ.binning.high )
        else:
            self._firstDrawnObject.GetYaxis().SetRangeUser( self._minimum, self._maximum )
        
        if self.logX:
            self.canvas.SetLogx()
        if self.logY:
            self.canvas.SetLogy()
        if self.logZ:
            self.canvas.SetLogz()
        # scale objects
        if (self._scaleX != 1.0 or self._scaleY != 1.0) and self._firstDrawnObject is not self._previousFirstDrawnObject:
            sizeX = self.canvas.GetWw() * self.canvas.GetWNDC()
            sizeY = self.canvas.GetWh() * self.canvas.GetHNDC()
            # axis decoration
            xAxis = self._firstDrawnObject.GetXaxis()
            yAxis = self._firstDrawnObject.GetYaxis()
            # axis tick mark length needs to be corrected for shorter axis due to margins
            xAxis.SetTickLength( xAxis.GetTickLength() * self._scaleY / (1. - self.canvas.GetLeftMargin() - self.canvas.GetRightMargin() ) )
            yAxis.SetTickLength( yAxis.GetTickLength() * self._scaleX / (1. - self.canvas.GetBottomMargin() - self.canvas.GetTopMargin() ) )
            # avoid scaling for fonts defined by pixel size
            if xAxis.GetTitleFont() % 10 != 3:
                xAxis.SetTitleSize( xAxis.GetTitleSize() * self._scaleTextX )
                # title and label offsets only change if the respective dimension of the canvas is smaller
                if sizeX < sizeY:
                    xAxis.SetTitleOffset( xAxis.GetTitleOffset() * self._scaleY / self._scaleTextX )
            else:
                # only title offset needs to be scaled for pixel size fonts
                xAxis.SetTitleOffset( xAxis.GetTitleOffset() * self._scaleY )
            if xAxis.GetLabelFont() % 10 != 3:
                xAxis.SetLabelSize( xAxis.GetLabelSize() * self._scaleTextX )
                # title and label offsets only change if the respective dimension of the canvas is smaller
                if sizeX < sizeY:
                    offset = xAxis.GetLabelOffset()
                    # need to add special treatment if x axis is shortened wrt. reference
                    # see implementation of TGaxis::PaintAxis()
                    if self._scaleX > 1.:
                        offset -= 0.8 * xAxis.GetLabelSize() * (1-self._scaleY/self._scaleTextX)
                    xAxis.SetLabelOffset( offset )
            if yAxis.GetTitleFont() % 10 != 3:
                if sizeX > sizeY:
                    yAxis.SetTitleOffset( yAxis.GetTitleOffset() * self._scaleX / self._scaleTextY )
                yAxis.SetTitleSize( yAxis.GetTitleSize() * self._scaleTextY )
            else:
                # only title offset needs to be scaled for pixel size fonts
                yAxis.SetTitleOffset( yAxis.GetTitleOffset() * self._scaleX )
            if yAxis.GetLabelFont() % 10 != 3:
                yAxis.SetLabelSize( yAxis.GetLabelSize() * self._scaleTextY )
                yAxis.SetLabelOffset( yAxis.GetLabelOffset() * self._scaleX )

if __name__ == '__main__':
    from ROOT import kBlack, kBlue, kRed, kGreen
    from ROOT import TH1D, TF1, TH2D, TF2
    from happy.variable import var_Normalized, Binning
    
    h1 = TH1D( 'hTest1', 'First Test', 40, -4, 4 )
    h1.FillRandom( 'gaus', 1000 )
    h1.SetLineColor( kRed )
    
    h2 = TH1D( 'hTest2', 'Second Test', 40, -4, 4 )
    h2.FillRandom( 'gaus', 500 )
    h2.SetMarkerColor( kBlue )
    h2.SetLineColor( kBlue )
    h2.SetLineWidth( 3 )
    # if no fill style is set, stacked histograms will not be filled
    h2.SetFillStyle( 0 )
    
    h3 = TH1D( 'hTest3', 'Data', 40, -4, 4 )
    h3.FillRandom( 'gaus', 1500 )
    h3.SetLineColor( kBlack )
    
    f = TF1( 'fTest', 'gaus', -4, 4 )
    f.SetLineColor( kGreen+2 )
    
    xVar = Variable( 'Invariant Mass', unit='GeV' )
    
    basicTest = Plot( 'basicTest', xVar, var_Normalized )
    h = basicTest.addHistogram( h1, 'HIST', copy=True )
    basicTest.addFitFunction( h, f, legendTitle='Fit to First' )
    basicTest.addHistogram( h2, 'E0', copy=True )
    basicTest.titles.append( 'My title' )
    basicTest.titles.append( 'My second title' )
    basicTest.normalized = True
    basicTest.draw()
    basicTest.saveIn()
        
    stackedTest = Plot( 'stackedTest', xVar )
    # increase the canvas size but scale all label and text sizes as if it were a 600x600 canvas
    stackedTest.sizeX = 900
    stackedTest.referenceSizeX = 600
    stackedTest.sizeY = 800
    stackedTest.referenceSizeY = 600
    stackedTest.addHistogram( h1, stacked=True )
    stackedTest.addHistogram( h2, stacked=True )
    stackedTest.addHistogram( h3, 'E' )
    stackedTest.draw()
    stackedTest.saveIn()
    
    f2 = TF2("f2","sin(x)*sin(y)/(x*y)",0,5,0,5);
    h2D = TH2D( 'hTest2D', 'Test 2D', 40, -1, 1, 40, -1, 1 )
    h2D.FillRandom( "f2", 10000 )
    test2D = Plot( 'Test2D', Variable( 'x' ), Variable( 'y' ), Variable( 'Entries', binning=Binning(low=-5., high=None) ) )
    test2D.variableX.binning.nDivisions = 505
    test2D.showBinWidthY = False
    test2D.addHistogram( h2D, 'COLZ', False )
    test2D.draw()

    raw_input( 'Continue?' )
