## @package happy
#  @brief Higgs Analysis Plotting in Python
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.style import loadAtlasStyle
import logging

# set the layout of logger messages
logging.basicConfig( level=logging.INFO, format='%(levelname)-8s %(name)s %(message)s' )

# load the ATLAS style macro
loadAtlasStyle()
