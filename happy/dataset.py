## @package happy.dataset
#  @brief Datasets for physics analyses
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.crossSectionDB import crossSectionDB
from happy.cut import Cut
from happy.histogramStore import HistogramStore
from happy.variable import createCutFlowVariable, VariableBinning, var_Yield
from happy.systematics import SystematicsSet, TreeSystematicVariation
from happy.style import Style
from happy.stringTools import string2bool, appendUUID
from happy.histogramTools import overflowIntoLastBins, extractHistogramsFromTDirectory
from happy.miscellaneous import progressBarInt, findAllFilesInPath, getValuesFromTree
from happy import scheduler
from copy import copy
import logging, os, math, hashlib

## Helper method to get the bin content of a given histogram in a Dataset.
#  Calculates the total sum of the bin content over all files associated with
#  the dataset.
#  @param dataset           input Dataset
#  @param histogramName     name of the histogram (can be a full path inside the ROOT file)
#  @param binIndex          index of the bin
#  @result total bin content over all files in the dataset
def getDatasetHistogramBinContent( dataset, histogramName, binIndex ):
    result = 0
    from ROOT import TFile
    for fileName in dataset.resolvedFileNames:
        rootFile = TFile.Open( fileName )
        if rootFile and rootFile.IsOpen():
            h = rootFile.Get( histogramName )
        if h:
            result += h.GetBinContent( binIndex )
        else:
            dataset.logger.error( 'getDatasetHistogramBinContent(): no histogram "%s" available in "%s"' % ( histogramName, fileName ) )
        rootFile.Close()
    return result

## Class to wrap sum of weight calculation.
#  Implement a derived class to cover special cases of how
#  the total sum of weights are stored in a Dataset.
#  This base class always returns 0.
class SumOfWeightsCalculator( object ):
    logger = logging.getLogger( __name__ + '.SumOfWeightsCalculator' )
    ## Default constructor
    def __init__( self, sumOfWeights=0. ):
        ## Cache of the sum of weights
        self.sumOfWeights = sumOfWeights
        ## Store if the sum of weights has been calculated already
        self.calculated = False
    
    ## Calculates the sum of weights for the given Dataset
    #  @param dataset     input dataset
    #  @return the sum of weights
    def calculate( self, dataset ):
        return self.sumOfWeights

## Class to extract the total sum of weights from a histogram.
#  Assumes a histogram present in all ROOT files associated with
#  a Dataset which provides the total sum of weights in a certain
#  bin. This is the default behavior for n-tuples produced by the
#  xTauFramework.
class HistogramSumOfWeightsCalculator( SumOfWeightsCalculator ):
    logger = logging.getLogger( __name__ + '.HistogramSumOfWeightsCalculator' )
    ## Default constructor
    #  @param histogramName    name of the histogram to use
    #  @param binIndex         index of the bin containing the sum of weights
    def __init__( self, histogramName='h_metadata', binIndex=8 ):
        SumOfWeightsCalculator.__init__( self )
        ## Name of the histogram
        self.histogramName = histogramName
        ## Index of the bin containing the sum of weights
        self.binIndex = int(binIndex)
    
    ## Calculate the sum of weights for the given Dataset
    #  @param dataset     input dataset
    #  @return the sum of weights
    def calculate( self, dataset ):
        if self.calculated:
            self.logger.debug( 'calculate(): metadatahist=%s , binIndex=%g, sum of weights=%g' % ( self.histogramName,self.binIndex,self.sumOfWeights ) )
            return self.sumOfWeights
        self.sumOfWeights = getDatasetHistogramBinContent( dataset, self.histogramName, self.binIndex )
        # protect for floating point precision to avoid tiny sum of weights
        if abs(self.sumOfWeights) < 1e-09:
            self.sumOfWeights = 0
            self.logger.error( 'calculate(): no useful sumOfWeights could be extracted for %r' % ( dataset ) )
        self.logger.debug( 'calculate(): metadatahist=%s , binIndex=%g, sum of weights=%g' % ( self.histogramName,self.binIndex,self.sumOfWeights ) )
        return self.sumOfWeights

## Class to store friend trees for a Dataset.
#  These files are opened automatically together with the dataset
#  and the corresponding trees are attached as friends to the main
#  trees of the dataset. Also allows to set an alias that can be used
#  in the draw expression instead of the tree name in case of name
#  clashes. In addition, branch names to define a major and minor index
#  can be defined. These indices are used to map events in the friend
#  tree to events in the main tree. Both friend tree and main tree
#  need to have the index branches available for the mapping to work.
class FriendTree( object ):
    logger = logging.getLogger( __name__ + '.FriendTree' )
    
    ## Default constructor
    #  @param fileNames              list of file names
    #  @param treeName               name of the friend tree in the files
    #  @param alias                  alias of the friend tree (optional, use in case of clash of branch names with the main tree)
    #  @param systematicVariations   list of TreeSystematicVariation objects this friend tree should be used (empty list is used for all)
    def __init__( self, treeName, alias='', indexMajor=None, indexMinor=None, fileNames=[], systematicVariations=[] ):
        ## List of file names
        self.fileNames = fileNames
        ## Name of the friend tree in all files
        self.treeName = treeName
        ## Alias used to be used in draw expressions
        self.alias = alias
        ## List of systematic variations to which this friend tree should be applied
        self.systematicVariations = systematicVariations
        ## Primary branch name to map events from main tree to events in the friend tree.
        #  For example "run_number".
        self.indexMajor = indexMajor
        ## Secondary branch name to map events from main tree to events in the friend tree.
        #  For example "event_number"
        self.indexMinor = indexMinor
        ## The friend tree
        self.tree = None
    
    # Helper method to open all files
    def _open( self ):
        from ROOT import TChain
        self.tree = TChain( self.treeName )
        nFiles = 0
        for fileNamePattern in self.fileNames:
            for fileName in findAllFilesInPath( fileNamePattern ):
                nFiles += self.tree.Add( fileName )
        if nFiles:
            # update the estimate for this TChain, needed for proper use of GetSelectedRows
            self.tree.SetEstimate( self.tree.GetEntries() + 1 )
            self.logger.debug( '_open(): opened %d files with %d entries from %r' % ( nFiles, self.tree.GetEntries(), self.fileNames ) )
            if self.indexMajor and self.indexMinor:
                self.logger.debug( '_open(): index variables major %s, minor %s' % ( self.indexMajor, self.indexMinor ) )
                self.tree.GetEntries()
                from ROOT import TTreeIndex
                _index = TTreeIndex(self.tree, self.indexMajor, self.indexMinor)
                self.tree.SetTreeIndex(_index)
        else:
            self.logger.warning( '_open(): found no files for %r in %r' % ( self, self.fileNames ) )

    # Delete the link to the tree allowing the underlying file to be closed.
    # The file might still open if other trees or other references to this tree are stored elsewhere.
    def _close( self ):
        if self.tree:
            del self.tree

    ## Add this tree as a friend to the given tree.
    #  @param tree     tree to which the freind tree should be added
    def addTo( self, tree ):
        treeName = tree.GetName()
        # check if this tree is a suitable friend for this systematic variation
        if self.systematicVariations:
            use = False
            for variation in self.systematicVariations:
                try:
                    if treeName == variation.treeName:
                        use = True
                        break
                except:
                    if treeName == variation:
                        use = True
                        break
                if not use:
                    return
        if not self.tree:
            self._open()
        if not self.tree:
            return
        self.logger.debug( '_addTo(): adding "%s" as friend tree to "%s" with alias %s' % ( self.treeName, treeName, self.alias ) )
        tree.AddFriend( self.tree, self.alias )
        
## Represents an arbitrary dataset and associated information.
#  Allows to generate histograms and calculate yields.
class Dataset( object ):
    logger = logging.getLogger( __name__ + '.Dataset' )
    defaultHistogramStore = None
    ## Default constructor.
    #  @param name               name of the dataset used for output file names, etc. avoid special characters
    #  @param title              title used for example in legend entries (use TLatex syntax)
    #  @param style              Style object applied to all histograms by default
    #  @param crossSection       sample crossSection in pb, applied as global scale factor to all histograms
    #  @param kFactor            correction factor, applied as global scale factor to all histograms
    #  @param isData             this is data (not MC) no scale factors will be applied to histograms
    #  @param isSignal           this is signal MC, stored for example to decide how it is used in MVA training
    def __init__( self, name, title=None, style=None, crossSection=1., kFactor=1., isData=False, isSignal=False ):
        ## name of the dataset, used in file names etc., avoid special characters
        self.name = name
        ## title used for example in legend entries (use TLatex syntax)
        self.title = title if title else name
        ## Style object applied to all histograms by default
        self.style = style
        ## dictionary of string to double. Defines arbitrary scale factors which are applied to all plots.
        self.scaleFactors = {} 
        ## overall scale factor applied inversely to all histograms
        self.sumOfWeights = None
        if crossSection:
            self.crossSection = crossSection
        self.kFactor = kFactor
        ## flag to identify data, no scale factors are applied to histograms
        self.isData = isData
        ## flag to identify signal
        self.isSignal = isSignal
        ## list of Cut objects that will be ignored. The string will be removed from any processed Cut. Use for example to create Datasets with inverted cuts.
        self.ignoredCuts = []
        ## list of Cut objects that will be added. The Cuts will be added to any Cut that is processed. Use for example to create Datasets with inverted cuts.
        self.additionalCuts = []
        ## dictionary of Variable to Variable objects. Use to replace a drawn Variable with another one only for this Dataset.
        self.replaceVariables = {}
        ## SystematicsSet object holding all Systematics object relevant to this Dataset.
        self.systematicsSet = SystematicsSet()
        ## HistogramStore used to store and lookup histograms via canonical paths
        self.histogramStore = self.defaultHistogramStore
        ## DatasetMetadata object holding additional information
        self.metadata = None
        ## nominal systematics
        self.nominalSystematics = TreeSystematicVariation( 'nominal', 'Nominal', 'NOMINAL' )
        ## Cut object defining a common weight expression
        self.weightExpression = Cut()
        self.preselection = Cut()
    
    ## Print this Dataset with various information
    def __str__( self ):
        return 'Dataset(%s): XS=%g pb, effXS=%g pb, sF=%r' % (self.name, self.crossSection, self.effectiveCrossSection, self.scaleFactors)


    ## Cross section in pb, overall normalisation applied to all histograms.
    @property 
    def crossSection( self ):
        return self.scaleFactors['crossSection']
    
    @crossSection.setter 
    def crossSection( self, value ):
        self.scaleFactors['crossSection'] = value
    
    ## k-factor, overall normalisation applied to all histograms.
    @property 
    def kFactor( self ):
        return self.scaleFactors['kFactor']
    
    @kFactor.setter 
    def kFactor( self, value ):
        self.scaleFactors['kFactor'] = value
        
    ## Effective cross section including all scale factors in pb.
    @property
    def effectiveCrossSection( self ):
        return self.combinedScaleFactors * self.systematicsSet.totalScaleFactor()
        
    ## Product of all global scale factors.
    @property
    def combinedScaleFactors( self ):
        product = 1.0
        for value in self.scaleFactors.itervalues():
            product *= value
        return product
    
    ## SystematicsSet applied to this Dataset.
    @property
    def combinedSystematicsSet( self ):
        return self.systematicsSet
    
    ## Number of entries in this Dataset.
    @property
    def entries( self ):
        return 0
    
    ## Cut object defining a common preselection
    @property
    def preselection( self ):
        return self._preselection
    
    @preselection.setter
    def preselection( self, value ):
        self._preselection = value
    
    ## MD5 hash to uniquely identify this Dataset.
    @property
    def md5( self ):
        md5 = hashlib.md5()
        md5.update( self.title )
        # include the sum of weights in the hash
        #md5.update( str(self.sumOfWeights) )
        return md5.hexdigest()
    
    ## List of all nested Dataset objects.
    @property
    def nestedDatasets( self ):
        return [self]
    
    # helper method to determine final Variable to be used
    def _determineVariable( self, variable ):
        if self.replaceVariables.has_key( variable ):
            return self.replaceVariables[ variable ]
        return variable
    
    # helper method to determine final Cut to be used
    def _determineCut( self, cut ):
        cut = Cut() + cut
        for ignoredCut in self.ignoredCuts:
            cut = cut.withoutCut( ignoredCut )
        for additionalCut in self.additionalCuts:
            cut += additionalCut
        return cut
    
    # helper method for the actual histogram creation
    def _getHistogram( self, xVar, cut, weightExpression, systematicVariation ):
        raise NotImplementedError()
    
    # helper method for the actual 2D histogram creation
    def _getHistogram2D( self, xVar, yVar, cut, weightExpression, systematicVariation ):
        raise NotImplementedError()
    
    # helper method for the actual value calculation
    def _getValues( self, xVar, cut, systematicVariation ):
        raise NotImplementedError()
    
    ## Creates a histogram from this dataset for the given selection.
    #  If a HistogramStore is defined it will first try to find the histogram in the store. If it does not exist the histogram will be
    #  created as usual and afterwards placed in the HistogramStore. Scale factor, cross section and kFactor are not persisted and always
    #  applied in this command.    The "recreate" option allows to override existing histograms if they are already in the HistogramStore.
    #  @param xVar                 Variable object that defines the variable expression used to fill the histogram as well as its binning
    #  @param cut                  Cut object that defines the selection (optional)
    #  @param luminosity           global scale factor, i.e. integrated luminosity, not applied if isData is True (optional, default is 1)
    #  @param systematicVariation  SytematicVariation which defines the tree name and/or modified weight expression (optional, default uses nominal tree for this dataset).
    #  @param systematicsSet       additional Systematics objects that will be considered. Used to pass Systematics from PhysicsProcess objects that contain this dataset (optional).
    #  @param recreate             force recreation of the histogram if it exists in the HistogramStore. Will replace the previously stored histogram (optional, default is False).
    #  @return TH1
    def getHistogram( self, xVar, cut=None, luminosity=1., systematicVariation=None, systematicsSet=None, recreate=True ):
        self.logger.debug( 'getHistogram(): creating histogram for var=%s with cut=%s and syst=%s from %s' % (xVar, cut, systematicVariation, self) )
        cut = self._determineCut( cut )
        xVar = self._determineVariable( xVar )
        # determine final SystematicVariation
        systematicVariation = systematicVariation if systematicVariation else self.nominalSystematics
        systematicsSet = self.systematicsSet.union( systematicsSet ) if systematicsSet else self.systematicsSet
        systematics = systematicVariation.systematics
        if not systematics or systematics not in systematicsSet:
            systematicVariation = self.nominalSystematics
        storeSystematicVariation = systematicVariation if systematicVariation.isShapeSystematics else self.nominalSystematics

        
        # print 'self weight: '
        #print self.weightExpression
        #print type(self.weightExpression)
        # print type(systematicsSet.totalWeight( systematicVariation, cut ))
        # print systematicsSet.totalWeight( systematicVariation, cut )
        # print 'systematicSet : '
        # print str(systematicsSet.totalWeight( systematicVariation, cut ))

        weightExpression = self.weightExpression * systematicsSet.totalWeight( systematicVariation, cut )
        #weightExpression = self.weightExpression * Cut() 

        # try to get histogram from histogram store
        hist = None
        if self.histogramStore and not recreate:
            hist = self.histogramStore.getHistogram( self, storeSystematicVariation, xVar, cut, weightExpression )
        if hist:
            hist.SetTitle( self.title )
        else:
            hist = self._getHistogram( xVar, cut, weightExpression, storeSystematicVariation )
        # finalisation of histogram
        if hist:
            # treat overflow bins
            overflowIntoLastBins( hist, xVar.binning.includeUnderflowBin, xVar.binning.includeOverflowBin )
            # apply scale factors
            sF = self.combinedScaleFactors * systematicsSet.totalScaleFactor( systematicVariation, cut )
            print "dataset::getHistogram1D::hist.getname(), ", hist.GetTitle()
            print "dataset::getHistogram1D::hist.Integral() =", hist.Integral()
            print "dataset::getHistogram1D::sF =", sF
            if not self.isData:
                sF *= luminosity
                # print "getHistogram SF is "+str(type(sF))+" = "+str(sF)
                #sF *= 1
            print "dataset::getHistogram1D::sF*luminosity =", sF
            hist.Scale( sF )
            print "dataset::getHistogram1D::hist.Integral()2 =", hist.Integral()
            self.logger.debug( 'getHistogram(): scaling histogram by %g, total yield=%g' % (sF, hist.Integral()) )
            # apply styling
            if self.style:
                self.style.applyTo( hist )
            # apply blinding in case of data
            if self.isData:
                xVar.applyBlinding( cut, hist )
        return hist
    
    ## Creates a 2D histogram from this dataset for the given selection.
    #  If a HistogramStore is defined it will first try to find the histogram in the store. If it does not exist the histogram will be
    #  created as usual and afterwards placed in the HistogramStore. Scale factor, cross section and kFactor are not persisted and always
    #  applied in this command.    The "recreate" option allows to override existing histograms if they are already in the HistogramStore.
    #  @param xVar                 Variable object that defines the variable expression used for the x axis to fill the histogram as well as its binning
    #  @param yVar                 Variable object that defines the variable expression used for the y axis to fill the histogram as well as its binning
    #  @param cut                  Cut object that defines the selection (optional)
    #  @param luminosity           global scale factor, i.e. integrated luminosity, not applied if isData is True (optional, default is 1)
    #  @param recreate             force recreation of the histogram if it exists in the HistogramStore. Will replace the previously stored histogram (optional, default is False).
    #  @param systematicVariation  SytematicVariation which defines the tree name and/or modified weight expression (optional, default uses nominal tree for this dataset).
    #  @param systematicsSet       additional Systematics objects that will be considered. Used to pass Systematics from PhysicsProcess objects that contain this dataset (optional).
    #  @param recreate             force recreation of the histogram if it exists in the HistogramStore. Will replace the previously stored histogram (optional, default is False).
    #  @return TH2
    def getHistogram2D( self, xVar, yVar, cut=None, luminosity=1., systematicVariation=None, systematicsSet=None, recreate=False ):
        self.logger.debug( 'getHistogram2D(): creating histogram for xVar=%r and yVar=%r with cut=%r and syst=%r from %r' % (xVar, yVar, cut, systematicVariation, self) )
        cut = self._determineCut( cut )
        xVar = self._determineVariable( xVar )
        yVar = self._determineVariable( yVar )
        # determine final SystematicVariation
        systematicVariation = systematicVariation if systematicVariation else self.nominalSystematics
        systematicsSet = self.systematicsSet.union( systematicsSet ) if systematicsSet else self.systematicsSet
        systematics = systematicVariation.systematics
        if not systematics or systematics not in systematicsSet:
            systematicVariation = self.nominalSystematics
        storeSystematicVariation = systematicVariation if systematicVariation.isShapeSystematics else self.nominalSystematics
        weightExpression = self.weightExpression * systematicsSet.totalWeight( systematicVariation, cut)
        hist = None
        if self.histogramStore and not recreate:
            hist = self.histogramStore.getHistogram2D( self, storeSystematicVariation, xVar, yVar, cut, weightExpression )
        if hist:
            hist.SetTitle( self.title )
        else:
            hist = self._getHistogram2D( xVar, yVar, cut, weightExpression, storeSystematicVariation )
        # apply scale factors
        if hist:
            sF = self.combinedScaleFactors * systematicsSet.totalScaleFactor( systematicVariation, cut )
            print "dataset::getHistogram2D::hist.getname(), ", hist.GetTitle()
            print "dataset::getHistogram2D::hist.Integral() =", hist.Integral()
            print "dataset::getHistogram2D::sF =", sF
            if self.isData:
                xVar.applyBlinding2D( cut, yVar, hist )
            else:
                sF *= luminosity
            print "dataset::getHistogram2D::sF*luminosity =", sF
            hist.Scale( sF )
            print "dataset::getHistogram2D::hist.Integral()2 =", hist.Integral()
            self.logger.debug( 'getHistogram2D(): scaling histogram by %g, total yield=%g' % (sF, hist.Integral()) )
            if self.style:
                self.style.applyTo( hist )
        return hist
    
    ## Creates a TProfile from this dataset for the given selection.
    #  If a HistogramStore is defined it will first try to find the histogram in the store. If it does not exist the histogram will be
    #  created as usual and afterwards placed in the HistogramStore. Scale factor, cross section and kFactor are not persisted and always
    #  applied in this command.    The "recreate" option allows to override existing histograms if they are already in the HistogramStore.
    #  @param xVar                 Variable object that defines the variable expression used for the x axis to fill the histogram as well as its binning
    #  @param yVar                 Variable object that defines the variable expression used for the y axis to fill the histogram as well as its binning
    #  @param cut                  Cut object that defines the selection (optional)
    #  @param luminosity           global scale factor, i.e. integrated luminosity, not applied if isData is True (optional, default is 1)
    #  @param recreate             force recreation of the histogram if it exists in the HistogramStore. Will replace the previously stored histogram (optional, default is False).
    #  @param systematicVariation  SytematicVariation which defines the tree name and/or modified weight expression (optional, default uses nominal tree for this dataset).
    #  @param systematicsSet       additional Systematics objects that will be considered. Used to pass Systematics from PhysicsProcess objects that contain this dataset (optional).
    #  @param recreate             force recreation of the histogram if it exists in the HistogramStore. Will replace the previously stored histogram (optional, default is False).
    #  @return TProfile object
    def getProfile( self, xVar, yVar, cut=None, luminosity=1., systematicVariation=None, systematicsSet=None, recreate=False ):
        hist = self.getHistogram2D( xVar, yVar, cut, luminosity, systematicVariation, systematicsSet, recreate )
        prof = None
        if hist:
            prof = hist.ProfileX( 'prof_'+hist.GetTitle(), 1, -1, '' )
        return prof
    
    ## Gets the values and weights for a given variable and selection
    #  @param xVar                 Variable object defining which values should be calculated
    #  @param cut                  Cut object that defines the applied cut
    #  @param luminosity           global scale factor, i.e. integrated luminosity, not applied for data
    #  @param systematicVariation  SytematicVariation object defining the tree name and potential additional weights
    #  @param systematicsSet       additional systematics that should be considered
    #  @return (values, weights)
    def getValues( self, xVar, cut=None, luminosity=1., systematicVariation=None, systematicsSet=None ):
        systematicVariation = systematicVariation if systematicVariation else self.nominalSystematics
        systematicsSet = self.systematicsSet.union( systematicsSet ) if systematicsSet else self.systematicsSet
        cut = self._determineCut( cut ) * self.weightExpression * systematicsSet.totalWeight( systematicVariation ).cut
        xVar = self._determineVariable( xVar )
        self.logger.debug( 'getValues(): getting values for %r with cut=%r and sytematics=%r from %r' % (xVar, cut, systematicVariation, self) )
        values, weights = self._getValues( xVar, cut, systematicVariation )
        sF = self.combinedScaleFactors * systematicsSet.totalScaleFactor( systematicVariation, cut )
        if not self.isData:
            sF *= luminosity
        return values, weights * sF
    
    ## Create and fill a resolution graph for this dataset, i.e. resolution of y vs. x
    #  @param xVar                 defines the x axis. The binning defines the slicing for different resolution evaluation
    #  @param yVar                 variable for which the resolution is determined 
    #  @param title                title of the graph
    #  @param measure              Measure object to evaluate the resolution (see plotting/ResolutionGraph.py)
    #  @param cut                  Cut object that defines the applied cut
    #  @param luminosity           global scale factor, i.e. integrated luminosity, not applied for data
    #  @param systematicVariation  SytematicVariation object defining the tree name and potential additional weights
    #  @param systematicsSet       additional systematics that should be considered
    #  @return the filled graph
    def getResolutionGraph( self, xVar, yVar, title=None, measure=None, cut='', luminosity=1., systematicVariation=None, systematicsSet=None ):
        self.logger.debug( 'getResolutionGraph(): creating resolution graph for yVar=%r, xVar=%r with cut=%r and syst=%r from %r' % (yVar, xVar, cut, systematicVariation, self) )
        
        bins = xVar.binning.bins
        title = '%s vs %s' % (yVar.title, xVar.title) if title is None else title
        name = appendUUID( 'g%s' % ( title.replace(' ', '_').replace('(', '').replace(')','') ) )
        systematicVariation = systematicVariation if systematicVariation else self.nominalSystematics
    
        from ROOT import TGraphAsymmErrors
        graph = TGraphAsymmErrors( xVar.binning.nBins )
        graph.SetNameTitle( name, title )
        
        yMean, yErrLow, yErrUp = (0., 0., 0.)
        for iBin in xrange( xVar.binning.nBins ):
            low = bins[iBin]
            up = bins[iBin+1]
            xMean = low + 0.5 * (up - low)
            xErr = xMean - low
            myCut = Cut( '%s >= %s && %s < %s' % (xVar.command, low, xVar.command, up) ) + cut + yVar.defaultCut
            values, weights = self.getValues( yVar, myCut, self.weightExpression, luminosity, systematicVariation, systematicsSet )
            if measure:
                yMean, yErrLow, yErrUp = measure.calculateFromValues( values, weights )
            else:
                yMean, yErrLow, yErrUp = ( 0., 0., 0. )
            graph.SetPoint( iBin, xMean, yMean )
            graph.SetPointError( iBin, xErr, xErr, yErrLow, yErrUp )
        
        if graph and self.style:
            self.style.applyTo( graph )
        return graph
    
    ## Calculates the event yield of this dataset for the given selection.
    #  Relies on Dataset::getHistogram(). If a HistogramStore is defined it will first try to find the histogram in the store. If it does
    #  not exist, the histogram will be created as usual and afterwards placed in the HistogramStore. Scale factor, cross section and kFactor
    #  are not persisted and always applied in this command. The "recreate" option allows to override existing histograms if they are already
    #  in the HistogramStore. There is an option to get the raw number of events, i.e. all event weights or scale factors are ignored.
    #  @param cut                  Cut object that defines the selection (optional)
    #  @param luminosity           global scale factor, i.e. integrated luminosity, not applied if isData is True (optional, default is 1)
    #  @param ignoreWeights        if True will return raw number of events, ignoring all event weights and scale factors
    #  @param recreate             force recreation of the histogram if it exists in the HistogramStore. Will replace the previously stored histogram (optional, default is False).
    #  @param systematicVariation  SytematicVariation which defines the tree name and/or modified weight expression (optional, default uses nominal tree for this dataset).
    #  @param systematicsSet       additional Systematics objects that will be considered. Used to pass Systematics from PhysicsProcess objects that contain this dataset (optional).
    #  @return tuple of two floats: (yield, uncertainty) 
    def getYield( self, cut=Cut(), luminosity=1., ignoreWeights=False, systematicVariation=None, systematicsSet=None, recreate=False ):
        hist = self.getHistogram( var_Yield, cut, luminosity, systematicVariation, systematicsSet, recreate )
        if ignoreWeights:
            value = hist.GetEntries()
            error = math.sqrt( value )
        else:
            from ROOT import Double
            error = Double(0.0)
            value = hist.IntegralAndError( 0, hist.GetNbinsX()+2 , error )
        return value, error
    
    ## Create and fill a dictionary of cut objects to corresponding yields for this dataset.
    #  The yield is calculated for each of the cut expressions sucessively and set as the respective
    #  bin content. The x axis bin labels are the titles of the different Cut objects. One can chose
    #  to either evaluate each cut as defined or combine it with the cuts from previous bins.
    #  @param cuts                 list of Cut objects to be executed in the cut flow
    #  @param luminosity           global scale factor, only applied if isData is False (optional, default is 1)
    #  @param ignoreWeights        ignore the weights and any other scale factors including luminosity (optional, default is False)
    #  @param systematicVariation  SytematicVariation object defining the tree name and potential additional weights (optional, default is nominal)
    #  @param accumulateCuts       decide if the cuts should be successively combined, i.e. second cut applied in addition to first etc. (optional, default is True)
    #  @param systematicsSet       additional systematics that should be considered (optional)
    #  @param recreate             force recreation of histograms and override them in HistogramStore (optional, default is False)
    #  @return dictionary of cut to yield
    def getCutFlowYields( self, cuts, luminosity=1., ignoreWeights=False, systematicVariation=None, accumulateCuts=True, systematicsSet=None, recreate=False ):
        result = {}
        myCut = Cut()
        for cut in cuts:
            if accumulateCuts:
                myCut += cut
            else:
                myCut = cut
            result[cut] = self.getYield( myCut, luminosity, ignoreWeights, systematicVariation, systematicsSet, recreate )
        return result
    
    ## Create and fill a cutflow histogram for this dataset.
    #  The yield is calculated for each of the cut expressions sucessively and set as the respective
    #  bin content. The x axis bin labels are the titles of the different Cut objects. One can chose
    #  to either evaluate each cut as defined or combine it with the cuts from previous bins.
    #  @param cuts                 list of Cut objects to be executed in the cut flow
    #  @param luminosity           global scale factor, only applied if isData is False (optional, default is 1)
    #  @param ignoreWeights        ignore the weights and any other scale factors including luminosity (optional, default is False)
    #  @param systematicVariation  SytematicVariation object defining the tree name and potential additional weights (optional, default is nominal)
    #  @param accumulateCuts       decide if the cuts should be successively combined, i.e. second cut applied in addition to first etc. (optional, default is True)
    #  @param systematicsSet       additional systematics that should be considered (optional)
    #  @return TH1
    def getCutflowHistogram( self, cuts, luminosity=1., ignoreWeights=False, systematicVariation=None, accumulateCuts=True, systematicsSet=None ):
        var = createCutFlowVariable( cuts=cuts )
        hist = var.createHistogram( self.title )
        yields = self.getCutFlowYields( cuts, luminosity, ignoreWeights, systematicVariation, accumulateCuts, systematicsSet )
        for index, cut in enumerate( cuts ):
            y, error = yields[cut]
            hist.SetBinContent( index+1, y )
            hist.SetBinError( index+1, error )
        if hist and self.style:
            self.style.applyTo( hist )
        return hist

## Wrapper for a RooAbsPdf that acts like a Dataset.
#  Histograms are produced from the PDF independent of variable or cut.
class PdfDataset( Dataset ):
    
    def __init__( self, name, title, pdf, style=None, crossSection=1., kFactor=1., isData=False, isSignal=False ):
        Dataset.__init__( self, name, title, style, crossSection, kFactor, isData, isSignal )
        
    

## Class to describe a dataset and associated information.
#  Datasets are opened as TChains.
class RootDataset( Dataset ):
    defaultSumOfEventsCalculator = HistogramSumOfWeightsCalculator( 'h_metadata', 7 )
    defaultSumOfWeightsCalculator = HistogramSumOfWeightsCalculator( 'h_metadata', 8 )
    defaultSumOfWeightsSquaredCalculator = HistogramSumOfWeightsCalculator( 'h_metadata', 9 )
    logger = logging.getLogger( __name__ + '.RootDataset' )
    
    ## Default constructor.
    #  @param name               name of the dataset used for output file names
    #  @param title              title used for example in legend entries (use TLatex here)
    #  @param fileNames          list of input file names belonging to this dataset
    #  @param treeName           name of the ROOT tree in each file
    #  @param style              default Style object associated with this dataset
    #  @param weightExpression   default weight expression for this dataset
    #  @param crossSection       sets the crossSection (in pb)
    #  @param kFactor            correction factor applied as scaling to all histograms
    #  @param isData             this is data (not MC) no scale factors will be applied to histograms
    #  @param isSignal           this is signal MC, simply stored to decide how it is used in MVA training
    def __init__( self, name, title='',fileNames=[], treeName='NOMINAL', style=None, weightExpression='', crossSection=1., kFactor=1., isData=False, isSignal=False ):
        self._openTrees = {}
        self._treeEntryLists = {}
        ## Decides if open TTrees are kept in memory or closed after each access
        self.keepTreesInMemory = True
        ## List of file names associated to this dataset
        self.fileNames = fileNames if fileNames else []
        ## List of FrindTree objects associated to this Dataset
        self.friendTrees = []
        Dataset.__init__( self, name, title, style, crossSection, kFactor, isData, isSignal )
        self.weightExpression = weightExpression
        self.sumOfEventsCalculator = copy( self.defaultSumOfEventsCalculator )
        self.sumOfWeightsCalculator = copy( self.defaultSumOfWeightsCalculator )
        self.sumOfWeightsSquaredCalculator = copy( self.defaultSumOfWeightsSquaredCalculator )
        self.nominalSystematics.treeName = treeName
        self._hash = 0

    ## Contructor from string used to read in text files
    #  Format is "name; title; treeName; color; crossSection; kFactor; fileName1, fileName2, ... "
    #  @param string    input string
    #  @return new Dataset object
    @classmethod
    def fromString( cls, string ):
        result = [ x.lstrip().rstrip() for x in string.split( ';' ) ]
        name = result[0] if len(result) > 0 else 'DataSet'
        title = result[1] if len(result) > 1 else None
        treeName = result[2] if len(result) > 2 else 'NOMINAL'
        lineColor = int(result[3]) if len(result) > 3 else 0
        crossSection = float(result[4]) if len(result) > 4 else 1.
        kFactor = float(result[5]) if len(result) > 5 else 1.
        fileNamesString = result[6] if len(result) > 6 else ''
        fileNames = [ x.strip() for x in fileNamesString.split( ',' ) ]
        return cls( name, title, fileNames, treeName, Style(lineColor), crossSection=crossSection, kFactor=kFactor )

    ## Constructor from an XML element
    #  @code
    #  <Dataset name="" title="" treeName="" isData="" isSignal="" crossSection="" kFactor="" dsid="">
    #    <Style color="5"/>
    #    <File> File1 </File>
    #    <File> File2 </File>
    #    <AddCuts>
    #      <Cut> Cut1 </Cut>
    #      <Cut> Cut2 </Cut>
    #    </AddCuts>
    #    <IgnoreCuts>
    #      <Cut> Cut3 </Cut>
    #      <Cut> Cut4 </Cut>
    #    </IgnoreCuts>
    #  </Dataset>
    #  @endcode
    #  @param element    the XML element
    #  @return new Dataset object
    @classmethod
    def fromXML( cls, element ):
        attributes = element.attrib
        name = attributes[ 'name' ]
        dataset = cls( name )
        if attributes.has_key( 'title' ):
            dataset.title = attributes['title']
        if attributes.has_key( 'treeName' ):
            dataset.treeName = attributes['treeName']
        if attributes.has_key( 'isData' ):
            dataset.isData = string2bool(attributes['isData'])
        if attributes.has_key( 'isSignal' ):
            dataset.isSignal = string2bool(attributes['isSignal'])
        if attributes.has_key( 'isBSMSignal' ):
            dataset.isBSMSignal = string2bool(attributes['isBSMSignal'])
        if attributes.has_key( 'crossSection' ):
            dataset.crossSection = float(attributes['crossSection'])
        if attributes.has_key( 'kFactor' ):
            dataset.kFactor = float(attributes['kFactor'])
        if attributes.has_key( 'weightExpression' ):
            dataset.weightExpression = attributes['weightExpression']
        if attributes.has_key( 'dsid' ):
            dataset.dsid = int( attributes['dsid'] )
        dataset.style = Style.fromXML( element.find( 'Style' ) ) if element.find( 'Style' ) is not None else None
        for fileElement in element.findall( 'File' ):
            dataset.fileNames.append( fileElement.text.strip() )
        if element.find( 'AdditionalCuts' ):
            for cutElement in element.find( 'AdditionalCuts' ).findall( 'Cut' ):
                dataset.additionalCuts.append( Cut.fromXML( cutElement ) )
        if element.find( 'IgnoredCuts' ):
            for cutElement in element.find( 'IgnoredCuts' ).findall( 'Cut' ):
                dataset.ignoredCuts.append( Cut.fromXML( cutElement ) )
        return dataset
        
    def __repr__( self ):
        return 'Dataset(%s)' % self.name
    
    
    @property
    def md5( self ):
        # first check if the list of file names has changed
        if self._hash and self._hashFileNames == self.fileNames:
            return self._hash.hexdigest()
        defaultCheckSize = 2**13    # 1024 bytes
        md5 = hashlib.md5()
        for fileName in self.resolvedFileNames:
            fileSize = os.path.getsize( fileName )
            checkSize = defaultCheckSize if fileSize > defaultCheckSize else fileSize
            with open( fileName , "rb" ) as f:
                # read the first MB
                md5.update( f.read( checkSize ) )
                # go to 1 MB before the end
                f.seek( -checkSize, 2 )
                # read last MB
                md5.update( f.read( checkSize ) )
            # include the file size in the hash
            md5.update( str(fileSize) )
        # include the sum of weights in the hash
        md5.update( str(self.sumOfWeights) )
        # store the hash for later use
        self._hash = md5
        # store the list of file names used to generate the hash
        self._hashFileNames = self.fileNames
        return md5.hexdigest()

    # Read the files into the TChain. Automatically called when trying to create a histogram
    def _open( self, treeName=None ):
        self.logger.debug( '_open(): Opening tree %s' % (treeName) )
        if not treeName:
            treeName = self.nominalSystematics.treeName
            self.logger.debug( '_open(): Set nominal tree %s'%treeName )
        self.logger.debug( '_open(): Checking if tree is open' )
        self.logger.debug( '_open(): %s' % self._openTrees )
        if self._openTrees.has_key( treeName ):
            self.logger.debug( '_open(): returning open trees %s' % ( self._openTrees[ treeName ] ) )
            return self._openTrees[ treeName ]
        from ROOT import TChain
        self.logger.debug( '_open(): create chain')
        tree = TChain( treeName )
        nFiles = 0
        for fileName in self.resolvedFileNames:
            self.logger.debug( '_open(): Adding tree from file %s to chain' % fileName )
            #gal: OPen Tchain here, Add Efftree files in correct order, AddFriend to main TChain.
            nFiles += tree.Add( fileName )
        if nFiles>0:
            # update the estimate for this TChain, needed for proper use of GetSelectedRows
            self.logger.debug( '_open(): Update tree estimate' )
            tree.SetEstimate( tree.GetEntries() + 1 )
            if self.keepTreesInMemory:
                self.logger.debug( '_open(): Adding tree to openTrees list' )
                self._openTrees[ treeName ] = tree
            self.logger.debug( '_open(): Apply preselection to tree' )
            self._applyPreselectionToTree( tree )
            self.logger.debug( '_open(): opened %d files with %d entries from %r with sum of weights %g' % ( nFiles, tree.GetEntries(), self.fileNames, self.sumOfWeights ) )
            # add friend trees
            self.logger.debug( '_open(): %s has friendTrees %s' % (self.name,self.friendTrees ) )
            for friend in self.friendTrees:
                friend.addTo( tree )
        else:
            self.logger.warning( '_open(): found no files for %r in %r' % ( self, self.fileNames ) )
        return tree
    
    def _close( self, treeName ):
        if self._openTrees.has_key( treeName ):
            self.logger.debug( '_close(): closing trees %s' % ( self._openTrees[ treeName ] ) )
            del self._openTrees[ treeName ]
        
        for fTree in self.friendTrees:
            if not treeName in fTree.treeName: continue
            if self.nominalSystematics.treeName in treeName: continue
            self.logger.debug( '_close(): freeing friendTrees %s for DS %s, treeName %s' % ( fTree.treeName, self.name,treeName) )
            if fTree.tree:
                fTree.tree.GetCurrentFile().Close()
            fTree.tree=None
    
    # helper method to apply preselection using TEntryList
    def _applyPreselectionToTree( self, tree ):
        treeName = tree.GetName()
        selection = self.preselection.cut
        listName = 'entryList_%s_%s' % ( treeName, self.name ) 
        entryList = None
        if tree.GetEntryList():
            # we already had set an entry list, replace with NULL
            tree.SetEntryList( 0 )
            self.logger.debug( '_applyPreselectionToTree(): removing preselection for %s in %r' % ( tree, self ) )
        if not selection:
            # nothing to select simply return
            return
        if self._treeEntryLists.has_key( treeName ):
            entryList = self._treeEntryLists[ treeName ]
        else:
            tree.Draw( '>>' + listName, selection, 'entrylist' )
            from ROOT import gDirectory
            entryList = gDirectory.Get( listName )
            self._treeEntryLists[ treeName ] = entryList
        if entryList:
            tree.SetEntryList( entryList )
    
    # helper method for the actual histogram creation
    def _getHistogram( self, xVar, cut, weightExpression, systematicVariation ):
        if scheduler.isActive and self.histogramStore:
            hist = xVar.createHistogram( self.title )
            scheduler.addQuery( hist, self, systematicVariation, xVar, None, cut, weightExpression )
        else:
            tree = self._open( systematicVariation.treeName )
            hist = xVar.createHistogramFromTree( tree, self.title, cut * weightExpression )
            if hist:
                # scale the histogram and store it in the HistogramStore
                if not self.isData and self.sumOfWeights:
                    hist.Scale( 1. / self.sumOfWeights )
                self.logger.debug( '_getHistogram: created histogram with yield %s' % hist.Integral() )
                if self.histogramStore:
                    self.histogramStore.putHistogram( self, systematicVariation, xVar, cut, weightExpression, hist )
            self._close( systematicVariation.treeName )
        return hist 
    
    # helper method for the actual 2D histogram creation
    def _getHistogram2D( self, xVar, yVar, cut, weightExpression, systematicVariation ):
        if scheduler.isActive and self.histogramStore:
            hist = xVar.create2DHistogram( yVar, self.title )
            scheduler.addQuery( hist, self, systematicVariation, xVar, yVar, cut, weightExpression )
        else:
            tree = self._open( systematicVariation.treeName )
            hist = xVar.create2DHistogramFromTree( tree, yVar, self.title, cut * weightExpression )
            if hist:
                # scale the histogram and store it in the HistogramStore
                if not self.isData and self.sumOfWeights:
                    hist.Scale( 1. / self.sumOfWeights )
                if self.histogramStore:
                    self.histogramStore.putHistogram2D( self, systematicVariation, xVar, yVar, cut, weightExpression, hist )
            self._close( systematicVariation.treeName )
        return hist
    
    ## Total sum of weights of this dataset.
    #  Required to take into account cut efficiencies correctly. Unless set
    #  by user it is calculated using the sumOfWeightsCalculator.
    @property
    def sumOfWeights( self ):
        if self.__sumOfWeights:
            return self.__sumOfWeights
        else:
            return self.sumOfWeightsCalculator.calculate( self )
    
    @sumOfWeights.setter
    def sumOfWeights( self, expression ):
        self.__sumOfWeights = expression
    
    ## Number of entries in the dataset
    @property
    def entries( self ):
        tree = self._open( self.nominalSystematics.treeName )
        return tree.GetEntries()
    
    @Dataset.preselection.setter
    def preselection( self, cut=Cut() ):
        if hasattr(self, '_preselection') and cut == self._preselection:
            return
        self._preselection = cut
        # remove all stored TEntryLists
        self._treeEntryLists.clear()
        # apply the preselection to all open trees
        for tree in self._openTrees.itervalues():
            self._applyPreselectionToTree( tree )

    ## The dataset ID.
    #  If the dsid is present in the crossSectionDB, automatically updates
    #  crossSection, kFactor, branchingRatio and filterEfficiency.
    @property
    def dsid( self ):
        return self._dsid
    
    @dsid.setter
    def dsid( self, dsid ):
        if not dsid:
            return
        self._dsid = dsid
        if self.isData:
            return
        try:
            dbEntry = crossSectionDB[ dsid ]
            self.crossSection = dbEntry.crossSection
            self.kFactor = dbEntry.kFactor
            self.scaleFactors['branchingRatio'] = dbEntry.branchingRatio
            self.scaleFactors['filterEfficiency'] = dbEntry.efficiency
        except KeyError:
            self.logger.warning( 'dsid(): unable to find cross section for DSID %d' % dsid )
    
    ## The set of file names after resolving wild card expressions
    @property
    def resolvedFileNames( self ):
        result = set()
        for fileNamePattern in self.fileNames:
            result |= set(findAllFilesInPath( fileNamePattern ))
        return result
    
    ## Store this dataset into a new ROOT file.
    #  The given selection is applied. Only trees registered in the SystematicsSet are stored
    #  in the new file together with all histogram objects found. The histograms are added up
    #  from all input files. In addition, a list of branches to be removed can be defined.
    #  Alternatively a list of branches that should be kept can be defined. In that case all other
    #  will not be stored.
    #  @param fileName        name of the output file
    #  @param selection       event selection applied to the trees (default preselection if defined)
    #  @param keepBranches    list of branches to be kept, all other branches will not be saved
    #  @param removeBranches  list of branches to be removed
    def save( self, fileName, selection=None, keepBranches=[], removeBranches=[] ):
        if selection is None:
            selection = self.preselection.cut if self.preselection else ''
        from ROOT import TFile
        outputFile = TFile.Open( fileName, 'RECREATE' )
        if not outputFile or not outputFile.IsOpen():
            self.logger.error( 'save(): unable to open output file at "%s"' % fileName )
            return
        # copy all trees connected to any systematics
        treeNames = self.systematicsSet.treeNames
        nTrees = len( treeNames ) + 1
        self.logger.info( 'save(): storing %d trees with selection="%s" in %s' % ( nTrees, selection, fileName ) )
        for index, treeName in enumerate( [self.nominalSystematics.treeName] + sorted( treeNames ) ):
            tree = self._open( treeName )
            if not tree.GetEntries():
                continue
            outputFile.cd()
            progressBarInt( index, nTrees, 'Writing: ' + treeName )
            if tree.GetEntries():
                # deactivate all branches except for those to be kept
                if keepBranches:
                    tree.SetBranchStatus( '*', 0 )
                    for branch in keepBranches:
                        tree.SetBranchStatus( branch, 1 )
                # deactivate branches
                for branch in removeBranches:
                    tree.SetBranchStatus( branch, 0 )
                # copy the tree
                newtree = tree.CopyTree( selection )
                # reactivate all branches in the original tree
                tree.SetBranchStatus( '*', 1 )
            else:
                from ROOT import TTree
                newtree = TTree( treeName, treeName )
            newtree.Write()
            self.logger.debug( 'save(): selected %d/%d entries from %s' % ( newtree.GetEntries(), tree.GetEntries(), treeName ) )
        progressBarInt( nTrees, nTrees, 'Done' )
        # now get all histogram objects and add them together
        histograms = {}
        for fileName in self.resolvedFileNames:
            rootFile = TFile.Open( fileName )
            if rootFile and rootFile.IsOpen():
                for path, hist in extractHistogramsFromTDirectory( rootFile ).items():
                    if histograms.has_key( path ):
                        histograms[path].Add( hist )
                    else:
                        outputFile.cd()
                        histograms[path] = hist.Clone( appendUUID( hist.GetName() ) )
                rootFile.Close()
        # store the histograms in the output file
        self.logger.debug( 'save(): storing %d histograms' % ( len( histograms ) ) )
        for path in sorted( histograms.keys() ):
            hist = histograms[path]
            outputFile.cd()
            path, name = os.path.split( path )
            if not outputFile.GetDirectory( path ):
                outputFile.mkdir( path )
            outputFile.cd( path )
            hist.Write( name )
        outputFile.Close()
        
    def addToTmvaFactory( self, factory, cut=Cut(), weightExpression=None, luminosity=1., className='Background', tmvaWeightBranch='TmvaWeight', systematicsSet=None, scaleFactor=1., createWeightControlHist=False, treeType=None ):
        ## Add the tree of this dataset to a TMVA factory.
        #  An in memory copy of the tree is created with the selection cut already applied.
        #  Then a branch with the combined event weight is computed and the tree is added to the TMVA::Factory object
        #  Events with negative weight are kept with positive weight instead to reatin them for increased training statistics
        #  @param factory              TMVA.Factory object to which this tree will be added
        #  @param cut                  Cut object that defines the applied cut
        #  @param weightExpression     weight expression
        #  @param luminosity           global scale factor, i.e. integrated luminosity, not applied for data 
        #  @param className            class used in the classification (usually "Signal" or "Background")
        #  @param systematicsSet       additional systematics that should be considered
        #  @param scaleFactpr          additional scale factor that should be considered
        from ROOT import TTree, TTreeFormula
        self.logger.info( 'addToTMVAFactory: Adding Tree from DS %s' % ( self.name ) )
        
        # in cross eval scheme a tree is added twice (test/train) with different splitting scenarios (even/odd, vv)
        # absolutely must NOT use the same tree twice when adding to factory, line below essential
        self._openTrees={}
        tree = self._open( self.nominalSystematics.treeName )
        systematicsSet = self.systematicsSet.union( systematicsSet ) if systematicsSet else self.systematicsSet
        weightExpression = weightExpression if weightExpression else self.weightExpression
        weightExpression *= systematicsSet.totalWeight( cut=cut )
        self.logger.debug( 'addToTmvaFactory(): systematicsSet %s' % (systematicsSet) )
        self.logger.debug( 'addToTmvaFactory(): weightExpression %s' % (weightExpression) )
        cut = self._determineCut( cut )
        self.logger.debug( 'addToTmvaFactory(): final cut on DS %s' % (cut) )
        
        # copy the relevant entries into a new tree
        nEntries=tree.GetEntries()
        copyTree = tree.CopyTree( cut.cut,"",nEntries )
        
        # remove tree ownership from current directory
        # not sure this has any effect
        copyTree.SetDirectory( 0 )
        
        # create a branch to store the combined event weights
        from array import array
        weightValue = array( 'f', [0.] )
        weightBranch = copyTree.Branch( tmvaWeightBranch, weightValue, tmvaWeightBranch+'/F' )
        
        # suppress some standard ROOT warnings
        import warnings
        warnings.filterwarnings( action='ignore', category=RuntimeWarning, message='creating converter.*' )
        
        # create the TTreeFormula to evaluate the weight expression
        weightFormula = weightExpression.getTTreeFormula( copyTree )
        copyTree.SetNotify(weightFormula)
        
        allWeights=[]
        for i in range(copyTree.GetEntries()):
            copyTree.GetEntry(i)
            weightValue[0] = weightFormula.EvalInstance()
            weightBranch.Fill()
            allWeights.append(weightValue[0])
        
        if copyTree.GetEntries() > 0:
            if not self.isData:
                scaleFactor *= self.combinedScaleFactors * systematicsSet.totalScaleFactor( cut=cut ) / self.sumOfWeights
                scaleFactor *= luminosity
            self.logger.debug( 'addToTmvaFactory(): scaleFactor %s' % (scaleFactor) )
            #adding tree without specifying whether used for training or testing
            #factory.AddTree( copyTree, className, scaleFactor, cut.getTCut() )
            #add tree with train or test defined
            #AddTree (TTree *tree, const TString &className, Double_t weight, const TCut &cut, const TString &treeType)
            if not treeType:
                raise Exception('Could not determine treeType train or test')
            factory.AddTree( copyTree, className, scaleFactor, cut.getTCut(), treeType)

        if createWeightControlHist:
            from ROOT import TH1F
            controlHist=TH1F('controlHistWeights'+self.name+'_'+cut.name,'controlHistWeights'+self.name+'_'+cut.name,2000,-100,100)
            for i in allWeights:
                controlHist.Fill(i)
            return controlHist
        return 0
    
    def _getValues( self, xVar, cut, systematicVariation ):
        tree = self._open( systematicVariation.treeName )
        if not tree:
            return None, None
        return getValuesFromTree( tree, xVar.command, cut.cut )

    ## Create a string representation of this dataset to be used for persistency.
    #  @return string
    def toString( self ):
        s = '%s; %s; %s; %d; %g; %g; ' % ( self.name, self.title, self.nominalSystematics.treeName, self.style.lineColor, self.crossSection, self.kFactor )
        for fileName in self.fileNames:
            s += '%s, ' % fileName
        s = s.rstrip(', ')
        return s
    
class CombinedDataset( Dataset ):
    ## Container class for a set of datasets that should be treated together.
    #  Fulfills Dataset interface so it can be nested, i.e. contain other CombinedDataset objects.
    logger = logging.getLogger( __name__ + '.CombinedDataset' )
    
    ## Default contructor
    #  @param name               name used for output file names, avoid special characters
    #  @param title              title used for example in legend entries (use TLatex here)
    #  @param style              default Style object applied to all histograms by default
    #  @param kFactor            correction factor applied as scaling to all histograms
    #  @param isData             this is data (not MC), simply for book keeping
    #  @param isSignal           this is signal MC, simply stored to decide how it is used in MVA training
    #  @param datasets           list of Dataset objects in this CombinedDataset
    def __init__( self, name, title='', style=None, kFactor=1.0, isData=False, isSignal=False, datasets=None ):
        ## list of Dataset objects contained
        self.datasets = datasets if datasets else []
        Dataset.__init__( self, name, title, style, None, kFactor, isData, isSignal )
    
    def __str__( self ):
        return 'CombinedDataset: %s, %s' % (Dataset.__str__(self), self.datasets)
        
    ## Contructor from string used to read in text files.
    #  Expected format is 
    #  @code
    #  "name; title; color; kFactor; dataset1, dataset2, ... "
    #  @endcode
    #  @param s     string
    #  @return PhysicsProcess object
    @classmethod
    def fromString( cls, s ):
        result = [x.lstrip().rstrip() for x in s.split( ';' )]
        name = result[0] if len(result) > 0 else 'PhysicsProcess'
        title = result[1] if len(result) > 1 else None
        lineColor = int(result[2]) if len(result) > 2 else 0
        kFactor = float(result[3]) if len(result) > 3 else 1.
        datasetsString = result[4] if len(result) > 4 else ''
        datasetStrings = datasetsString.split( ',' )
        datasets = []
        for datasetString in datasetStrings:
            datasetName = datasetString.strip()
            if not datasetName:
                continue
            else:
                cls.logger.error( 'fromString(): Unknown dataset "%s"' % datasetName )
        return cls( name, title, Style(lineColor), kFactor, datasets )
    
    ## Constructor from an XML element.
    #  Expected format is
    #  @code
    #  <PhysicsProcess name="" title="" isData="" isSignal="" kFactor="">
    #    <Style color="5"/>
    #    <Dataset name=""/>
    #    <PhysicsProcess name=""/>
    #    <AddCuts>
    #      <Cut> Cut1 </Cut>
    #      <Cut> Cut2 </Cut>
    #    </AddCuts>
    #    <IgnoreCuts>
    #      <Cut> Cut3 </Cut>
    #      <Cut> Cut4 </Cut>
    #    </IgnoreCuts>
    #  </PhysicsProcess>
    #  @endcode
    #  @param element    XML element
    #  @return PhysicsProcess object
    @classmethod
    def fromXML( cls, element ):
        attributes = element.attrib
        name = attributes[ 'name' ]
        process = cls( name )
        if attributes.has_key( 'title' ):
            process.title = attributes['title']
        if attributes.has_key( 'isData' ):
            process.isData = string2bool(attributes['isData'])
        if attributes.has_key( 'isSignal' ):
            process.isSignal = string2bool(attributes['isSignal'])
        if attributes.has_key( 'isBSMSignal' ):
            process.isBSMSignal = string2bool(attributes['isBSMSignal'])
        if attributes.has_key( 'kFactor' ):
            process.kFactor = float(attributes['kFactor'])
        process.style = Style.fromXML( element.find( 'Style' ) ) if element.find( 'Style' ) is not None else None
        for datasetElement in element.findall( 'Dataset' ):
            process.datasets.append( RootDataset.fromXML(datasetElement) )
        for processElement in element.findall( 'PhysicsProcess' ):
            process.datasets.append( RootDataset.fromXML(processElement) )
        if element.find( 'AdditionalCuts' ) is not None:
            for cutElement in element.find( 'AdditionalCuts' ).findall( 'Cut' ):
                process.additionalCuts.append( Cut.fromXML( cutElement ) )
        if element.find( 'IgnoredCuts' ) is not None:
            for cutElement in element.find( 'IgnoredCuts' ).findall( 'Cut' ):
                process.ignoredCuts.append( Cut.fromXML( cutElement ) )
        return process
    
    def __repr__( self ):
        return 'PhysicsProcess(%s)' % self.name
    
    @property
    def md5( self ):
        md5 = hashlib.md5()
        for dataset in self.datasets:
            md5.update( dataset.md5 )
        return md5.hexhdigest()

    @property 
    def crossSection( self ):
        crossSection = 0.
        for dataset in self.datasets:
            crossSection += dataset.crossSection
        return crossSection
    
    @property
    def effectiveCrossSection(self):
        crossSection = 0.
        for dataset in self.datasets:
            crossSection += dataset.effectiveCrossSection
        return crossSection * self.combinedScaleFactors * self.systematicsSet.totalScaleFactor()
    
    @property
    def entries( self ):
        entries = 0
        for dataset in self.datasets:
            entries += dataset.entries
        return entries
    
    ## List of all included datasets.
    #  Recursively resolves all PhysicsProcess daughters, only returns Dataset objects.
    @property
    def nestedDatasets( self ):
        datasets = []
        for dataset in self.datasets:
            datasets.extend( dataset.nestedDatasets )
        return datasets
    
    @property
    def combinedSystematicsSet( self ):
        result = set( self.systematicsSet )
        for dataset in self.datasets:
            result |= dataset.combinedSystematicsSet
        return result
    
    @Dataset.preselection.setter
    def preselection( self, cut=Cut() ):
        for dataset in self.datasets:
            dataset.preselection = cut
    
    def addToTmvaFactory( self, factory, cut=Cut(), weightExpression=None, luminosity=1., className='Background', tmvaWeightBranch='TmvaWeight', systematicsSet=None, scaleFactor=1., createWeightControlHist=False,treeType=None ):
        ## Add the tree of this dataset to a TMVA factory
        #  An in memory copy of the tree is created with the selection cut already applied.
        #  Then a branch with the combined event weight is computed and the tree is added to the TMVA::Factory object
        #  @param factory              TMVA::Factory object to which this tree will be added
        #  @param cut                  Cut object that defines the applied cut
        #  @param weightExpression     weight expression
        #  @param luminosity           global scale factor, i.e. integrated luminosity, not applied for data 
        #  @param className            class used in the classification (usually "Signal" or "Background")
        #  @param systematicsSet       additional systematics that should be considered
        #  @param scaleFactpr          additional scale factor that should be considered
        systematicsSet = self.systematicsSet.union( systematicsSet ) if systematicsSet else self.systematicsSet
        scaleFactor *= self.combinedScaleFactors
        cut = self._determineCut( cut )
        self.logger.info( 'addToTmvaFactory(): adding %s at cutstage %s' % (self.name,cut.name) )
        self.logger.debug( 'addToTmvaFactory(): scaleFactor %s, systematics %s' % (scaleFactor, systematicsSet) )
        histList=[]
        for dataset in self.datasets:
            conHist=dataset.addToTmvaFactory( factory, cut, weightExpression, luminosity, className, tmvaWeightBranch, systematicsSet, scaleFactor, createWeightControlHist=createWeightControlHist,treeType=treeType  )
            if not isinstance(conHist,list):
                histList.append(conHist)
            else:
                histList+=conHist
        if createWeightControlHist:
            return histList
        return None
    
    def getValues( self, xVar, cut=None, weightExpression=None, luminosity=1., systematicVariation=None, systematicsSet=None ):
        systematicsSet = self.systematicsSet.union( systematicsSet ) if systematicsSet else self.systematicsSet
        import numpy
        values = numpy.empty( [0.] )
        weights = numpy.empty( [0.] )
        cut = self._determineCut( cut )
        xVar = self._determineVariable( xVar )
        for dataset in self.datasets:
            v, w = dataset.getValues( xVar, cut, weightExpression, luminosity, systematicVariation, systematicsSet )
            values = numpy.append( values, v )
            weights = numpy.append( weights, w )
        return values, weights * self.combinedScaleFactors
    
    def getHistogram( self, xVar, cut=None, luminosity=1., systematicVariation=None, systematicsSet=None, recreate=False ):    
        self.logger.debug( 'getHistogram(): creating histogram for var=%r with cut=%r and syst=%r from %r' % (xVar, cut, systematicVariation, self) )
        systematicsSet = self.systematicsSet.union( systematicsSet ) if systematicsSet else self.systematicsSet
        cut = self._determineCut( cut )
        xVar = self._determineVariable( xVar )
        histogram = None
        for dataset in self.datasets:
            h = dataset.getHistogram( xVar, cut, luminosity, systematicVariation, systematicsSet, recreate )
            if not h:
                self.logger.warning( 'getHistogram(): no histogram created for: dataset=%r, var=%r, cut=%r' % ( dataset, xVar, cut ) )
                continue
            if not histogram:
                histogram = h
                histogram.SetTitle( self.title )
            else:
                histogram.Add( h )
        if histogram:
            if self.style:
                self.style.applyTo( histogram )
            histogram.Scale( self.combinedScaleFactors )
            self.logger.debug( 'getHistogram(): scaling histogram by %g, total yield=%g' % (self.combinedScaleFactors, histogram.Integral()) )
        return histogram

    def getHistogram2D( self, xVar, yVar, cut=None, luminosity=1., systematicVariation=None, systematicsSet=None, recreate=False ):
        self.logger.debug( 'getHistogram2D(): creating histogram for var=%r with cut=%r and syst=%r from %r' % (xVar, cut, systematicVariation, self) )
        systematicsSet = self.systematicsSet.union( systematicsSet ) if systematicsSet else self.systematicsSet
        cut = self._determineCut( cut )
        xVar = self._determineVariable( xVar )
        yVar = self._determineVariable( yVar )
        histogram = None
        for dataset in self.datasets:
            h = dataset.getHistogram2D( xVar, yVar, cut, luminosity, systematicVariation, systematicsSet, recreate )
            if not h:
                self.logger.warning( 'getHistogram2D(): no histogram created for: dataset=%r, xVar=%r, yVar=%r, cut=%r' % ( dataset, xVar, yVar, cut ) )
                continue
            if not histogram:
                histogram = h
                histogram.SetTitle( self.title )
            else:
                histogram.Add( h )
        if histogram:
            if self.style:
                self.style.applyTo( histogram )
            histogram.Scale( self.combinedScaleFactors )
        return histogram
    
    def toString( self ):
        s = '%s; %s; %d; %g; ' % (self.name, self.title, self.style.lineColor, self.kFactor)
        for dataset in self.datasets:
            s += '%s, ' % dataset.name
        s = s.rstrip(', ')
        return s

if __name__ == '__main__':
    from ROOT import TTree, TFile, TRandom3, TCut, kBlue, kRed
    from array import array
    from happy.plot import Plot
    from happy.style import redLine, blueLine, greenLine, orangeLine, blackLine
    from happy.variable import Binning, Variable
    from happy.systematics import Systematics
    from happy.analysisTools import createCutflowPlot
    
    #logging.root.setLevel( logging.DEBUG )
     
    # create some dummy trees in a dummy file
    f = TFile( 'temp.root', 'recreate' )
    rndm = TRandom3()
    mass1 = array( 'f', [0] )
    mass2 = array( 'f', [0] )
    mass3 = array( 'f', [0] )
    mass4 = array( 'f', [0] )
    weight = array( 'f', [0] )
    t1 = TTree( 'tree1', 'tree1' )
    t2 = TTree( 'tree2', 'tree2' )
    t3 = TTree( 'tree3', 'tree3' )
    t4 = TTree( 'data', 'data' )
    t1.Branch( 'mass', mass1, 'mass/F' )
    t1.Branch( 'weight', weight, 'weight/F' )
    t2.Branch( 'mass', mass2, 'mass/F' )
    t2.Branch( 'weight', weight, 'weight/F' )
    t3.Branch( 'mass', mass3, 'mass/F' )
    t3.Branch( 'weight', weight, 'weight/F' )
    t4.Branch( 'mass', mass4, 'mass/F' )
    t4.Branch( 'weight', weight, 'weight/F' )
    sumOfWeights = 0.
    for entry in xrange( 50000 ):
        mass1[0] = rndm.Gaus( 5, 8 )
        mass2[0] = rndm.Gaus( 20, 5 )
        mass3[0] = rndm.Gaus( 15, 1 )
        weight[0] = rndm.Gaus( 1.0, 0.1 )
        sumOfWeights += weight[0]
        t1.Fill()
        t2.Fill()
        t3.Fill()
    weight[0] = 1.0
    for entry in xrange( int(1.2*5*2500) ):
        mass4[0] = rndm.Gaus( 5, 8 )
        t4.Fill()
    for entry in xrange( int(1.2*1*2500) ):
        mass4[0] = rndm.Gaus( 20, 5 )
        t4.Fill()
    f.Write()
    f.Close()
 
    # we can define a HistogramStore which will store all created histograms for faster recreation of plots with the same histograms
    Dataset.defaultHistogramStore = HistogramStore( 'hist.root' )
 
    # create the datasets
    dataset1 = RootDataset( 'background1', 'Background 1', ['temp.root'], 'tree1', orangeLine, 'weight', crossSection=5.0 )
    dataset2 = RootDataset( 'background2', 'Background 2', ['temp.root'], 'tree2', greenLine, 'weight', crossSection=1.0 )
    dataset3 = RootDataset( 'signal', 'Signal', ['temp.root'], 'tree3', redLine, 'weight', crossSection=0.1 )
    data = RootDataset( 'data', 'Data', ['temp.root'], 'data', blackLine, 'weight', isData=True )
     
    # alternatively we can also build the physics process from a string representation
    # the format is "name; title; treeName; color; crossSection; kFactor; fileName1, fileName2, ... "
    #dataset21 = RootDataset.fromString( 'background21; Background 1; tree1; 801; 5.0; 1.0; temp.root' )
    #dataset22 = RootDataset.fromString( 'background22; Background 2; tree2; 417; 1.0; 1.0; temp.root' )
    #dataset23 = RootDataset.fromString( 'signal2; Signal; tree3; 632; 0.1; 1.0; temp.root' )
     
    # combine some datasets into a physics process
    backgrounds = CombinedDataset( 'backgrounds', 'Background x 1.2', blueLine, datasets=[dataset1, dataset2] )
    # add another scale factor
    backgrounds.scaleFactors['rQCD'] = 1.2
     
    # alternatively we can also build the physics process from a string representation. The dataset objects with the referenced names have to exist before.
    # the format is "name; title; color; kFactor; dataset1, dataset2, ... "
    backgrounds2 = CombinedDataset.fromString( 'backgrounds2; Background 2; 600; 1.0; background21, background22' )
    
    # manually set the sum of weights
    #for dataset in [ dataset1, dataset2, dataset3, dataset21, dataset22, dataset23 ]:
    for dataset in [ dataset1, dataset2, dataset3 ]:
        dataset.sumOfWeights = sumOfWeights
    
    # define the global lumi scaling in pb-1
    luminosity = 2500.
    
    # get the yield for a certain cut
    print 'Yield from "backgrounds" with "mass>5:"', backgrounds.getYield( Cut('mass > 5'), luminosity=luminosity )
    print 'Yield from "backgrounds" with "mass>15:"', backgrounds.getYield( Cut('mass > 15'), luminosity=luminosity )
     
    # create and draw a cut flow diagram for all datasets
    cutFlowPlot = createCutflowPlot( [dataset1, dataset2, dataset3], [Cut('', 'All Events' ), Cut('mass > 5', 'M > 5 GeV'), Cut('mass > 15', 'M > 15 GeV')], luminosity )
    cutFlowPlot.draw()
     
    # define a variable object for each branch
    massVar = Variable( 'mass', title='M', unit='GeV', binning=Binning(50, 0., 25.) )
    # define some blinded bins, applied to data only
    massVar.blindRegion( Cut(''), 12., 18.)
     
    # define a generic preselection to improve performance
    backgrounds.preselection = Cut( 'mass > 2' )
    data.preselection = Cut( 'mass > 2' )
    
    # create a plot using the Plot class
    testPlot = Plot( 'Dataset Test', massVar )
    # add background and signal datasets as stacked
    testPlot.addHistogram( backgrounds.getHistogram( massVar, luminosity=luminosity ), stacked=True )
    testPlot.addHistogram( dataset3.getHistogram( massVar, luminosity=luminosity ), stacked=True )
    # add lines to indicate the two background components (note that they are automatically weighed by their cross section)
    testPlot.addHistogram( dataset1.getHistogram( massVar, luminosity=luminosity ), drawOption='HIST' )
    testPlot.addHistogram( dataset2.getHistogram( massVar, luminosity=luminosity ), drawOption='HIST' )
    testPlot.addHistogram( data.getHistogram( massVar ), drawOption='E0' )
    # draw the plot
    testPlot.draw()
    
    # example for including systematics
    systematicsPlot = Plot( 'Systematics Test', massVar )
    # define a simple scale uncertainty +10%, -5%
    backgrounds.systematicsSet.add( Systematics.scaleSystematics( 'scaleSyst', upScale=1.1, downScale=0.95 ) )
    # add the nominal histogram with statistical uncertainties only
    h = backgrounds.getHistogram( massVar )
    #systematicsPlot.addSystematicsGraph( h, s )
    systematicsPlot.addHistogram( h, 'E0' )
    systematicsPlot.combineStatsAndSyst = False
    systematicsPlot.draw()
    
    # example for a 2D histogram
    weightVar = Variable('weight', title='Weight', binning=VariableBinning([0.8,0.9,1.2,1.7,1.8]))
    plot2D = Plot( '2D Test', massVar, weightVar )
    plot2D.addHistogram( backgrounds.getHistogram2D( massVar, weightVar, luminosity=1000 ), 'COLZ' )
    plot2D.draw()
     
    # Datasets and PhysicsProcesses can be persisted in text files
    #writeDatasetsToTextFile( 'datasets.txt', DATASETS.values() )
    #writePhysicsProcessesToTextFile( 'processes.txt', PHYSICSPROCESSES.values() )
    #print 'Reading datasets from "datasets.txt":', readDatasetsFromTextFile( 'datasets.txt' )
    #print 'Reading processes from "processes.txt":', readPhysicsProcessesFromFile( 'processes.txt' )
 
    #for dataset in [dataset1, dataset2, dataset3, data]:
    #    dataset._close( dataset.nominalSystematics.treeName )
 
    raw_input( 'Continue?' )
     
    # clean up and delete temp file
    os.remove( 'temp.root' )
    #os.remove( 'datasets.txt' )
    #os.remove( 'processes.txt' )
    
