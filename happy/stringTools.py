## @package happy.stringTools
#  @brief Collection of helper methods for string mainpulation.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
import logging, string, re, uuid

logger = logging.getLogger( __name__ )

# all characters usable in file names
allowedStringCharacters = string.ascii_letters + string.digits + '_-'

## Create copy of a string with all non-alphanumeric characters removed.
#  Returned string only contains numbers, letters and "_" or "-" which
#  should always yield a string which is a valid file name.
#  @param s         input string
#  @return string 
def alphanumericString( s ):
    return ''.join( ch for ch in s if ch in allowedStringCharacters )

## Helper method for converting strings to booleans
#  @param s    string to convert
#  @return True or False
def string2bool( s ):
    if s.lower() in ['true', 'yes', '1']:
        return True
    return False

UUID_PATTERN = re.compile( '[-_]*[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}' )
## Helper method to remove any UUID from a given string.
#  Will also remove any "-" or "_" before any UUID.
#  @param s     input string
#  @return string
def removeUUID( s ):
    return UUID_PATTERN.subn( '', s, 0 )[0]

## Helper method to append a UUID to a string.
#  If the string already contains a UUID it will be reomved.
#  @param s     input string
#  @return string
def appendUUID( s ):
    s = removeUUID( s )
    return s + '_%s' % uuid.uuid1()

## Helper method to find all occurances of a given char in a string.
#  Used for example to find all parantheses in a cut expression
#  @param expression   string to be searched
#  @param char         single character
#  @return list of positions where the given character is found in the input string.
def findAllOccurrences( expression, char ):
    return [i for i, ltr in enumerate( expression ) if ltr == char]

## Helper method to find matching paranthesis in a string.
#  Generates a list of tuples with the indices of corresponding
#  opening and closing parantheses positions.
#  @param expression   string
#  @return list of tuples with the indices
def findMatchingParantheses( expression ):
    result = []
    openParentheses = findAllOccurrences( expression, '(' )
    for i in openParentheses:
        index = i
        counter = 1
        while counter > 0:
            index += 1
            if expression[index] == '(':
                counter += 1
            elif expression[index] == ')':
                counter -= 1
        result.append( (i, index) )
    return result

## Helper method to remove redundant parantheses.
#  Removes sets of opening and closing parantheses from a string if they are
#  the first and last character or if they are directly inside another set
#  of parantheses.
#  @param expression   string with the input expression
#  @return string -- expression without redundant parantheses
def removeRedundantParantheses( expression ):
    indicesToRemove = []
    previousParantheses = None
    for parantheses in findMatchingParantheses( expression ):
        # check if the parantheses are just first and last character
        if parantheses[0] == 0 and parantheses[1]+1 == len( expression ):
            indicesToRemove.extend( parantheses )
        # check if the parantheses are nested directly inside another set of parantheses
        elif previousParantheses and parantheses[0] == previousParantheses[0]+1 and parantheses[1] == previousParantheses[1]-1:
            indicesToRemove.extend( parantheses )
        previousParantheses = parantheses
    # start with highest character and remove indices
    indicesToRemove.sort( reverse=True )
    for index in indicesToRemove:
        expression = expression[:index] + expression[index+1:]
    return expression

def addParantheses( s, checkFor ):
    ## Helper method to check if any of the given characters is present
    #  @param s            the string to check
    #  @param checkFor     list of strings to check for
    #  @return the string surrounded by parantheses if true, otherwise leave it unchanged
    if any(x in s for x in checkFor):
        return '(%s)' % s
    else:
        return s
