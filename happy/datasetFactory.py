## @package happy.datasetFactory
#  @brief Module to facilitate Dataset creation
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.cut import Cut
from happy.dataset import RootDataset, CombinedDataset
from happy.stringTools import string2bool
import logging, os, re

logger = logging.getLogger( __name__ )

## Object representing a dataset tag
class DatasetTag( object ):
    def __init__( self, name, description='' ):
        self.name = name
        self.description = description
    
    # only hashes the name
    def __hash__( self ):
        return hash( self.name )
    
    # compares only the name
    def __eq__( self, other ):
        return self.name == other.name
    
    def __repr__( self ):
        return 'Dataset(%r, %r)' % (self.name, self.description)
    
    def __str__( self ):
        return self.name

## Class to hold metadata for Dataset objects
class DatasetMetadata( object ):
    ## Default constructor
    #  @param dsid            int dataset ID
    #  @param project         string describing the project name, i.e. "mc15_13TeV"
    #  @param title           string describing the title (optional, default uses dataset ID)
    #  @param derivationType  string describing the derivation type, i.e. "DxAOD_HIGG4D1" (optional)
    #  @param tags            list of DatasetTag objects (optional)
    def __init__( self, dsid, project, title=None, derivationType=None, tags=[] ):
        ## Dataset ID (used as run number for data)
        self.dsid = int(dsid)
        ## Project tag
        self.project = project
        # parsing of the project name following ATLAS naming convention
        prefix, postfix = project.split( '_' )
        ## Year of data taking or MC campaign
        self.year = 2000 + int(prefix[-2:])
        ## Check if dataset is data or MC
        self.isData = 'data' in prefix
        ## Check if dataset is cosmic data (only for data)
        self.isCos = 'cos' == postfix
        ## Check if dataset is commissioning (only for data)
        self.isComm = 'comm' == postfix
        ## Centre of mass energy
        self.sqrtS = float(postfix[:-3]) if 'eV' in postfix else 0.
        ## List of DatasetTag objects
        self.tags = tags
        ## String to identify the derivation type
        self.derivationType = derivationType
        ## Title of the dataset
        self.title = title if title else '%s' % dsid
        ## The tag of the derivation production
        self.derivationTag=None
        for tag in self.tags:
            if tag.name.startswith("p"):
                self.derivationTag=tag
                break
    
    ## Constructor from canonical dataset name.
    #  Assumes ATLAS naming convention:
    #    - MC: @code *.mcXX_XXTeV.<title>.<derivation>.<tags>.* @endcode
    #    - Data: @code *.dataXX_XXTeV.<stream>.<derivation>.<tags>.* @endcode
    #
    #  There can be multiple tags separated by "_".
    #  @param datasetName    dataset name
    @classmethod
    def fromDatasetName( cls, datasetName ):
        s = re.sub('.*data1', 'data1', datasetName )
        s = re.sub('.*mc', 'mc', s )
        result = s.split('.')
        project, dsid, title, derivationType, tagString = result[:5]
        tags = []
        for tagName in tagString.split( '_' ):
            tags.append( DatasetTag(tagName) )
        meta = cls( dsid, project, title, derivationType, tags )
        return meta

    def __repr__( self ):
        return 'DatasetMetaData(%r, %r, %r, %r, %r)' % (self.dsid, self.project, self.title, self.derivationType, self.tags)
    def __str__( self ):
        return 'DatasetMetadata: %d, %s, %s, tags=%s' % ( self.dsid, self.title, self.project, self.tags )
    
    ## Generates the canonical name following the ATLAS naming convention.
    @property
    def canonicalName( self ):
        result = '{}.{:08d}.{}.{}.'.format(self.project, self.dsid, self.title, self.derivationType)
        for tag in self.tags:
            result += tag.name + '_'
        return result.rstrip( '_' )
    
    ## Run number (equivalent to dsid)
    @property
    def runNumber( self ):
        return self.dsid

    @runNumber.setter
    def runNumber( self, runNumber ):
        self.dsid = runNumber

## Create a list of Dataset objects with common attributed from an XML element
#  @code
#  <Datasets basepath="" treeName="" isData="" isSignal="" weightExpression="">
#    <Style color="5"/>
#    <DSIDS> 1234, 4321, 3412 </DSIDS>
#    <AddCuts>
#      <Cut> Cut1 </Cut>
#      <Cut> Cut2 </Cut>
#    </AddCuts>
#    <IgnoreCuts>
#      <Cut> Cut3 </Cut>
#      <Cut> Cut4 </Cut>
#    </IgnoreCuts>
#  </Dataset>
#  @endcode
#  @param element    the XML element
#  @return list of Dataset objects
def datasetsFromXML( element ):
    attributes = element.attrib
    datasets = []
    treeName = None
    isData = False
    isSignal = False
    isBSMSignal = False
    weighExpression = None
    if attributes.has_key( 'basePath' ):
        basePath = attributes['basePath']
    else:
        logger.warning( 'datasetsFromXML(): Datasets XML element is missing "basePath" attribute. No datasets created.' )
        return datasets
    if attributes.has_key( 'treeName' ):
        treeName = attributes['treeName']
    if attributes.has_key( 'isData' ):
        isData = string2bool(attributes['isData'])
    if attributes.has_key( 'isSignal' ):
        isSignal = string2bool(attributes['isSignal'])
    if attributes.has_key( 'isBSMSignal' ):
        isBSMSignal = string2bool(attributes['isBSMSignal'])
    if attributes.has_key( 'weightExpression' ):
        weightExpression = attributes['weightExpression']
    style = Style.fromXML( element.find( 'Style' ) ) if element.find( 'Style' ) is not None else None
    addCuts = []
    ignoreCuts = []
    if element.find( 'AddCuts' ):
        for cutElement in element.find( 'AddCuts' ).findall( 'Cut' ):
            addCuts.append( Cut.fromXML( cutElement ) )
    if element.find( 'IgnoreCuts' ):
        for cutElement in element.find( 'IgnoreCuts' ).findall( 'Cut' ):
            ignoreCuts.append( Cut.fromXML( cutElement ) )
    if element.find( 'DSIDS' ) is not None:
        dsids = element.text.split(',')
        for dsid in dsids:
            dataset = cls.datasetFromDSID( basePath, int(dsid), dsid.strip(), treeName  , style, weightExpression )
            dataset.isData = isData
            dataset.isSignal = isSignal
            dataset.isBSMSignal = isBSMSignal
            dataset.addCuts += addCuts
            dataset.ignoreCuts += ignoreCuts
    else:
        logger.warning( 'datasetsFromXML(): Datasets XML element is missing "DSIDS" element. No datasets created.' )
        return datasets
    return datasets

## Create a list of Dataset objects from all directories matching the pattern.
#  Expects that the ROOT files belonging to each Dataset are stored in individual
#  directories. The directory name has to match the naming pattern for ATLAS
#  datasets. The ROOT files within each dataset directory have to end on "*.root*".
#  If the DSID extracted from the directory name is found in the crossSectionDB,
#  cross section, etc. will be set automatically.
#  @param basePath          string for the path that conatins all dataset directories
#  @param pattern           regular expression to identify dataset directories that should be considered
#  @param treeName          name of nominal tree for all datasets (optional, default is "NOMINAL")
#  @param weightExpression  default weight expression for all datasets (optional, default is an empty string)
#  @return list of Dataset objects
def datasetsFromPattern( basePath, pattern, treeName='NOMINAL', weightExpression=None ):
    import ROOT
    directory = ROOT.gSystem.OpenDirectory( basePath )
    entry = True
    datasets = []
    p = re.compile( pattern )
    while entry:
        entry = ROOT.gSystem.GetDirEntry( directory )
        if p.match( entry ):
            metadata = DatasetMetadata.fromDatasetName( entry )
            if metadata:
                fileNames = [os.path.join( basePath, entry, '*.root*' )]
                dataset = RootDataset( str(metadata.dsid), metadata.title, fileNames, treeName )
                dataset.isData = metadata.isData
                dataset.metadata = metadata
                if metadata.isData:
                    dataset.isData = True
                # implicitely sets the cross section, etc. if it is in the crossSectionDB
                dataset.dsid = metadata.dsid
                datasets.append( dataset )
            else:
                logger.warning( 'datasetFromPattern(): unable to find metadata for "%s"' % entry )
    ROOT.gSystem.FreeDirectory( directory )
    return datasets

## Creates a RootDataset object from a dataset ID.
#  Expects that the ROOT files belonging to the RootDataset are stored in a directory
#  which contains the dataset ID in its name. The ROOT files within the directory
#  have to end on "*.root*". If the DSID is found in the crossSectionDB, cross section,
#  etc. will be set automatically.
#  @param dsid              int dataset ID
#  @param title             title of the dataset (optional)
#  @param basePath          string for the path that conatins all dataset directories
#  @param treeName          name of nominal tree for the dataset (optional, default is "NOMINAL")
#  @param style             Style object defining the dataset plotting style
#  @param weightExpression  default weight expression for the dataset (optional, default is an empty string)
#  @return new Dataset objects
def datasetFromDSID( dsid, title='', basePath='', treeName='NOMINAL', style=None, weightExpression=None ):
    import ROOT
    directory = ROOT.gSystem.OpenDirectory( basePath )
    entry = True
    name = str(dsid)
    title = title if title else name
    dataset = None
    while entry:
        entry = ROOT.gSystem.GetDirEntry( directory )
        if name in entry:
            fileNames = [os.path.join( basePath, entry, '*.root*' )]
            dataset = RootDataset( name, title, fileNames, treeName, style, weightExpression )
            dataset.metadata = DatasetMetadata.fromDatasetName( entry )
            if dataset.metadata:
                dataset.isData = dataset.metadata.isData
            break
    ROOT.gSystem.FreeDirectory( directory )
    if dataset:
        # implicitely sets the cross section, etc. if it is in the crossSectionDB
        dataset.dsid = dsid
    else:
        logger.warning( 'datasetFromDSID(): unable to find dataset for "%d" in "%s"' % (dsid,basePath) )
    return dataset

## Creates a CombinedDataset object and adds a RootDataset for each DSID.
#  Expects that the ROOT files belonging to each RootDataset are stored in a separate
#  directory which contains the dataset ID in its name. The ROOT files within the
#  directory have to end on "*.root*". If the DSID is found in the crossSectionDB,
#  cross section, etc. will be set automatically.
#  @param name              name of the new PhysicsProcess
#  @param title             title of the new PhysicsProcess (optional)
#  @param basePath          string for the path that conatins all dataset directories
#  @param dsids             list of integers defining the dataset IDs
#  @param treeName          name of nominal tree for all datasets (optional, default is "NOMINAL")
#  @param style             Style object used for the new PhysicsProcess (and all its datasets)
#  @param weightExpression  default weight expression for all datasets (optional, default is an empty string)
#  @return new PhysicsProcess objects
def combinedDatasetFromDSIDs( name, title='', basePath='', dsids=[], treeName='NOMINAL', style=None, weightExpression=None ):
    process = CombinedDataset( name, title, style, isData=True )
    for dsid in dsids:
        dataset = datasetFromDSID( dsid, title, basePath, treeName, style, weightExpression )
        process.datasets.append( dataset )
        # if only one dataset is not data, the whole process is not data
        if not dataset.isData:
            process.isData = False
    return process
            
if __name__ == '__main__':
    from ROOT import kRed, kCyan
    from plotting.Dataset import PhysicsProcess
    from plotting.AtlasStyle import Style
    
    #logging.root.setLevel( logging.DEBUG )
    
    db = CrossSectionDB.get()
    db.readTextFile( 'plotting/files/susy_crosssections_13TeV.txt' )
    
    basePath = '/eos/atlas/user/c/cgrefe/xTau-NTuples/hh.v14_skimmed/mc15'
    
    print 'Building signal process:'
    signal = PhysicsProcess( 'signal', 'Signal', style=Style( kRed+1 ) )
    for dsid in [ 341124, 341157 ]:
        d = DatasetFactory.datasetFromDSID( basePath, dsid )
        d.isSignal = True
        print d
        print d.metadata
        signal.datasets.append( d )
    print signal
    
    print 'Building ztautau process:'
    ztautau = PhysicsProcess( 'ztautau', 'Z#rightarrow#tau#tau', style=Style( kCyan+1 ) )
    for dsid in range(361510, 361515) + range(361638, 361643):
        d = DatasetFactory.datasetFromDSID( basePath, dsid )
        d.isSignal = True
        print d
        print d.metadata
        ztautau.datasets.append( d )
    print ztautau
