## @package happy.miscellaneous
#  @brief Collection of various helper methods.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
import copy, logging, os, re, sys

logger = logging.getLogger( __name__ )

## Class that emulates the behavior of an Enum
#  @code
# from happy.tools.miscellaneous import Enum
# animals = Enum(['Cat', 'Dog'])
# print animals.Dog
#  @endcode
class Enum( set ):
    def __getattr__( self, name ):
        if name in self:
            return name
        raise AttributeError
    
## Method to execute a ROOT C macro
#  @param path   path to the ROOT macro
def loadRootMacro( path ):
    if os.path.exists( path ) and os.path.isfile( path ):
        import ROOT
        ROOT.gROOT.LoadMacro( path )
    else:
        logger.error( 'loadRootMacro: unable to open %s' % path )

## Method to resolve regular expressions in file names.
#  TChain::Add only supports wildcards in the last items, i.e. on file level.
#  This method can resolve all wildcards at any directory level,
#  e.g. /my/directory/a*test*/pattern/*.root
#  @param pattern      the file name pattern using python regular expressions
#  @return list of all files matching the pattern
def findAllFilesInPath( pattern ):
    files = []
    path = ''
    items = pattern.split( '/' )
    
    def checkPath( path, items ):
        # nested method to deal with the recursion
        import ROOT
        if not items:
            return
        myItems = copy.copy( items )
        item = myItems.pop(0)
        if '*' in item:
            directory = ROOT.gSystem.OpenDirectory( path )
            # beg and end of line control so that *truc does not match bla_truc_xyz
            item = "^"+item.replace( '*', '.*' )+"$"
            p = re.compile( item )
            entry = True
            while entry:
                entry = ROOT.gSystem.GetDirEntry( directory )
                if p.match( entry ):
                    if not myItems:
                        files.append( path + entry )
                    else:
                        checkPath( path + entry + '/', myItems)
            ROOT.gSystem.FreeDirectory( directory )
        elif item and not myItems:
            files.append( path + item )
        else:
            checkPath( path + item + '/', myItems )
    checkPath( path, items )
    return files

## Method to interrupt the program and wait for user input.
#  Answering the prompt with "no" or "n" will stop the program.
def interrupt():
    res = raw_input( 'Continue? [Y/N] ' )
    if res.lower() in ['n', 'no']:
        sys.exit()

## Helper method for converting strings to booleans
#  @param string    string to convert
#  @return True or False
def string2bool( string ):
    if string.lower() in ['true', 'yes', '1']:
        return True
    return False

## Generate a wrapper method for an inherited method.
#  This is used to replace methods in the base class
#  that return new instances of the base class. The
#  new method returns a new instance of the derived
#  class instead.
#  @param cls        class derived from base class
#  @param baseCls    base class
#  @param name       method name
def wrapMethod( cls, baseCls, name ):
    def inner( self, *args ):
        result = getattr( baseCls, name )( self, *args )
        if isinstance( result, baseCls ):
            result = cls( result )
        return result
    setattr( cls, name, inner )

## Calculates the Poisson error for a given number.
#  @param n                    number of entries
#  @param confidenceInterval   probability covered inside the error band (default is 0.6827, i.e. 1 sigma)
def calculatePoissonErrors( n, confidenceInterval=0.6827 ):
    from ROOT import Math
    n = round( n )
    invIntegral = (1.-confidenceInterval) / 2.
    lo = Math.gamma_quantile( invIntegral, n, 1. ) if n != 0 else 0.
    hi = Math.gamma_quantile_c( invIntegral, n+1, 1. )
    return n - lo, hi - n

## Prints a progress bar using percent
#  @param current   current value
#  @param total     total value
#  @param text      additional text appended to the line
def progressBarPercent( current, total, text='' ):
    if float(total) <= 0:
        return
    barLength = 20
    fraction = current / float(total)
    nBlocks = int( round( barLength * fraction ) )
    t = '\r[{0}] ({1:>3d}%) {2:50}'.format( '#'*nBlocks + '-'*(barLength-nBlocks), int(fraction*100), text )
    sys.stdout.write( t )
    sys.stdout.flush()
    if fraction >= 1:
        print ''

## Prints a progress bar using absolute numbers
#  @param current   current value
#  @param total     total value
#  @param text      additional text appended to the line
def progressBarInt( current, total, text='' ):
    if float(total) <= 0:
        return
    barLength = 20
    fraction = current / float(total)
    nBlocks = int( round( barLength * fraction ) )
    t = '\r[{0}] ({1}/{2}) {3:50}'.format( '#'*nBlocks + '-'*(barLength-nBlocks), current, total, text )
    sys.stdout.write( t )
    sys.stdout.flush()
    if fraction >= 1:
        print ''

## Helper method to get an array of values and weights from a TTree.
#  IMPORTANT: values and weights are associated with the TTree buffer.
#  They need to be copied in order to be persisted!
#  @param tree              TTree object used to extract the results
#  @param expression        string used as variable expression
#  @param selection         string used as weight and cut expression
#  @return array of values, array of weights (two return values)
def getValuesFromTree( tree, expression, selection ):
    logger.debug( 'getValuesFromTree(): calling TTree::Draw( "%s", "%s", "%s" )' % (expression, selection, 'goff') )
    tree.Draw( expression, selection, 'goff' )
    entries = tree.GetSelectedRows()
    valueBuffer = tree.GetV1()
    weightBuffer = tree.GetW()
    import numpy
    values = numpy.empty(0)
    weights = numpy.empty(0)
    if entries > 0:
        values = numpy.frombuffer( buffer=valueBuffer, dtype='double', count=entries )
        weights = numpy.frombuffer( buffer=weightBuffer, dtype='double', count=entries )
    return values, weights
